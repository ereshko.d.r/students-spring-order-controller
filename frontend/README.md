# frontend

## Структура и файлы

### src

#### App.js

Точка входа в приложение инициализирует приложение и роуты

### api

Каталог для файлов взаимодействия с `api` `backend` приложения

##### api.js

Класс `ApiService` для взаимодействия с эндоинтами `api` `backend` приложения. Содержит базовые методы для создания запросов и обработки ответов

##### postRequest(path, body, params), getRequest(path, params), patchRequest(path, body, params)

Методы класса для создания запросов к backend части приложения. Методы используют библиотеку `axios`

Так как система аутентификации использует короткоживущий `JWT` ключ, отправленные запросы будут с некоторой периодичностью приходить со стасусом `401`. При получении ответа с данным статусом методы будут отправлять запрос на обновление токена, и, если свежий токен получен, метод вызывается рекурсивно с теми же параметрами, но новым `JWT` ключом в заголовке. Иначе сервис `authService` очистит `refreshToken` из `cookie`, `accessToken` из `sessionStorage` и вернёт пользователя на страницу авторизации.

При остальных статуах отличных от `2**` метод парсит ошибку и также возвращает ответ сервера

```js
postRequest(path, body, params=null) {
        const {headers} = authService.authHeader
        const response = axios.post(`${API_URL}/${path}`, body, {
            headers,
            params
        })
            .catch(async (error) => {
                if (error.response.status === 401) {
                    if (await authService.refreshAccessToken()) {
                        return this.postRequest(path, body, params)
                    }
                    return {
                        status: error.response.status
                    }
                }
                return error.response
            })
        return response
    }
```

#### authService.js

Класс `AuthService` имеет методы для управления аутентификацией и авторизацией, в том числе для обновления `accessToken` и удалении `refreshToken`

##### authHeader()

Метод для формирования заголовка `авторизации` для исходящих запросов к `backend` приложению

```js
get authHeader() {
    const accessToken = sessionStorage.getItem('accessToken')
    if (!accessToken) {
        return false
    }
    const bearer = {authorization: 'Bearer ' + accessToken}
    return {headers: bearer}
}
```

##### refreshAccessToken()

Метеод для обновления `accessToken`. При удачной попытке сохраняет ответ и возвращает булевое `true`, иначе удаляет старый `accessToken` и возвращает `false`

```js
async refreshAccessToken() {
    const response = await axios.get(`${API_URL}/auth/refresh`)
        .catch(() => {
            this.removeAccessToken()
            return null
        })
    if (!response) {
        return false
    }
    this.saveAccessToken(response.data.accessToken)
    return true
}
```

### components

Каталог для хранения компонентов `react` приложения

#### adminPanel

Содержит компоненты и страницы для админ панели приложения

#### context

Содержит контекст для компонентов

#### hooks

Содержит кастомные хуки для приложения

#### modules

Содержит различные дополнительные модули для обработки компонентов или их содержимых, формирования данных и прочее

#### modules/imageHandler/imageCutter.js

Класс `BaseCutterImage` для обработки изображений на стороне клиента. В приложении используется для обрезки изображений на бейдж, аватар, обрезает части изображения паспорта, а также подготавливает изображение для отправки на сервера `tesseract` путём наложения фильтров для лучшего восприятия текста в изображении.

В основе работы с изображением использует `HTML` элемент `canvas` и его интерфейсы

```js
// create canvas element
createCanvas(width, height) {
    const canvas = document.createElement('canvas')
    canvas.width = width
    canvas.height = height
    return canvas
}
```

Пример взаимодействия с элементом `canvas`

```js
getDrewImage(canvas, sx, sy, sWidth, sHeight) {
    canvas.getContext('2d').drawImage(
        this.image,
        sx * this.widthRate, sy * this.heightRate, 
        sWidth * this.widthRate, sHeight * this.heightRate,
        0, 0, canvas.width, canvas.height
    )
    return canvas.toDataURL('image/png')
}
```

Класс `BaseCutterImage` используется как родительский для таких классов, как `PassportCutter` и `BageCutter` для обработки и обрезки фото паспорта и бейджа

Класс `PassportCutter` использует `api` `tesseract` для извлечения текста из изображения. Для более качественного извлечения текста, используются методы родительского класса для повышения контрастности, резкости и перевода изображения в ч/б. Ниже пример для подготовки иозбражения `серии` паспорта

```js
async getSerial() {
    const serial = this.TEMPLATE_DETAILS_COORDINATES.serial
    const width = serial.width
    const height = serial.height
    const canvas = this.createCanvas(width*10, height*10)
    this.addEffects(canvas)
    const cuttedImageData = this.getDrewImage(
        canvas, serial.sx, serial.sy, width, height
    )
    const flippedImage = await this.rotate(cuttedImageData)
    canvas.remove()
    return flippedImage
}
```

Данный метод создает объект `serial` содержащий координаты расположения серии паспорта на шаблоне, затем увеличивает изображение в 10 раз, добавляет эффекты и фильтры, затем переносит изображение на холст `canvas`, разворачивает изображение и возвращает изображение в формате `base64`

Для конвертации изображения паспорта используется метод `getPassData()`, который создаёт массив `tasks` с асинхронными функциями `convertToText` и передаёт в функции изображения в формате `base64`. После, результаты выполнений формируются в объект и возвращаются

```js
async getPassData() {
    const tasks = [
        this.convertToText(this.getFirstName()),
        this.convertToText(this.getLastName()),
        this.convertToText(this.getSecondName()),
        this.convertToText(this.getGender()),
        this.convertToText(this.getBirthdate()),
        this.convertToText(await this.getSerial()),
        this.convertToText(await this.getNumber()),
    ]
    const result = await Promise.all(tasks)
    return {
        firstName:  this.capitalizeString([0].replace(/[^А-ЯЁ]/g, '')),
        lastName:   this.capitalizeString(result[1].replace(/[^А-ЯЁ]/g, '')),
        secondName: this.capitalizeString(result[2].replace(/[^А-ЯЁ]/g, '')),
        gender:     this.capitalizeString(result[3].replace(/[^А-ЯЁ]/g, '')),
        birthdate:  result[4].replace(/[^0-9.]/g, ''),
        serialPass: result[5].replace(/[^0-9]/g, ''),
        numberPass: result[6].replace(/[^0-9]/g, ''),
        photoPass: this.getPhoto(),
        photoFirstName: this.getFirstName(),
        photoLastName: this.getLastName(),
        photoSecondName: this.getSecondName(),
    }
}
```

#### pages

Каталог содержащий страницы приложений собранные из компонентов

#### UI

Каталог содержащий атомарные компоненты приложения
