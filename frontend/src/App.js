import React from 'react'
import {BrowserRouter as Router, Routes, Route, Navigate} from 'react-router-dom'
import './App.css'

import UserContext, {user} from './components/context/userContext'

import AuthPage from './components/pages/auth/page/AuthPage';
import HomePage from './components/pages/homePage/page/HomePage';
import UserProfile from './components/pages/lk/page/UserProfile';
import RegistrationRoute from './components/pages/registration/page/RegistrationRoute';
import OrderPage from './components/pages/lk/module/orders/OrderPage';
import Profile from './components/pages/lk/module/profile/Profile';
import PrivateRoute from './components/pages/lk/module/privaterouter/PrivateRoute';
import OrderDetails from './components/pages/lk/module/orders/details/OrderDetails';
import AuthAdminPage from './components/adminPanel/pages/auth/page/AuthAdminPage';
import AdminPanelPrivateRoute from './components/adminPanel/pages/privateRoute/AdminPanelPrivateRoute';
import PartipiansList from './components/adminPanel/pages/partipians/page/ParticipantList';
import PartipiansDetails from './components/adminPanel/pages/partipians/page/ParticipantDetails';
import AdminOrdersList from './components/adminPanel/pages/orders/page/AdminOrdersList'
import AdminOrdersDetails from './components/adminPanel/pages/orders/page/AdminOrderDetails';
import About from './components/pages/about/About';

export const API_URL = '/api' 
export const URL = '/'

function App() {
    return (
        <UserContext.Provider value={user}>
            <Router>
                <Routes>
                    <Route path='/about' element={<About />} />
                    <Route path='/' element={<HomePage />} />
                    <Route path='/auth' element={<AuthPage />} />
                    <Route path='/registration' element={<RegistrationRoute user={user}/>} />
                    <Route element={<PrivateRoute />}>
                        <Route element={<UserProfile />}>
                            <Route path='/me' element={<Profile />}/>
                            <Route path='/orders' element={<OrderPage />}/>
                            <Route path='/orders/:id' element={<OrderDetails />} />                                   
                        </Route>
                    </Route>
                    <Route path='/admin/auth' element={<AuthAdminPage />} />
                    <Route path='/admin' element={<AdminPanelPrivateRoute />}>
                        <Route path='/admin/participants' element={<PartipiansList />} />
                        <Route path='/admin/participants/:id' element={<PartipiansDetails />} />
                        <Route path='/admin/orders/' element={<AdminOrdersList />} />
                        <Route path='/admin/orders/:id' element={<AdminOrdersDetails />} />
                    </Route>
                </Routes>
            </Router>
        </UserContext.Provider>
    )
}

export default App;
