import React from 'react'
import { Outlet } from 'react-router-dom'
import classes from './UserProfile.module.css'

import Menu from '../module/menu/Menu'


const UserProfile = () => {
    
    return (
        <div className={classes.ProfileContainer}>
            <Menu />
            <div className={classes.ContentContainer}>
                <div className={classes.Content}>
                    <Outlet />
                </div>
            </div>
        </div>
    )
}

export default UserProfile