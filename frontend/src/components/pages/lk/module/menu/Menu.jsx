import React, { useContext } from 'react'
import classes from './Menu.module.css'
import { useNavigate } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import AuthService from '../../../../../api/auth'
import UserContext from '../../../../context/userContext'

import AvatarSmall from '../../../../UI/avatar/avatarsmall/AvatarSmall'
import ButtonMedium from '../../../../UI/buttons/buttonMedium/ButtonMedium'

const authService = new AuthService()

const Menu = observer(() => {
    const userState = useContext(UserContext)
    const navigate = useNavigate()

    const logoutHandler = async () => {
        const response = await authService.logout()
        if (response) {
            userState.setUser(undefined)
            navigate('/')
        }
    }
    
    return (
        <div className={classes.Menu}>
            <div className={classes.MenuContent}>
                <div>
                <AvatarSmall
                    avatar={userState.user.avatar}
                    firstName={userState.user.firstName}
                />
                </div>
                    <div className={classes.NavBar}>
                        <ButtonMedium
                            onClick={() => navigate('/me')}
                        >
                            Профиль
                        </ButtonMedium>
                        <ButtonMedium
                            onClick={() => navigate('/orders')}
                        >
                            Заявки
                        </ButtonMedium>
                    </div>
                    <div className={classes.Documents}>

                    </div>
                    <div className={classes.Exit}>
                        <ButtonMedium
                            color={'red'}
                            onClick={logoutHandler}
                        >
                            Выход
                        </ButtonMedium>
                    </div>
            </div>
        </div>
    )
})

export default Menu