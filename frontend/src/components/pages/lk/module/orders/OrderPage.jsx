import React, { useContext, useEffect, useState } from 'react'
import Carousel from '../../../../modules/carousel/Carousel'

import OrderList from './list/OrderList'
import InviteList from './list/InviteList'
import ModalWindow from '../../../../UI/modalwindow/ModalWindow'
import CreateOrderModal from './list/CreateOrderModal'
import OrderInvitesContext from '../../../../context/orderInviteContext'
import eventSourceClient from '../../../../../api/eventSourceClient'

const OrderPage = ({invites=[], orders=[], ...props}) => {
    const [showModal, setShowModal] = useState(false)
    const [isSubscribe, setIsSubscribe] = useState(false)
    const ordersInvites = useContext(OrderInvitesContext)

    const handlerOrder = () => {
        setShowModal((currentState) => {return currentState ? false : true})
    }

    return (
        <>
            <ModalWindow showModal={showModal} setShowModal={setShowModal}>
                <CreateOrderModal setShowModal={setShowModal} />
            </ModalWindow>
            <Carousel
                handlerOrder={handlerOrder}
                invites={invites}
                orders={orders}
            >
                <OrderList handlerOrder={handlerOrder} />
                <InviteList />
            </Carousel>
        </>
    )
}

export default OrderPage