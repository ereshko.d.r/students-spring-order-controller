import React from 'react'
import classes from './OrderList.module.css'

import ButtonSmall from '../../../../../UI/buttons/buttonSmall/BurronSmall'
import apiService from '../../../../../../api/api'
import { useNavigate } from 'react-router-dom'
import { observer } from 'mobx-react-lite'

const InviteRow = observer(({ invite }) => {
    const navigate = useNavigate()

    const acceptHandle = async () => {
        const response = await apiService.acceptInvite(invite.orderId)
        if (response.status === 200) {
            const {orderId} = response.data
            navigate(`/orders/${orderId}`)
        }
    }

    const rejectHandle = () => {
        apiService.rejectInvite(invite.orderId)
    }

    return (
        <div className={classes.InviteRow}>
            <div>{invite.groupName}</div>
            <div>{invite.nominationName}</div>
            <div>{invite.perfomanceName}</div>
            <div className={classes.InviteButtons}>
                <ButtonSmall
                    onClick={acceptHandle}
                    color={'yellow'}
                >
                    Принять
                </ButtonSmall>
                <ButtonSmall
                onClick={rejectHandle}
                    color={'red'}
                >
                    Отклонить
                </ButtonSmall>
            </div>
        </div>
    )
})

export default InviteRow