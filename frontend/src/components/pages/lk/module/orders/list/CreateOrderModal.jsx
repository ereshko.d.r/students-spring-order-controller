import React, { useEffect, useState } from 'react'
import classes from './OrderList.module.css'

import apiService from '../../../../../../api/api'
import Input from '../../../../../UI/inputs/Input'
import Dropdown from '../../../../../UI/dropdown/Dropdown'
import ButtonLarge from '../../../../../UI/buttons/buttonLarge/ButtonLarge'
import { useNavigate } from 'react-router-dom'


const CreateOrderModal = ({setShowModal}) => {
    const [directionsList, setDirectionsList] = useState([])
    const [groupName, setGroupName] = useState('')
    const [perfomanceName, setPerfomanceName] = useState('')
    const [direction, setDirection] = useState()
    const [nomination, setNomination] = useState()
    const [disabled, setDisabled] = useState(true)
    const navigate = useNavigate()

    const submitHandler = async (event) => {
        event.preventDefault()
        setShowModal(false)
        if (!disabled) {
            const body = {
                groupName: groupName,
                perfomanceName: perfomanceName,
                directionId: direction.id,
                nominationId: nomination.id
            }
            const response = await apiService.createOrder(body)
            if (response.status === 201) {
                console.log(response)
                const id = response.data.orderId
                navigate(`/orders/${id}`)
            }
        }
    }

    const nameHandler = (event) => {
        setGroupName(event.target.value)
    }

    const perfomanceNameHandler = (event) => {
        setPerfomanceName(event.target.value)
    }

    const getDirections = async () => {
        const response = await apiService.getDirections()
        if (response.status === 200) {
            setDirectionsList(response.data)
        }
        else if (response.status === 401) {
            navigate('/auth')
        }
    }

    useEffect(() => {
        if (directionsList.length === 0) {
            getDirections()
        }
        if (groupName, direction, nomination) {
            
            setDisabled(false)
        }
        else {
            setDisabled(true)
        }
    }, [directionsList, direction, nomination, groupName, perfomanceName])

    useEffect(() => {
        setNomination()
    }, [direction])
    return (
        <div className={classes.ModalContent}>
            <h3>Создать заявку</h3>
            <form onSubmit={submitHandler} id='createOrderForm'>
                <Dropdown inputName={'Направление'} 
                    list={directionsList}
                    value={direction} setValue={setDirection}
                />
                <Dropdown inputName={'Номинация'} 
                    list={direction?.nominations}
                    value={nomination} setValue={setNomination}
                />
                <Input
                    inputName={'Название коллектива'}
                    value={groupName}
                    onChange={nameHandler}
                    form='createOrderForm'
                />
                <Input
                    hidden={!direction?.isPerfomance || false}
                    inputName={'Название номера'}
                    value={perfomanceName}
                    onChange={perfomanceNameHandler}
                    form='createOrderForm'
                />
                <ButtonLarge
                    form='createOrderForm'
                    type='submit'
                    color={'yellow'}
                    disabled={disabled}
                >Создать заявку</ButtonLarge>
            </form>
        </div>
    )
}

export default CreateOrderModal