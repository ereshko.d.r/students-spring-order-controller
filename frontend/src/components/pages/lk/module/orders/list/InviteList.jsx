import React, { useState, useRef, useEffect, useContext }from 'react'
import classes from './OrderList.module.css'
import { observer } from 'mobx-react-lite'
import InviteRow from './InviteRow'
import OrderInvitesContext from '../../../../../context/orderInviteContext'

const InviteList = observer(({ handlePrevPage, style, ...props }) => {
    const [height, setHeight] = useState(100)
    const listRef = useRef({})
    const {invites} = useContext(OrderInvitesContext)

    const resizeHandler = () => {
        setHeight(listRef.current.clientHeight - 150)
    }

    const InviteComponent = observer(() => {
        return (
            <div className={classes.InviteList} style={{height: height}}>
                {invites.map((value, index) => (
                    <InviteRow invite={value} key={index} />
                ))}
            </div>
        )
    })

    useEffect(() => {
        resizeHandler()
        window.addEventListener('resize', resizeHandler)
        return () => {
            window.removeEventListener('resize', resizeHandler)
        }
    }, [])

    return (
        <div className={classes.OrderListContent} style={style} ref={listRef}>
                <div className={classes.Header}>
                    <button onClick={handlePrevPage} style={{width: 100}}>Назад</button>
                    <h3>Входящие приглашения</h3>
                    <button disabled style={{width: 100}}></button>
                </div>
            {
                invites?.length > 0 ?
                <div className={classes.OrderConteiner}>
                    <InviteComponent />
                </div>
                :
                <></>
            }
        </div>
    )
})

export default InviteList