import React, { useContext, useEffect, useRef, useState }  from 'react'
import classes from './OrderList.module.css'
import { observer } from 'mobx-react-lite'

import OrderRow from './OrderRow'

import ButtonLarge from '../../../../../UI/buttons/buttonLarge/ButtonLarge'
import OrderInvitesContext from '../../../../../context/orderInviteContext'

const OrderList = observer(({ handleNextPage, handlerOrder, style, ...props }) => {
    const listRef = useRef(500)
    const {orders, invites} = useContext(OrderInvitesContext)


    const OrdersComponent = observer(() => {
        return (
            <div className={classes.OrderList}>
                {orders?.map((value, index) => (
                    <OrderRow order={value} key={index}/>
                ))}
            </div>
        )
    })

    return (
        <div className={classes.OrderListContent} style={style} ref={listRef}>
            <div className={classes.Header}>
                <button disabled></button>
                <h3>Заявки на участие</h3>
                <button onClick={handleNextPage}>
                    Приглашения
                    <div>{invites.length}</div>
                </button>
            </div>
            {
                orders?.length > 0 ?
                <div className={classes.OrderConteiner}>
                    <OrdersComponent />
                    <ButtonLarge
                        color={'yellow'}
                        onClick={handlerOrder}
                    >
                        + Создать заявку
                    </ButtonLarge>
                </div>
                :
                <>
                    <div style={{padding: '30px 0'}}>
                        <h3>У вас еще нет заявок</h3>
                    </div>
                    <ButtonLarge
                        color={'yellow'}
                        onClick={handlerOrder}
                    >
                        + Создать заявку
                    </ButtonLarge>
                </>
            }
        </div>
    )
})

export default OrderList