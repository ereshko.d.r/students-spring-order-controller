import React from 'react'
import classes from './OrderList.module.css'

import StatusbarSmall from '../../../../../UI/statusbars/statusbarSmall/StatusbarSmall'
import { useNavigate } from 'react-router-dom'

const OrderRow = ({order}) => {
    const navigate = useNavigate()

    const getStatus = () => {
        if (order.orderIsSent || order.orderStatus === false) {
            if (order.orderStatus === true) {
                return (
                    <StatusbarSmall
                        color={'blue'}
                    >
                        Заявка допущена
                    </StatusbarSmall>
                )
            }
            else if (order.orderStatus === false) {
                return (
                    <StatusbarSmall color={'red'}>
                        Заявка не допущена
                    </StatusbarSmall>
                )
            }
            else {
                return (
                    <StatusbarSmall>
                        Проверка заявки
                    </StatusbarSmall>
                )
            }
        }
        else {
            return (
                <StatusbarSmall>
                    Формирование заявки
                </StatusbarSmall>
            )
        }
    }
    return (
        <div
            className={classes.OrderRow}
            onClick={() => navigate(`${order.id}`)}
        >
            <div>{order.perfomanceName || order.groupName}</div>
            <div>{order.nominationName}</div>
            <div>{order.userOrderRole}</div>
            <div>{getStatus()}</div>
        </div>
    )
}

export default OrderRow