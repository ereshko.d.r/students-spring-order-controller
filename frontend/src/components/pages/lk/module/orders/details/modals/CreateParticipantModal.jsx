import React, { useEffect, useState } from 'react'
import classes from '../OrderDetails.module.css'
import Input from '../../../../../../UI/inputs/Input'
import Dropdown from '../../../../../../UI/dropdown/Dropdown'
import ButtonMedium from '../../../../../../UI/buttons/buttonMedium/ButtonMedium'
import apiService from '../../../../../../../api/api'
import { useLocation } from 'react-router-dom'

const CreateParticipantModal = ({setShowModal, socketHandler}) => {
    const [rolesList, setRolesList] = useState([])
    const [role, setRole] = useState()
    const [email, setEmail] = useState('')

    const [disabled, setDisabled] = useState(true)
    const [disabledEmail, setDisabledEmail] = useState(false)
    const [emailError, setEmailError] = useState(false)
    const [usersList, setUsersList] = useState(null)
    const location = useLocation()


    const getRoles = async () => {
        const result = await apiService.getRoles()
        result.data.splice(0, 1)
        setRolesList(result.data)
    }

    const getUsersList = async (email) => {
        const response = await apiService.getUsersByEmail({email})
        if (response.status === 200) {
            console.log(response.data)
            setUsersList(response.data)
        }
        else {
            setUsersList(null)
        }
    }

    const emailHandler = (event) => {
        setEmail(event.target.value)
        getUsersList(event.target.value)
        setEmailError(false)
    }

    const submitHandler = async (event) => {
        event.preventDefault()
        const user = await checkEmailHandler()
        if (user) {
            const body = {
                userId: user.id,
                roleId: role.id,
            }
            setRole('')
            setEmail('')
            socketHandler('sendInvite', body)
            return setShowModal(false)
        }
    }

    const checkEmailHandler = async () => {
        setDisabledEmail(true)
        const response = await apiService.getUserByEmail({email})
        if (response.status === 200) {
            const orderId = location.pathname.split('/')[2]
            const checkResponse = await apiService.checkIsUserInOrder({
                email, orderId
            })
            if (checkResponse.status === 404) {
                console.log(checkResponse)
                setDisabledEmail(false)
                return response.data
            }
            else {
                setEmailError('Данный пользователь уже в заявке')
                setDisabledEmail(false)
            }
        }
        else if (response.status === 404) {
            setEmailError('Пользователя не существует')
            setDisabledEmail(false)
        }
    }

    useEffect(() => {
        if (rolesList.length === 0) {
            getRoles()
        }
        const regEx = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}(?:\.[a-zA-Z]{2,})?$/
        if (regEx.test(email) && role) {
            setDisabled(false)
        }
        else {
            setDisabled(true)
        }
    }, [email, role])

    return (
        <div className={classes.ModalContent}>
            <h3>Добавить участника</h3>
                <Dropdown inputName={'Роль'} list={rolesList} value={role} setValue={setRole}/>
            <form onSubmit={submitHandler} id='addUserForm'>
                <Input
                    inputName={'Email участника'}
                    value={email}
                    onChange={emailHandler}
                    form='addUserForm'
                    disabled={disabledEmail}
                    error={emailError}
                    list={usersList}
                    setValue={setEmail}
                />
            </form>
            <ButtonMedium
                type='submit'
                disabled={disabled}
                form='addUserForm'
            >
                Пригласить участника
            </ButtonMedium>
        </div>
    )
}

export default CreateParticipantModal