import React, { useEffect, useState } from 'react'
import classes from '../OrderDetails.module.css'
import Input from '../../../../../../UI/inputs/Input'
import ButtonMedium from '../../../../../../UI/buttons/buttonMedium/ButtonMedium'
import apiService from '../../../../../../../api/api'
import { useLocation } from 'react-router-dom'

const EditNameModal = ({currentGroupName, isPerfomance, currentPerfomanceName, socketHandler, setShowModal}) => {
    const [groupName, setGroupName] = useState(currentGroupName)
    const [perfomanceName, setPerfomanceName] = useState(currentPerfomanceName)
    const [disabled, setDisabled] = useState(true)
    const location = useLocation()

    const groupNameHandler = (event) => {
        setGroupName(event.target.value)
    }

    const perfomanceNameHandler = (event) => {
        setPerfomanceName(event.target.value)
    }

    const submitHandler = async (event) => {
        event.preventDefault()
        if (!disabled) {
            socketHandler('editOrder', {groupName: groupName, perfomanceName: perfomanceName})
            setShowModal(false)
        }
    }

    useEffect(() => {
        if (groupName) {
            setDisabled(false)
        }
        else {
            setDisabled(true)
        }
    }, [groupName])

    return (
        <div className={classes.ModalContent}>
            <h3>Новое название</h3>
            <form id='newNameFrom' onSubmit={submitHandler}>
                <Input
                    inputName='Название коллектива'
                    onChange={groupNameHandler} value={groupName}
                    form='newNameFrom'
                />
                {
                    isPerfomance ? 
                    <Input
                        inputName='Название номера'
                        onChange={perfomanceNameHandler} value={perfomanceName}
                        form='newNameFrom'
                    /> :
                    null
                }
                
            </form>
            <ButtonMedium
                type='submit'
                form='newNameFrom'
                disabled={disabled}
                color='yellow'
            >
                Сохранить
            </ButtonMedium>
        </div>
    )
}

export default EditNameModal