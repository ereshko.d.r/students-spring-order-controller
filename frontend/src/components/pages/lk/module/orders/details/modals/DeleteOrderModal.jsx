import React, { useEffect, useState } from 'react'
import classes from '../OrderDetails.module.css'
import Input from '../../../../../../UI/inputs/Input'
import ButtonSmall from '../../../../../../UI/buttons/buttonSmall/BurronSmall'
import apiService from '../../../../../../../api/api'
import { useLocation, useNavigate } from 'react-router-dom'
import ButtonMedium from '../../../../../../UI/buttons/buttonMedium/ButtonMedium'

const DeleteOrderModal = ({setShowModal, socketHandler}) => {
    const [password, setPassword] = useState('')
    const [disabled, setDisabled] = useState(true)
    const [error, setError] = useState(false)
    const location = useLocation()
    const navigate = useNavigate()
    
    const passwordHandler = (event) => {
        setPassword(event.target.value)
        setError(false)
    }

    const submitHandler = async (event) => {
        event.preventDefault()
        const id = location.pathname.split('/')[2]
        const body = {
            password: password
        }
        socketHandler('deleteOrder')
    }

    useEffect(() => {
        if (password) {
            setDisabled(false)
        }
        else {
            setDisabled(true)
        }
    }, [password])

    return (
        <div className={classes.ModalContent}>
            <h3>Удалить заявку?</h3>
            {/* <h2>Вы уверены?</h2> */}
            {/* <form id='deleteForm' onSubmit={submitHandler}>
                <Input
                    isPass inputName={'Пароль'}
                    value={password} onChange={passwordHandler}
                    error={error}
                />
            </form> */}
            <ButtonMedium
                // type='submit'
                onClick={submitHandler}
                form='deleteForm'
                // disabled={disabled}
                color={'red'}
            >
                Удалить
            </ButtonMedium>
        </div>
    )
}

export default DeleteOrderModal