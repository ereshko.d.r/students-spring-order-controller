import React, { useRef, useState } from 'react'
import classes from './OrderDetails.module.css'

import StatusbarSmall from '../../../../../UI/statusbars/statusbarSmall/StatusbarSmall'
import ButtonImage from '../../../../../UI/buttons/buttonImage/ButtonImage'

import MenuDots from '../../../../../../static/media/menudots.svg'
import ButtonSmall from '../../../../../UI/buttons/buttonSmall/BurronSmall'
import useOutsideAlerter from '../../../../../hooks/useOutsideHook'
import AvatarMini from '../../../../../UI/avatar/avatarmini/AvatarMini'

const DetailsRow = ({ participant, isDirector, socketHandler}) => {
    const [mousePosition, setMousePosition] = useState({x: 0, y: 0})
    const [showParams, setShowParams] = useState(false)

    const handlerButton = (e) => {
        setMousePosition(() => {return {x: e.clientX, y: e.clientY}})
        setShowParams(currentState => {return currentState ? false : true})
    }

    const participantStatus = () => {
        if (participant.status === true) {
            return <StatusbarSmall color={'blue'}>Допущен</StatusbarSmall>
        }
        else if (participant.status === false) {
            const status = []
            if (participant.passport) {
                status.push('Невалидный паспорт')
            }
            if (participant.bage) {
                status.push('Невалидный бейдж')
            }
            return (
                <StatusbarSmall
                color={'red'} status={status}
                >
                    Не допущен
                </StatusbarSmall>
            )
        }
        else {
            return <StatusbarSmall>Проверяется</StatusbarSmall>
        }
    }

    const ParamsWindow = ({showParams, setShowParams, style, participant}) => {
        const clickHandler = () => {
            setShowParams(false)
        }

        const ref = useRef(null)
        useOutsideAlerter(ref, clickHandler)

        const removeHandler = async () => {
            const userId = participant.id
            socketHandler('removeUser', {userId})
            setShowParams(false)
        }
        const changeRole = async () => {
            const userId = participant.id
            const roleId = participant.orderRole === 'Участник' ? 3 : 2
            socketHandler('changeRole', {userId, roleId})
            setShowParams(false)
        }
        const changeDirector = async () => {
            const userId = participant.id
            socketHandler('changeDirector', {userId})
            setShowParams(false)
        }
        return (
            <div hidden={!showParams}>
                <div
                    className={classes.ParamsWindow}
                    style={style}
                    ref={ref}
                >
                    <ButtonSmall
                        color={'red'}
                        onClick={removeHandler}
                    >
                        Исключить
                    </ButtonSmall>
                    <ButtonSmall
                        onClick={changeRole}
                    >
                        {
                            participant.orderRole === 'Участник' ?
                            'Тех. персонал' :
                            'Участник'
                        }
                    </ButtonSmall>
                    {
                        participant.isJoin ?
                        <ButtonSmall onClick={changeDirector}>
                            Передать руководство
                        </ButtonSmall>
                        : null
                    }
                </div>
            </div>
        )
    }
    const fullName = `${participant.lastName} ${participant.firstName} ${participant.secondName}`
    return (
        <div className={classes.ParticipantRowWrapper}>
        <div className={classes.ParticipantRow}>
            <AvatarMini
                avatar={participant.isJoin ? participant.avatar : null}
            />
            <div>{fullName}</div>
            <div>{participant.orderRole}</div>
            <div>
                {
                    participant.isJoin ? 
                    participantStatus() :
                    <StatusbarSmall>Приглашен</StatusbarSmall>
                }
            </div>
            {
                isDirector ? 
                participant.orderRole !== 'Руководитель' ?
                <ButtonImage
                onClick={handlerButton}
                >
                    {MenuDots}
                </ButtonImage> :
                null :
                null
            }
        </div>
            <ParamsWindow
                showParams={showParams}
                setShowParams={setShowParams}
                participant={participant}
                style={{
                    left: mousePosition.x + 30,
                    top: mousePosition.y - 89
                }}
            />
        </div>
    )
}

export default DetailsRow