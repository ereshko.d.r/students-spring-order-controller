import React, { useEffect, useState, useRef, useContext } from 'react'
import classes from './OrderDetails.module.css'
import { useLocation, useNavigate } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import { io } from 'socket.io-client'
import AuthService from '../../../../../../api/auth'
import { URL } from '../../../../../../App'

import UserContext from '../../../../../context/userContext'
import CreateParticipantModal from './modals/CreateParticipantModal'
import DetailsRow from './DetailsRow'
import ModalWindow from '../../../../../UI/modalwindow/ModalWindow'
import EditNameModal from './modals/EditNameModal'

import ButtonMedium from '../../../../../UI/buttons/buttonMedium/ButtonMedium'
import StatusbarMedium from '../../../../../UI/statusbars/statusbarMedium/StatusbarMedium'
import DeleteOrderModal from './modals/DeleteOrderModal'
import ArrowBack from '../../../../../../static/media/arrowback.svg'
import ButtonImage from '../../../../../UI/buttons/buttonImage/ButtonImage'

const authService = new AuthService()

const OrderDetails = observer(() => {
    const [showModal, setShowModal] = useState(false)
    const [modalChildren, setModalChildren] = useState(<CreateParticipantModal />)
    const [participants, setParticipants] = useState([])
    const [groupName, setGroupName] = useState('')
    const [perfomanceName, setPerfomanceName] = useState('')
    const [nominationName, setNominationName] = useState('')
    const [isDirector, setIsDirector] = useState(false)
    const [isPerfomance, setIsPerfomance] = useState(false)
    const [status, setStatus] = useState()
    const [orderIsSent, setOrderIsSent] = useState(null)
    const [groupNameReason, setGroupNameReason] = useState(null)
    const [perfomanceNameReason, setPerfomanceNameReason] = useState(null)

    const {user} = useContext(UserContext)
    const [hasRejectedParticipants, setHasRejectedParticipants] = useState(false)
    const [isReady, setIsReady] = useState(false)

    const websocket = useRef(null)
    const location = useLocation()
    const navigate = useNavigate()

    const orderId = location.pathname.split('/')[2]

    const {headers} = authService.authHeader
    

    const websocketHandler = async () => {
        if (!websocket.current || !websocket.current.connected) {
            websocket.current = io(URL, {
                query: {orderId},
                auth: headers,
                autoConnect: false
            })
            websocket.current.connect()
            websocket.current.on('connect',() => {
                websocket.current.emit('orders', 'getOrder')
            })
            websocket.current.on('disconnect', () => {
                navigate('/orders')
            })
            websocket.current.on('test', (data) => {
                console.log('Received test event:', data)})
            websocket.current.on('connect_error', (error) => {
                console.log(error)
                if (error?.data?.reason === 401) {
                    if(authService.refreshAccessToken()) {
                        const {headers} = authService.authHeader
                        websocket.current.auth = headers
                        websocket.current.connect()
                    }
                    else {
                        navigate('/auth')
                    }
                }
                else if (error?.data?.reason === 404) {
                    navigate('/orders')
                }
            })

            websocket.current.on('error', (error) => {
                console.log(error)
                if (error === 404) {
                    navigate('/orders')
                }
            })

            websocket.current.on('orders:orderState', (data) => {
                setGroupName(data.groupName)
                setPerfomanceName(data.perfomanceName)
                checkIsDirector(data.participants)
                setParticipants(data.participants)
                setNominationName(data.nominationName)
                setIsPerfomance(data.isPerfomance)
                setStatus(data.orderStatus)
                setOrderIsSent(data.orderIsSent)
                setGroupNameReason(data.statusName?.reason)
                setPerfomanceNameReason(data.perfomanceName?.reason)
            })

            websocket.current.on('orders:updateName', (data) => {
                setGroupName(data.groupName)
                setPerfomanceName(data.perfomanceName)
            })
            websocket.current.on('orders:selfUpdate', () => {
                websocket.current.emit('orders', 'getOrder')
            })
            websocket.current.onAny((event) => console.log(event))
        }
    }

    const sendHandler = (data, args) => {
        websocket.current.connect()
        websocket.current.emit('orders', data, args)
    }

    const checkIsDirector = (participants) => {
        const currentUser = participants.find((obj) => obj.id === user.id)
        if (currentUser?.orderRole === 'Руководитель') {
            setIsDirector(true)
        }
        else {
            setIsDirector(false)
        }
    }

    const OrderStatusComponent = observer(() => {
        if (orderIsSent || status === false) {
            if (status === true) {
                return (
                    <StatusbarMedium
                        color={'blue'}
                    >
                        Заявка допущена
                    </StatusbarMedium>
                )
            }
            else if (status === false) {
                const reasonList = []
                if (groupNameReason) {
                    reasonList.push(groupNameReason)
                }
                if (perfomanceNameReason) {
                    reasonList.push(perfomanceNameReason)
                }
                return (
                    <StatusbarMedium
                        color={'red'}
                        status={reasonList}
                    >
                        Заявка не допущена
                    </StatusbarMedium>
                )
            }
            else {
                return (
                    <StatusbarMedium>
                        Заявка отправлена
                    </StatusbarMedium>
                )
            }
        }
        else {
            return (
                <StatusbarMedium>
                    Заявка формируется
                </StatusbarMedium>
            )
        }
    })

    const editHandler = () => {
        setModalChildren(
            <EditNameModal
                isPerfomance={isPerfomance}
                currentGroupName={groupName}
                socketHandler={sendHandler}
                currentPerfomanceName={perfomanceName}
                setShowModal={setShowModal}
            />
        ) 
        setShowModal((currentState) => currentState ? false : true)
    }

    const addHandler = () => {
        setModalChildren(
            <CreateParticipantModal
                socketHandler={sendHandler}
                setShowModal={setShowModal}
            />
        )
        setShowModal((currentState) => currentState ? false : true)
    }

    const deleteHandler = () => {
        setModalChildren(
            <DeleteOrderModal
                socketHandler={sendHandler}
                setShowModal={setShowModal}
            />
        )
        setShowModal((currentState) => currentState ? false : true)
    }

    const sendOrderHandler = () => {
        sendHandler('sendOrder')
    }

    const leaveHandler = async () => {
        sendHandler('leaveOrder')
    }

    const ParticipantsStatusOrderComponent = observer(() => {
        const acceptedParticipants = participants.reduce((sum, participant) => {
            if (participant.isJoin === true) {
                if (participant.status === true) {
                    sum++
                }
                else if (participant.status === false) {
                    setHasRejectedParticipants(true)
                }
            }
            return sum
        }, 0)
        if (acceptedParticipants === participants.length) {
            setIsReady(true)
            return (
                <StatusbarMedium
                    color={'blue'}
                >
                    {acceptedParticipants}/{participants.length}
                </StatusbarMedium>
            )
        }
        else if (hasRejectedParticipants) {
            setIsReady(false)
            return (
                <StatusbarMedium
                    color={'red'}
                >
                    {acceptedParticipants}/{participants.length}
                </StatusbarMedium>
            )
        }
        else {
            setIsReady(false)
            return (
                <StatusbarMedium>
                    {acceptedParticipants}/{participants.length}
                </StatusbarMedium>
            )
        }
    })

    const Header = observer(() => {
        return (
            <div className={classes.OrderHeader}>
                <div>
                    <ButtonImage
                        onClick={() => navigate('/orders')}
                    >{ArrowBack}</ButtonImage>
                </div>
                <div className={classes.Nomination}>
                    <h4>Номинация</h4>
                    <h3>{ nominationName }</h3>
                </div>
                {
                    orderIsSent ? 
                    null :
                    isDirector ?
                    <ButtonMedium
                        onClick={deleteHandler}
                    >
                        Удалить заявку
                    </ButtonMedium> :
                    <ButtonMedium
                        onClick={leaveHandler}
                    >
                        Покинуть заявку
                    </ButtonMedium>
                }
                
            </div>
        )
    })

    const GroupInfo = observer(() => {
        return (
            <div className={classes.GroupInfo}>
                <div>
                    <div>
                        <h4>Название коллектива</h4>
                        <h3>{groupName}</h3>
                    </div>
                    {
                        isPerfomance ? 
                        <div>
                            <h4>Название номера</h4>
                            <h3>{perfomanceName}</h3>
                        </div> :
                        null
                    }
                </div>
                <div className={classes.OrderStatus}>
                    <OrderStatusComponent />
                </div>
            </div>
        )
    })

    const Content = observer(() => {
        return (
            <>
                <div className={classes.OrderContent}>
                    <div className={classes.ParticipantList}>
                        {participants?.map((participant, index) => {
                            return (
                                <DetailsRow
                                    participant={participant}
                                    socketHandler={sendHandler}
                                    isDirector={isDirector}
                                    key={index}
                                />
                            )
                        })}
                    </div>
                </div>
                
            </>
        )
    })

    const Footer = observer(() => {
        return (
            <div className={classes.StatusBar}>
                <div style={{width: 100}}>
                    <ParticipantsStatusOrderComponent />
                </div>
                {
                    isDirector ?
                    <ButtonMedium
                        color={'yellow'}
                        onClick={addHandler}
                        disabled={orderIsSent}
                    >
                        + Добавить участника
                    </ButtonMedium> :
                    null
                }
                {
                    isDirector ?
                    <ButtonMedium
                        onClick={editHandler}
                        disabled={orderIsSent}
                    >
                        Редактировать
                    </ButtonMedium> :
                    null
                }
                {
                    isDirector ?
                    <ButtonMedium
                        color={'yellow'}
                        disabled={!isReady || orderIsSent}
                        onClick={sendOrderHandler}
                    >
                        Отправить заявку
                    </ButtonMedium> :
                    null
                }
            </div>
        )
    })

    useEffect(() => {
        if (!websocket.current || !websocket.current.connected) {
            websocketHandler()
        }
        return () => {
            websocket.current.disconnect()
        }
    }, [])

    return (
        <>
            <ModalWindow showModal={showModal} setShowModal={setShowModal}>
                {modalChildren}
            </ModalWindow>
            <div className={classes.OrderDetails}>
                <div className={classes.OrderDetailsContent}>
                    <Header />
                    <GroupInfo />
                    <Content />
                </div>
                <Footer />  
            </div>
        </>
    )
})

export default OrderDetails

