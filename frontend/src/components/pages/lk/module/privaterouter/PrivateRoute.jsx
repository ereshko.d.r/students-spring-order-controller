import React, { useContext, useEffect, useRef, useState } from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import apiService from '../../../../../api/api'
import eventSourceClient from '../../../../../api/eventSourceClient'

import UserContext from '../../../../context/userContext'
import OrderInvitesContext from '../../../../context/orderInviteContext'
import AuthService from '../../../../../api/auth'

const authService = new AuthService()

const PrivateRoute = observer(() => {
    const {user} = useContext(UserContext)
    const userContext = useContext(UserContext)
    const ordersInvites = useContext(OrderInvitesContext)
    const [subscribe, setSubscribe] = useState(false)
    const navigate = useNavigate()

    const events = useRef(null)

    const subscribeHandler = () => {
        const connectionTime = 10 * 60 * 1000
        const {headers} = authService.authHeader
        events.current = eventSourceClient.subscribe({
            headers: headers,
            heartbeatTimeout: connectionTime
            })
        events.current.onmessage = (event) => {
            const data = JSON.parse(event.data)
            if (data.orders) {
                ordersInvites.setOrders(data.orders)
            }
            if (data.invites) {
                ordersInvites.setInvites(data.invites)
            }
            if (data.state) {
                userContext.setUser(data.state)
            }
        }

        events.current.onopen = () => {
            setSubscribe(true)
        }

        events.current.onerror = (error) => {
            if (error.status === 401) {
                if (authService.refreshAccessToken()) {
                    subscribeHandler()
                }
                else {
                    navigate('/auth')
                }
            }
            setSubscribe(false)
            navigate('/orders')
        }
    }

    useEffect(() => {
        if (!user) {
            apiService.getUserData()
                .then(response => {
                    if (response.status === 404) {
                        userContext.setUser(undefined)
                        return navigate('/registration')
                    }
                    if (response.status === 401) {
                        userContext.setUser(undefined)
                        return navigate('/auth')
                    }
                    userContext.setUser(response.data)
                })
        }
        if (!subscribe) {
            subscribeHandler()
        }
        return () => {
            eventSourceClient.unsubscribe()
            events.current = null
            setSubscribe(false)
        }
    }, [])

    return (
        <>
            {user ? <Outlet /> : <></>}
        </>
    )
})

export default PrivateRoute