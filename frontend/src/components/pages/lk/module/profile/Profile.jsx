import React, { useContext, useState } from 'react'
import classes from './Profile.module.css'
import { observer } from 'mobx-react-lite'
import UserContext from '../../../../context/userContext'

import PassportHandler from '../../../../modules/imageHandler/PassportHandler'
import PhotoHandler from '../../../../modules/imageHandler/PhotoHandler'
import Draganddrop from '../../../../modules/draganddrop/Draganddrop'
import AvatarLarge from '../../../../UI/avatar/avatarlarge/AvatarLarge'
import apiService from '../../../../../api/api'
import PassportModal from '../../../registration/module/PassportModal'
import BageModal from '../../../registration/module/BageModal'

import StatusbarMedium from '../../../../UI/statusbars/statusbarMedium/StatusbarMedium'
import ButtonMedium from '../../../../UI/buttons/buttonMedium/ButtonMedium'
import ModalWindow from '../../../../UI/modalwindow/ModalWindow'



const Profile = observer(() => {
    const { user } = useContext(UserContext)
    const [showModal, setShowModal] = useState(false)
    const [modalContent, setModalContent] = useState(<></>)

    const getPassportStatus = () => {
        const passState = []
        const passportStatus = user.passportStatus
        if (passportStatus?.isPersonMatched) {
            passState.push(passportStatus.isPersonMatched)
        }
        if (passportStatus?.isNameMatched) {
            passState.push(passportStatus.isNameMatched)
        }
        return passState
    }

    const getBageStatus = () => {
        const bageState = []
        const bageStatus = user.bageStatus
        if (bageStatus?.isPhotoMatched) {
            bageState.push(bageStatus.isPhotoMatched)
        }
        if (bageStatus?.isNormalPhoto) {
            bageState.push(bageStatus.isNormalPhoto)
        }
        return bageState
    }

    const getStatus = () => {
        switch (user.status) {
            case true:
                return {
                    color: 'blue',
                    children: 'Допущен'
                }
            case false:
                return {
                    color: 'red',
                    children: 'Не допущен'
                }
            default:
                return {
                    color: 'grey',
                    children: 'Проверяется'
                }
        }
    }

    const PassUpdateModal = () => {
        const [passImage, setPassImage] = useState(null)
        return (
            <>
                {
                    passImage ?
                    <PassportHandler 
                        image={passImage}
                        setImage={setPassImage}
                        setPassData={checkPassModal}
                        setShowModal={() => {}}
                    /> :
                    <Draganddrop setImage={setPassImage} />
                }
            </>
        )
    }

    const BageUpdateModal = () => {
        const [bageImage, setBageImage] = useState(null)
        return (
            <>
                {
                    bageImage ?
                    <PhotoHandler 
                        image={bageImage}
                        setBage={checkBageModal}
                        setShowModal={setShowModal}
                    /> :
                    <Draganddrop setImage={setBageImage} />
                }
            </>
        )
    }

    const acceptPassHandler = async (passData) => {
        const body = {
            firstName: passData.firstName,
            lastName: passData.lastName,
            secondName: passData.secondName,
            gender: (passData.gender.toLowerCase() === 'муж' ? 'male' : 'female'),
            serialPass: passData.serialPass,
            numberPass: passData.numberPass,
            photoPass: passData.photoPass,
            photoFirstName: passData.photoFirstName,
            photoLastName: passData.photoLastName,
            photoSecondName: passData.photoSecondName
        }
        const response = await apiService.updatePassData(body)
        if (response.status === 200) {
            setShowModal(false)
        }
    }

    const acceptBageHandler = async (bageData) => {
        const body = {
            bage: bageData.bage,
            avatar: bageData.avatar
        }
        const response = await apiService.updateBageData(body)
        console.log(response)
        if (response.status === 200) {
            setShowModal(false)
        }
    }

    const checkPassModal = (data) => {
        setModalContent(
            <PassportModal
                data={data} setShowModal={setShowModal}
                acceptHandler={() => acceptPassHandler(data)}
                rejectHandler={newPassHandler}
            />
        )
    }

    const checkBageModal = (data) => {
        setModalContent(
            <BageModal
                bage={data} setShowModal={setShowModal}
                submitHandler={() => acceptBageHandler(data)}
            />
        )
    }

    const newPassHandler = () => {
        setModalContent(<PassUpdateModal />)
        setShowModal(true)
    }

    const newBageHandler = () => {
        setModalContent(<BageUpdateModal />)
        setShowModal(true)
    }

    const PassportComponent = observer(() => {
        const passState = getPassportStatus()
        return (
            <div className={classes.Status}>
                <h3>Паспорт</h3>
                <div style={{width: 200}}>
                    {
                        passState.length === 0 && user.status === null ?
                        <StatusbarMedium>Проверяется</StatusbarMedium> :
                        user.status === true ? 
                        <StatusbarMedium color={'blue'}>Допущен</StatusbarMedium> :
                        user.status === false && passState.length > 0 ?
                        <StatusbarMedium color={'red'}>Не допущен</StatusbarMedium> :
                        <StatusbarMedium>Проверяется</StatusbarMedium>
                    }
                </div>
                {
                    passState.length !== 0 ?
                    passState.map((state, index) => {
                        return <li key={index}>{state}</li>
                    }) :
                    null
                }
                {
                    passState.length > 0 ?
                    <div>
                        <ButtonMedium onClick={newPassHandler}>Обновить паспорт</ButtonMedium>
                    </div> :
                    null
                }
            </div>
        )
    })

    const BageComponent = observer(() => {
        const bageState = getBageStatus()
        return (
            <div className={classes.Status}>
                <h3>Бейдж</h3>
                <div style={{width: 200}}>
                    {
                        bageState.length === 0 && user.status === null ?
                        <StatusbarMedium>Проверяется</StatusbarMedium> :
                        user.status === true ? 
                        <StatusbarMedium color={'blue'}>Допущен</StatusbarMedium> :
                        bageState.length > 0 ?
                        <StatusbarMedium color={'red'}>Не допущен</StatusbarMedium> :
                        <StatusbarMedium>Проверяется</StatusbarMedium>
                    }
                </div>
                {
                    bageState.length !== 0 ?
                    bageState.map((state, index) => {
                        return <li key={index}>{state}</li>
                    }) :
                    null
                }
                {
                    bageState.length > 0 ?
                    <div>
                        <ButtonMedium onClick={newBageHandler}>Обновить бейдж</ButtonMedium>
                    </div> :
                    null
                }
            </div>
        )
    })

    return (
        <div className={classes.ProfileContainer}>
            <ModalWindow showModal={showModal} setShowModal={setShowModal}>
                <div className={classes.ModalContent}>
                    {modalContent}
                </div>
            </ModalWindow>
            <div className={classes.Profile}>
                <div className={classes.User}>
                    <div className={classes.UserInfo}>
                        <AvatarLarge avatar={user.avatar} firstName={user.firstName} lastName={user.lastName}/>
                        <div style={{width: 200}}>
                            <StatusbarMedium {...getStatus()}/>
                        </div>
                    </div>
                </div>
                <div className={classes.CheckStatus}>
                    <PassportComponent />
                    <BageComponent />
                </div>
            </div>
        </div>
    )
})

export default Profile