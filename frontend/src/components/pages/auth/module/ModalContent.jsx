import React, { useState, useEffect } from 'react'
import classes from './ModalContent.module.css'

import ButtonMedium from '../../../UI/buttons/buttonMedium/ButtonMedium'
import Input from '../../../UI/inputs/Input'

const ModalContent = ({ setShowModal }) => {
    const [sentEmail, setSentEmail] = useState(false)
    const [email, setEmail] = useState('')
    const [disabled, setDisabled] = useState()
    
    const emailHandler = (event) => {
        setEmail(event.target.value)
    }

    const submitHandler = (event) => {
        event.preventDefault()
        setSentEmail(true)

    }

    useEffect(() => {
        if (!email) {
            setDisabled(true)
        }
        else {
            setDisabled(false)
        }
    }, [email])

    return (
        <div className={classes.ModalContent}>
            {
                !sentEmail ?
                <>
                    <h3>Введите email</h3>
                    <form onSubmit={submitHandler} id='remindForm'>
                        <Input
                            value={email}
                            onChange={emailHandler}
                            inputName={'Email'}
                        />
                    </form>
                    <ButtonMedium
                        type='submit' form='remindForm'
                        disabled={disabled}
                    >
                        Отправить
                    </ButtonMedium>
                </> :
                <>
                    <h3>Успешно</h3>
                    <p>Инструкция была отправлена на email</p>
                    <p>{email}</p>
                    <ButtonMedium onClick={() => {
                        setShowModal(false)
                        setSentEmail(false)
                        setEmail('')
                    }}>
                        Отлично!
                    </ButtonMedium>
                </>
            }
        </div>
    )
}

export default ModalContent