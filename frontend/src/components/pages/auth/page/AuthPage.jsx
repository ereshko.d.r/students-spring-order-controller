import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import classes from './AuthPage.module.css'
import AuthService from '../../../../api/auth'

import ModalContent from '../module/ModalContent'
import ModalWindow from '../../../UI/modalwindow/ModalWindow'

import ButtonLarge from '../../../UI/buttons/buttonLarge/ButtonLarge'
import Input from '../../../UI/inputs/Input'
import Logo from '../../../../static/media/bigcat.svg'


const authService = new AuthService()

const AuthPage = () => {
    const [showModal, setShowModal] = useState(false)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [disabled, setDisabled] = useState(false)
    const [errorEmail, setErrorEmail] = useState(false)
    const [errorPass, setErrorPass] = useState(false)
    const navigate = useNavigate()

    const emailHandler = (event) => {
        setEmail(event.target.value)
        setErrorEmail(false)
    }

    const passwordHandler = (event) => {
        setPassword(event.target.value)
        setErrorPass(false)
    }
    
    const submitHandler = async (event) => {
        event.preventDefault()
        if (!disabled) {
            const response = await authService.authorization(email, password)
            console.log(response)
            if (response.status === 404) {
                setErrorEmail('Неверный email')
                setErrorPass('Неверный пароль')
            }
            else {
                navigate('/me')
            }
        }
    }

    useEffect(() => {
        if (email && password) {
            setDisabled(false)
        }
        else {
            setDisabled(true)
        }
    }, [email, password])

    return (
        <>
            <ModalWindow showModal={showModal} setShowModal={setShowModal}>
                <ModalContent setShowModal={setShowModal} />
            </ModalWindow>
            <div className={classes.AuthPageContainer}>
                <img src={Logo} alt='' />
                <div className={classes.AuthPageContent}>
                    <h3>Авторизация</h3>
                    <form onSubmit={submitHandler} id='AuthForm'>
                        <Input
                            inputName={'Email'} error={errorEmail}
                            value={email} onChange={emailHandler}
                            form='AuthForm'
                        />
                        <Input
                            inputName={'Пароль'} isPass error={errorPass}
                            value={password} onChange={passwordHandler}
                            form='AuthForm'
                        />
                    </form>
                    <div className={classes.ForgotPassword} onClick={() => setShowModal(true)}>Забыл пароль</div>
                    <ButtonLarge
                        color={'yellow'}
                        disabled={disabled} type="submit" form='AuthForm'
                    >
                        Войти
                    </ButtonLarge>
                </div>
            </div>
        </>
    )
}

export default AuthPage