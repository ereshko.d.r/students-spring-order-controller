class PasswordValidator {
    /**
     * 
     * @param {string} pass 
     * @returns {boolean}
     */

    lenght(pass) {
        return pass.length >= 8
    }

    /**
     * 
     * @param {string} pass 
     * @returns {boolean}
     */
    hasUpperAndLowerCase(pass) {
        const expression = /^(?=.*[a-z])(?=.*[A-Z]).+$/
        return expression.test(pass)
    }

    /**
     * 
     * @param {string} pass 
     * @returns {boolean}
     */
    hasSpecialSymbols(pass) {
        const expression = /[^A-za-z]/
        return expression.test(pass)
    }

    /**
     * 
     * @param {string} firstPass 
     * @param {string} secondPass 
     * @returns {boolean}
     */
    passwordsMatch(firstPass, secondPass) {
        return (firstPass.length !== 0 && firstPass === secondPass)
    }
}

export default PasswordValidator