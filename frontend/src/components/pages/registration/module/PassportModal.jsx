import React, { useEffect, useState } from 'react'
import classes from './ModalContent.module.css'

import ButtonMedium from '../../../UI/buttons/buttonMedium/ButtonMedium'
import Input from '../../../UI/inputs/Input'

const PassportModal = ({ data, acceptHandler, rejectHandler }) => {
    const [disabled, setDisabled] = useState(false)
    const [serialError, setSerialError] = useState(false)
    const [numberError, setNumberError] = useState(false)
    
    useEffect(() => {
        if (data?.serialPass?.length !== 4) {
            setSerialError('Невалидное значение')
        }
        else {
            setSerialError(false)
        }
        if (data?.numberPass?.length !== 6) {
            setNumberError('Невалидное значение')
        }
        else {
            setNumberError(false)
        }
        if (serialError || numberError) {
            setDisabled(true)
        }
        else {
            setDisabled(false)
        }
    }, [data, serialError, numberError])

    return (
        <div className={classes.ModalContent}>
            <h3>Паспортные данные верны?</h3>
            <div className={classes.GridColumn}>
                <Input
                    value={data.firstName}
                    inputName={'Имя'} disabled
                />
                <Input
                    value={data.lastName}
                    inputName={'Фамилия'} disabled
                />
                <Input
                    value={data.secondName}
                    inputName={'Отчество'} disabled
                />
                <Input
                    value={data.gender}
                    inputName={'Пол'} disabled
                />
                <Input
                    value={data.serialPass}
                    inputName={'Серия паспорта'} disabled
                    error={serialError}
                />
                <Input
                    value={data.numberPass}
                    inputName={'Номер паспорта'} disabled
                    error={numberError}
                />
            </div>
                {
                    (serialError || numberError) ?
                    <div>
                        Проверьте, что серия и
                        номер чётко видны и находятся в зоне
                    </div> :
                    null
                }
            <div className={classes.ModalButtons}>
                <ButtonMedium
                    color={'red'} onClick={rejectHandler}
                >
                    Нет
                </ButtonMedium>
                <ButtonMedium
                    onClick={acceptHandler}
                    disabled={disabled}
                >
                    Да
                </ButtonMedium>
            </div>
        </div>
    )
}

export default PassportModal