import React from 'react'
import classes from './ModalContent.module.css'

import ButtonMedium from '../../../UI/buttons/buttonMedium/ButtonMedium'

const BageModal = ({ bage, submitHandler, setShowModal }) => {
    return (
        <div className={classes.ModalContent}>
            <h3>Нравится?</h3>
            <div className={classes.BagePreview}>
                <img src={bage.bage} alt='' />
            </div>
            <div className={classes.ModalButtons}>
                <ButtonMedium
                    color={'red'}
                    onClick={() => setShowModal(false)}
                >
                    Нет
                </ButtonMedium>
                <ButtonMedium onClick={submitHandler}>Да</ButtonMedium>
            </div>
        </div>
    )
}

export default BageModal