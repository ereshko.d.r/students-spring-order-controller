import React, { useState } from 'react'
import classes from './RegistrationUser.module.css'

import apiService from '../../../../api/api'

import BageModal from '../module/BageModal'
import Draganddrop from '../../../modules/draganddrop/Draganddrop'
import PhotoHandler from '../../../modules/imageHandler/PhotoHandler'
import ModalWindow from '../../../UI/modalwindow/ModalWindow'

const RegistrationBage = ({ getStateHandler }) => {
    const [image, setImage] = useState()
    const [bage, setBage] = useState(false)
    const [showModal, setShowModal] = useState(false)

    const submitHandler = async (event) => {
        event.preventDefault()
        const body = {
            bage: bage.bage,
            avatar: bage.avatar
        }
        const response = await apiService.RegistrationBage(body)
        if (response.status === 201) {
            getStateHandler()
        }
    }

    return (
        <>
        <ModalWindow showModal={showModal} setShowModal={setShowModal}>
            <BageModal
                bage={bage} setShowModal={setShowModal}
                submitHandler={submitHandler}
            />
        </ModalWindow>
        <div className={classes.RegistrationWrapper}>
            <div className={classes.RegistrationBageContent}>
                <div className={classes.RegistrationBageHeader}>
                    <h3>Регистрация</h3>
                    <div>Фото на бейдж</div>
                </div>
                {
                    image ?
                    <PhotoHandler 
                        image={image} setImage={setImage}
                        setBage={setBage} setShowModal={setShowModal}
                    /> :
                    <Draganddrop setImage={setImage}/>
                }
            </div>
        </div>
        </>
    )
}

export default RegistrationBage