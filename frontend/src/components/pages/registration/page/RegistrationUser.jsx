import React, { useEffect, useState } from 'react'
import classes from './RegistrationUser.module.css'
import Draganddrop from '../../../modules/draganddrop/Draganddrop'

import apiService from '../../../../api/api'

import Input from '../../../UI/inputs/Input'
import GenderRadiobutton from '../../../UI/radiobuttons/GenderRadiobutton'
import ButtonMedium from '../../../UI/buttons/buttonMedium/ButtonMedium'
import PassportHandler from '../../../modules/imageHandler/PassportHandler'
import ModalWindow from '../../../UI/modalwindow/ModalWindow'
import PassportModal from '../module/PassportModal'
import PassportContext, { passport } from '../../../context/passportContext'

const RegistrationUser = ({ getStateHandler }) => {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [secondName, setSecondName] = useState('')
    const [telegram, setTelegram] = useState('')
    const [gender, setGender] = useState()
    const [disabled, setDisabled] = useState(true)
    const [image, setImage] = useState(null)
    const [showModal, setShowModal] = useState(false)
    const [passData, setPassData] = useState(false)

    const [firstNameError, setFirstNameError] = useState(false)
    const [lastNameError, setLastNameError] = useState(false)
    const [secondNameError, setSecondNameError] = useState(false)
    const [telegramError, setTelegramError] = useState(false)
    const [serialError, setSerialError] = useState(false)
    const [numberError, setNumberError] = useState(false)

    const firstNameHandler = (event) => {
        setFirstName(event.target.value)
        setFirstNameError(false)
    }

    const lastNameHandler = (event) => {
        setLastName(event.target.value)
        setLastNameError(false)
    }

    const secondNameHandler = (event) => {
        setSecondName(event.target.value)
        setSecondNameError(false)
    }

    const telegramHandler = (event) => {
        setTelegram(event.target.value)
        setTelegramError(false)
    }

    const submitHandler = async (event) => {
        event.preventDefault()
        if(!disabled) {
            const body = {
                firstName: firstName,
                lastName: lastName,
                secondName: secondName,
                telegram: telegram,
                gender: gender,
                serialPass: passData.serialPass,
                numberPass: passData.numberPass,
                photoPass: passData.photoPass,
                photoFirstName: passData.photoFirstName,
                photoLastName: passData.photoLastName,
                photoSecondName: passData.photoSecondName
            }
            const response = await apiService
                .RegistrationUser(body)
            if (response.status === 400) {
                const fields = Object.keys(response.data.errors)
                if (fields.includes('firstName')) {
                    setFirstNameError(response.data.errors.firstName)
                }
                if (fields.includes('lastName')) {
                    setLastNameError(response.data.errors.lastName)
                }
                if (fields.includes('secondName')) {
                    setSecondNameError(response.data.errors.secondName)
                }
                if (fields.includes('telegram')) {
                    setTelegramError(response.data.errors.telegram)
                }
                if (fields.includes('serialPass')) {
                    setSerialError(response.data.errors.serialPass)
                }
                if (fields.includes('numberPass')) {
                    setNumberError(response.data.errors.numberPass)
                }
            }
            else if (response.status === 201){
                getStateHandler()
            }
        }
    }

    const acceptHandler = () => {
        setFirstName(passData.firstName)
        setLastName(passData.lastName)
        setSecondName(passData.secondName)
        setGender(passData.gender.toLowerCase() === 'муж' ? 'male' : 'female')
        setShowModal(false)
    }

    const rejectHandler = () => {
        setPassData(false)
        setShowModal(false)
    }

    useEffect(() => {
        if (
            firstName && lastName && secondName &&
            gender && telegram && passData
        ) {
            setDisabled(false)
        }
        else {
            setDisabled(true)
        }
    }, [firstName, lastName, secondName, gender, telegram, passData])

    return (
        <PassportContext.Provider value={passport}>

            <ModalWindow showModal={showModal} setShowModal={setShowModal}>
                <PassportModal
                    data={passData} setShowModal={setShowModal}
                    acceptHandler={acceptHandler}
                    rejectHandler={rejectHandler}
                />
            </ModalWindow>
            <div className={classes.RegistrationWrapper}>
                <div className={classes.RegistrationUserContainer}>
                    <div className={classes.PhotoHandler}>
                        <h3>Паспорт</h3>
                        {
                            image ?
                            <PassportHandler
                                image={image} setImage={setImage}
                                setPassData={setPassData}
                                setShowModal={setShowModal}
                            /> :
                            <Draganddrop setImage={setImage} />
                        }
                    </div>
                    <div className={classes.UserForm}>
                        <h3>Данные участника</h3>
                        <form onSubmit={submitHandler} id='regUser'>
                            <Input
                                inputName={'Фамилия'}
                                value={lastName} onChange={lastNameHandler}
                                error={lastNameError} form='regUser'
                            />
                            <Input
                                inputName={'Имя'}
                                value={firstName} onChange={firstNameHandler}
                                error={firstNameError} form='regUser'
                            />
                            <Input
                                inputName={'Отчество'}
                                value={secondName}
                                onChange={secondNameHandler}
                                error={secondNameError} form='regUser'
                            />
                            <Input
                                inputName={'Telegram'}
                                value={telegram} onChange={telegramHandler}
                                error={telegramError} form='regUser'
                            />
                            <GenderRadiobutton
                                gender={gender} setGender={setGender}
                            />
                        </form>
                    </div>
                    <div className={classes.NavBar}>
                        <ButtonMedium
                            disabled={disabled}
                            type='submit' form='regUser'
                        >
                            Далее
                        </ButtonMedium>
                    </div>
                </div>
            </div>
        </PassportContext.Provider>
    )
}

export default RegistrationUser