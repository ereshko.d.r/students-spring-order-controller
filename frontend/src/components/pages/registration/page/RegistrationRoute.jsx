import React, { useContext, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import UserContext from '../../../context/userContext'

import apiService from '../../../../api/api'

import RegistrationAccount from './RegistrationAccount'
import RegistrationUser from './RegistrationUser'
import RegistrationBage from './RegistrationBage'


const RegistrationRoute = observer(() => {
    const [currrentPage, setCurrentPage] = useState()
    const {user} = useContext(UserContext)
    const userState = useContext(UserContext)
    const navigate = useNavigate()

    const getStateHandler = async () => {
        console.log('here again')
        const result = await apiService.getUserState()
        console.log(result)
        if (result?.status === 404) {
            setCurrentPage(
                <RegistrationAccount
                    getStateHandler={getStateHandler}
                />
            )
        }
        else if (result?.status === 401) {
            navigate('/auth')
        }
        else if (result?.status === 200) {
            userState.setUser(result.data)
        }
    }

    useEffect(() => {
        if (!user) {
            getStateHandler()
        }
        else if (!user.id) {
            setCurrentPage(
                <RegistrationAccount
                    getStateHandler={getStateHandler}
                />
            )
        }
        else if (!user.firstName) {
            console.log('update')
            setCurrentPage(
                <RegistrationUser
                    getStateHandler={getStateHandler}
                />
            )
        }
        else if (!user.avatar) {
            setCurrentPage(
                <RegistrationBage
                    getStateHandler={getStateHandler}
                />
            )
        }
        else if (
            user.id && user.firstName &&
            user.avatar
        ) {
            navigate('/orders')
        }
    }, [user])

    return (
        <>
            {currrentPage}
        </>
    )
})

export default RegistrationRoute