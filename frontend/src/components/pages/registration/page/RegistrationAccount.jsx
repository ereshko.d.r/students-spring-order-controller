import React, { useState, useEffect } from 'react'
import classes from './RegistrationUser.module.css'
import PasswordValidator from '../module/validatorPassword'
import apiService from '../../../../api/api.js'

import ButtonLarge from '../../../UI/buttons/buttonLarge/ButtonLarge'
import Input from '../../../UI/inputs/Input'

const validatorPass = new PasswordValidator()

const RegistrationAccount = ({ getStateHandler }) => {
    const [email, setEmail] = useState('')
    const [firstPassword, setFirstPassword] = useState('')
    const [secondPassword, setSecondPassword] = useState('')
    const [disabled, setDisabled] = useState(true)
    const [firstPasswordError, setFirstPasswordError] = useState(false)
    const [secondPasswordError, setSecondPasswordError] = useState(false)
    const [emailError, setEmailError] = useState(false)

    const [errorLenght, setErrorLenght] = useState(true)
    const [errorCase, setErrorCase] = useState(true)
    const [errorNotLetterChars, setErrorNotLetterChars] = useState(true)
    const [errorNotEqualPass, setErrorNotEqualPass] = useState(true)
    
    const emailHandler = (event) => {
        setEmail(event.target.value)
        setEmailError(false)
    }

    const firstPasswordHandler = (event) => {
        const pass = event.target.value
        if (!/[а-яА-ЯЁё]/.test(pass)) {
            setFirstPassword(pass)
            setFirstPasswordError(false)
        }
        else {
            setFirstPasswordError('Вы используете кириллицу')
        }
    }

    const secondPasswordHandler = (event) => {
        const pass = event.target.value
        if (!/[а-яА-ЯЁё]/.test(pass)) {
            setSecondPassword(pass)
            setSecondPasswordError(false)
        }
        else {
            setSecondPasswordError('Вы используете кириллицу')
        }
    }

    useEffect(() => {
        if (validatorPass.lenght(firstPassword)) {
            setErrorLenght(false)
        }
        else {
            setErrorLenght(true)
        }
        if (validatorPass.hasSpecialSymbols(firstPassword)) {
            setErrorNotLetterChars(false)
        }
        else {
            setErrorNotLetterChars(true)
        }
        if (validatorPass.hasUpperAndLowerCase(firstPassword)) {
            setErrorCase(false)
        }
        else {
            setErrorCase(true)
        }
        if (validatorPass.passwordsMatch(secondPassword, firstPassword)) {
            setErrorNotEqualPass(false)
        }
        else {
            setErrorNotEqualPass(true)
        }
        if (
            !errorLenght && !errorCase && !errorNotLetterChars &&
            !errorNotEqualPass && email
        ) {
            setDisabled(false)
        }
        else {
            setDisabled(true)
        }
    }, [
        email, firstPassword, secondPassword, errorLenght,
        errorCase, errorNotLetterChars, errorNotEqualPass
    ])

    const submitHandler = async (event) => {
        event.preventDefault()
        if (!disabled) {
            const response = await apiService
                .RegistrationAccount(email, secondPassword)
            if (response.status === 400) {
                const fields = Object.keys(response.data.errors)
                if (fields.includes('email')) {
                    setEmailError(response.data.errors.email)
                }
                if (fields.includes('password')) {
                    setFirstPasswordError(response.data.errors.password)
                    setSecondPasswordError(response.data.errors.password)
                }
            }
            else if (response.status === 201) {
                console.log('here')
                getStateHandler()
            }
        }
    }
    
    return (
        <div className={classes.RegistrationWrapper}>
            <div className={classes.RegistrationAccountContent}>
            <h3>Регистрация</h3>
            <form onSubmit={submitHandler} id='regAccount'>
                <Input
                    inputName={'Email'}
                    value={email}
                    onChange={emailHandler}
                    error={emailError}
                    form='regAccount'
                />
                <Input
                    inputName={'Пароль'}
                    isPass
                    value={firstPassword}
                    onChange={firstPasswordHandler}
                    error={firstPasswordError}
                    form='regAccount'
                />
                <Input
                    inputName={'Повторите пароль'}
                    isPass
                    value={secondPassword}
                    onChange={secondPasswordHandler}
                    error={secondPasswordError}
                    form='regAccount'
                />
            </form>
            <div className={classes.RequirementsList}>
                <h4>Требование к паролю</h4>
                <li
                     className={
                        [
                            errorLenght ?
                            classes.Error :
                            classes.NormalRow
                        ].join(' ')
                     }
                >
                    более 8 символов
                </li>
                <li
                    className={
                        [
                            errorCase ?
                            classes.Error :
                            classes.NormalRow
                        ].join(' ')
                    }
                >
                    строчные и прописные буквы
                </li>
                <li
                    className={
                        [
                            errorNotLetterChars ?
                            classes.Error :
                            classes.NormalRow
                        ].join(' ')
                    }
                >
                    небуквенные символы (1-9!#_-”*()$)
                </li>
                <li
                    className={
                        [
                            errorNotEqualPass ?
                            classes.Error :
                            classes.NormalRow
                        ].join(' ')
                    }
                >
                    пароли должны совпадать
                </li>
            </div>
            <ButtonLarge
                color={'yellow'} disabled={disabled}
                type='submit' form='regAccount'
            >
                Далее
            </ButtonLarge>
            </div>
        </div>
        
    )
}

export default RegistrationAccount