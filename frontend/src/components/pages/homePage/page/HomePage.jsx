import React from 'react'
import classes from './HomePage.module.css'
import { useNavigate } from 'react-router-dom'

import ButtonExtraLarge from '../../../UI/buttons/buttonXLarge/ButtonExtraLarge'
import ButtonMedium from '../../../UI/buttons/buttonMedium/ButtonMedium'
import Logo from '../../../../static/media/bigcat.svg'

const HomePage = () => {
    const navigate = useNavigate()
    return (
        <>
            <header className={classes.Header}>
                <div>
                    <ButtonMedium
                        onClick={() => navigate('/auth')}
                    >
                        Войти
                    </ButtonMedium>
                    <ButtonMedium
                        color={'red'}
                        onClick={() => navigate('/registration')}
                    >
                        Регистрация</ButtonMedium>
                </div>
            </header>
            <div className={classes.HomePageContainer}>
                <div className={classes.HomePageStartPage}>
                    <img src={Logo} alt='' />
                    <h1>Студенческая весна</h1>
                    <h1>2024</h1>
                    <ButtonExtraLarge
                        onClick={() => navigate('/registration')}
                    >Учавствовать!</ButtonExtraLarge>
                </div>
            </div>
        </>
    )
}

export default HomePage