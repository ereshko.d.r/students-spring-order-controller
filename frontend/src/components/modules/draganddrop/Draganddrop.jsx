import React, { useRef, useState } from 'react'
import classes from './Draganddrop.module.css'
import Icon from '../../../static/media/download.svg'
import ButtonMedium from '../../UI/buttons/buttonMedium/ButtonMedium'

const MAX_FILE_SIZE = 10485760 // 10Mb

const Placeholder = ({handle, sizeError, extensionError, ...props}) => {
    return (
        <>
        <div className={classes.TutorialText}>
            <div
                className={
                    sizeError ? classes.ErrorText : classes.NormalText
                }
            >
                Размер изображения должен быть не больше 10Мб
            </div>
            <div
                className={
                    extensionError ? classes.ErrorText : classes.NormalText
                }
            >
                Файл должен иметь расширение изображения
                (.jpeg .png .svg и т.п.)
            </div>
        </div>
        <div className={classes.MainContent}>
            <div>Можно перетащить файл в эту зону</div>
            <img src={Icon} alt=''/>
            <div>или можно просто</div>
            <ButtonMedium
                 color={'black'} onClick={handle}
            >
                Выбрать файл
            </ButtonMedium>
        </div>
        </>
    )
}


const Draganddrop = ({setImage, ...props}) => {

    const [dragActive, setDragActive] = useState(false)
    const inputRef = useRef(null)
    const [sizeError, setSizeError] = useState(false)
    const [extensionError, setExtensionError] = useState(false)


    const validateFile = (file) => {
        const fileType = file.type.split('/')[0]
        const fileSize = file.size
        if (fileType === 'image' && fileSize <= MAX_FILE_SIZE) {
            return true
        }
        if (fileType !== 'image') {
            setExtensionError(true)
        }
        if (fileSize > MAX_FILE_SIZE) {
            setSizeError(true)
        }
        return false
    }

    const convertToBase64 = (file) => {
        if (!validateFile(file)) {
            return null
        }
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onloadend = () => {
            setImage(reader.result)
        }
    }

    const handleDrag = (event) => {
        event.preventDefault()
        event.stopPropagation()
        if (event.type === 'dragenter' || event.type === 'dragover') {
            setSizeError(false)
            setExtensionError(false)
            setDragActive(true)
        }
        else if (event.type === 'dragleave') {
            setDragActive(false)
        }
    }

    const handleDrop = (event) => {
        event.preventDefault()
        event.stopPropagation()
        setDragActive(false)
        if (event.dataTransfer.files && event.dataTransfer.files[0]) {
            const file = event.dataTransfer.files[0]
            convertToBase64(file)
        }
    }

    const handleChange = (event) => {
        event.preventDefault()
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0]
            convertToBase64(file)
        }
    }

    const handleButton = () => {
        inputRef.current.click()
        setSizeError(false)
        setExtensionError(false)
    }

    
    return (
        <>
        <form 
            id='formFileUpload'
            onDragEnter={handleDrag}
            onDragLeave={handleDrag}
            onDragOver={handleDrag}
            onDrop={handleDrop}
            onSubmit={(e) => e.preventDefault()}
        >
            <input 
                type="file"
                ref={inputRef}
                id='inputFileUpload'
                className={classes.InputButton}
                multiple={false}
                onChange={handleChange}
            />
            <label
                htmlFor="inputFileUpload"
                className={
                    [
                        classes.DraganddropContainer,
                        (dragActive) ? classes.dragActive : '',
                        (sizeError || extensionError) ?
                        classes.ErrorBorder : classes.NormalBorder
                    ].join(' ')
                }
                onClick={(e) => e.preventDefault()}
            >
                {
                    dragActive ?
                    <img src={Icon} alt='' width={100} height={100}/> :
                    <Placeholder
                        handle={handleButton}
                        sizeError={sizeError}
                        extensionError={extensionError}
                    />
                }
            </label>
        </form>
        </>
    )
}

export default Draganddrop