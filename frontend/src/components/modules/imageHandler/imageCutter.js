import Tesseract from 'tesseract.js'

/**
 * Class `CutterImage`
 * 
 * This class for cutting parts from image
 * 
 *  @param {HTMLImageElement} img image HTMLImageElement
 *  @param {HTMLImageElement} template template HTMLImageElement
 * 
 *  sx      - distance from left side to object
 *  sy      - distance from top side to object
 *  sWidth  - object width
 *  sHeight - object height
 * 
 */

class BaseCutterImage{
    #CONTRAST = 600
    #GRAYSCALE = 100
    #SATURATE = 100


    constructor(image, template) {
        this.image = image
        this.template = template
        this.widthRate = image.width / template.width
        this.heightRate = image.height / template.height
    }

    /**
     * 
     * @param {number} width 
     * @param {number} height 
     * @returns {HTMLCanvasElement}
     */

    createCanvas(width, height) {
        const canvas = document.createElement('canvas')
        canvas.width = width
        canvas.height = height
        return canvas
    }

    /**
     * 
     * @param {HTMLCanvasElement} canvas 
     * @param {number} sx 
     * @param {number} sy 
     * @param {number} sWidth 
     * @param {number} sHeight 
     * @param {number} rotate 
     * @returns {string} image `base64`
     */

    getDrewImage(canvas, sx, sy, sWidth, sHeight) {
        canvas.getContext('2d').drawImage(
            this.image,
            sx * this.widthRate, sy * this.heightRate, 
            sWidth * this.widthRate, sHeight * this.heightRate,
            0, 0, canvas.width, canvas.height
        )
        return canvas.toDataURL('image/png')
    }

    addEffects(canvas) {
        const context = canvas.getContext('2d')
        context.filter = `saturate(${this.#SATURATE}%) contrast(${this.#CONTRAST}%) grayscale(${this.#GRAYSCALE}%)`
    }

    createImage(src, width=undefined, height=undefined) {
        return new Promise((resolve, reject) => {
            const image = new Image(width, height)
            image.src = src
            image.onload = () => {
                resolve(image)
            }
            image.onerror = () => {
                reject(new Error('Error'))
            }
        })
    }

    async rotate(base64) {
        const image = await this.createImage(base64)
        const canvas = this.createCanvas(image.height, image.width)
        const context = canvas.getContext('2d')
        context.rotate(-Math.PI / 2)
        context.drawImage(
            image, -canvas.height, 0, canvas.height, canvas.width
        )
        const flippedImage = canvas.toDataURL('image/png')
        canvas.remove()
        return flippedImage
    }

}

/**
 * Class wich cutter photo of passport
 */
export class PassportCutter extends BaseCutterImage {

    TEMPLATE_DETAILS_COORDINATES = {
        photo:      {width: 126, height: 160, sx: 0, sy: 24},
        lastName:   {width: 164, height: 37, sx: 147, sy: 34},
        firstName:  {width: 164, height: 20, sx: 147, sy: 68},
        secondName: {width: 164, height: 20, sx: 147, sy: 85},
        gender:     {width: 50, height: 19, sx: 130, sy: 102},
        birthdate:  {width: 130, height: 19, sx: 199, sy: 102},
        serial:     {width: 29, height: 64, sx: 330, sy: 52},
        number:     {width: 29, height: 64, sx: 330, sy: 113}
    }

    /**
     * 
     * @param {string} str 
     * @returns 
     */

    capitalizeString(str) {
        return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()
    }

    getPhoto() {
        const photo = this.TEMPLATE_DETAILS_COORDINATES.photo
        const width = photo.width
        const height = photo.height
        const canvas = this.createCanvas(width*2, height*2)
        const cuttedImageData = this.getDrewImage(
            canvas, photo.sx, photo.sy, width, height
        )
        canvas.remove()
        return cuttedImageData
    }

    getLastName() {
        const lastName = this.TEMPLATE_DETAILS_COORDINATES.lastName
        const width = lastName.width
        const height = lastName.height
        const canvas = this.createCanvas(width*10, height*10)
        this.addEffects(canvas)
        const cuttedImageData = this.getDrewImage(
            canvas, lastName.sx, lastName.sy, width, height
        )
        canvas.remove()
        return cuttedImageData
    }

    getFirstName() {
        const firstName = this.TEMPLATE_DETAILS_COORDINATES.firstName
        const width = firstName.width
        const height = firstName.height
        const canvas = this.createCanvas(width*10, height*10)
        this.addEffects(canvas)
        const cuttedImageData = this.getDrewImage(
            canvas, firstName.sx, firstName.sy, width, height
        )
        canvas.remove()
        return cuttedImageData
    }

    getSecondName() {
        const secondName = this.TEMPLATE_DETAILS_COORDINATES.secondName
        const width = secondName.width
        const height = secondName.height
        const canvas = this.createCanvas(width*10, height*10)
        this.addEffects(canvas)
        const cuttedImageData = this.getDrewImage(
            canvas, secondName.sx, secondName.sy, width, height
        )
        canvas.remove()
        return cuttedImageData
    }

    getGender() {
        const gender = this.TEMPLATE_DETAILS_COORDINATES.gender
        const width = gender.width
        const height = gender.height
        const canvas = this.createCanvas(width*10, height*10)
        this.addEffects(canvas)
        const cuttedImageData = this.getDrewImage(
            canvas, gender.sx, gender.sy, width, height
        )
        canvas.remove()
        return cuttedImageData
    }

    getBirthdate() {
        const birthdate = this.TEMPLATE_DETAILS_COORDINATES.birthdate
        const width = birthdate.width
        const height = birthdate.height
        const canvas = this.createCanvas(width*10, height*10)
        this.addEffects(canvas)
        const cuttedImageData = this.getDrewImage(
            canvas, birthdate.sx, birthdate.sy, width, height
        )
        canvas.remove()
        return cuttedImageData
    }

    async getSerial() {
        const serial = this.TEMPLATE_DETAILS_COORDINATES.serial
        const width = serial.width
        const height = serial.height
        const canvas = this.createCanvas(width*10, height*10)
        this.addEffects(canvas)
        const cuttedImageData = this.getDrewImage(
            canvas, serial.sx, serial.sy, width, height
        )
        const flippedImage = await this.rotate(cuttedImageData)
        canvas.remove()
        return flippedImage
    }

    async getNumber() {
        const number = this.TEMPLATE_DETAILS_COORDINATES.number
        const width = number.width
        const height = number.height
        const canvas = this.createCanvas(width*10, height*10)
        this.addEffects(canvas)
        const cuttedImageData = this.getDrewImage(
            canvas, number.sx, number.sy, width, height
        )
        const flippedImage = await this.rotate(cuttedImageData)
        canvas.remove()
        return flippedImage
    }

    async convertToText(base64) {
        const worker = await Tesseract.createWorker()
        await worker.loadLanguage('rus')
        await worker.initialize('rus')
        const {data:{text}} = await worker.recognize(base64, 'rus')
        worker.terminate()
        const cleanedString = text
        return cleanedString
    }

    async getPassData() {
        const tasks = [
            this.convertToText(this.getFirstName()),
            this.convertToText(this.getLastName()),
            this.convertToText(this.getSecondName()),
            this.convertToText(this.getGender()),
            this.convertToText(this.getBirthdate()),
            this.convertToText(await this.getSerial()),
            this.convertToText(await this.getNumber()),
        ]
        const result = await Promise.all(tasks)
        return {
            firstName:  this.capitalizeString(
                result[0].replace(/[^А-ЯЁ]/g, '')
            ),
            lastName:   this.capitalizeString(
                result[1].replace(/[^А-ЯЁ]/g, '')
            ),
            secondName: this.capitalizeString(
                result[2].replace(/[^А-ЯЁ]/g, '')
            ),
            gender:     this.capitalizeString(
                result[3].replace(/[^А-ЯЁ]/g, '')
            ),
            birthdate:  result[4].replace(/[^0-9.]/g, ''),
            serialPass: result[5].replace(/[^0-9]/g, ''),
            numberPass: result[6].replace(/[^0-9]/g, ''),
            photoPass: this.getPhoto(),
            photoFirstName: this.getFirstName(),
            photoLastName: this.getLastName(),
            photoSecondName: this.getSecondName(),
        }
    }
}

export class BageCutter extends BaseCutterImage {
    #WIDTH = 684
    #HEIGHT = 828

    async #arrangePhoto() {
        const canvas = this.createCanvas(this.#WIDTH, this.#HEIGHT)
        const context = canvas.getContext('2d')
        const image = await this.createImage(
            this.image, this.#WIDTH, this.#HEIGHT
        )
        context.drawImage(image, 0, 0, canvas.width, canvas.height)
        return canvas
    }

    async getBage(firstName='Котик', lastName='Котикович') {
        const canvas = await this.#arrangePhoto()
        const context = canvas.getContext('2d')
        context.font = '50px Geologica'
        context.fillStyle = '#FFFFFF'
        context.textAlign = 'center'
        context.textBaseline = 'middle'
        context.drawImage(this.template, 0, 0, canvas.width, canvas.height)
        context.fillText(firstName, 340, 470)
        context.fillText(lastName, 340, 530)
        const bageData = canvas.toDataURL('image/png')
        canvas.remove()
        return bageData
    }

    async getAvatar() {
        const canvas = this.createCanvas(220, 220)
        const image = await this
            .createImage((await this.#arrangePhoto())
            .toDataURL('image/png'))
        const context = canvas.getContext('2d')
        context.drawImage(image, 232, 212, 220, 220, 0, 0, 220, 220)
        const avatarData = canvas.toDataURL('image/png')
        canvas.remove()
        return avatarData
    }
}
