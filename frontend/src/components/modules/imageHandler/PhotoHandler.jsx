import React, { useState, useRef, useContext } from 'react'
import classes from './PhotoHandler.module.css'
import UserContext from '../../context/userContext'

import InputRange from '../../UI/inputrange/InputRange'
import Participant from '../../../static/media/participant.svg'
import ButtonSmall from '../../UI/buttons/buttonSmall/BurronSmall'
import AvatarEditor from 'react-avatar-editor'
import Minus from '../../../static/media/minus.svg'
import Plus from '../../../static/media/plus.svg'
import ArrowLeft from '../../../static/media/arrowleft.svg'
import ArrowRight from '../../../static/media/arrowright.svg'
import { BageCutter } from './imageCutter'

const PhotoHandler = ({ image, setImage, setBage, setShowModal, userData, ...props }) => {
    const [scale, setScale] = useState(1)
    const [rotate, setRotate] = useState(0)
    const {user} = useContext(UserContext)

    const templateRef = useRef()
    const canvasRef = useRef()

    const imageHandler = async () => {
        const canvas = canvasRef.current.getImage()
        const template = templateRef.current
        const bageCutter = new BageCutter(
            canvas.toDataURL('image/png'), template
        )
        const bage = await bageCutter.getBage(user.firstName, user.lastName)
        const avatar = await bageCutter.getAvatar()
        setBage({bage, avatar})
        setShowModal(true)
    }

    const scaleHandler = (event) => {
        if (event.type === 'dblclick') {
            setScale(1)
        }
        else {
            setScale(parseFloat(event.target.value))
        }
    }

    const rotateHandler = (event) => {
        if (event.type === 'dblclick') {
            setRotate(0)
        }
        else {
            setRotate(parseFloat(event.target.value))
        }
    }

    return (
        <div className={classes.ImageContainer}>
            <img
                className={classes.TemplateImage}
                ref={templateRef}
                src={Participant}
                style={{opacity: '90%'}}
                alt=''
            />
            <AvatarEditor
                className={classes.AvatarEditor}
                ref={canvasRef}
                image={image}
                width={264}
                height={320}
                border={0}
                scale={scale}
                rotate={rotate}
            />
            <div className={classes.RangeContainer}>
                <div className={classes.Range}>
                    <img src={Minus} alt='' />
                    <InputRange
                        min={0}
                        max={2}
                        step={0.01}
                        value={scale}
                        onChange={scaleHandler}
                        onDoubleClick={scaleHandler}
                    />
                    <img src={Plus} alt='' />
                </div>
                <div className={classes.Range}>
                    <img src={ArrowLeft} alt='' />
                    <InputRange
                        min={-45}
                        max={45}
                        step={0.1}
                        value={rotate}
                        onChange={rotateHandler}
                        onDoubleClick={rotateHandler}
                    />
                    <img src={ArrowRight} alt='' />
                </div>
                <div className={classes.ButtonContainer}>
                    <ButtonSmall
                        color={'red'}
                        onClick={() => setImage()}
                    >
                        Удалить
                    </ButtonSmall>
                    <ButtonSmall
                        onClick={imageHandler}
                    >
                        Применить
                    </ButtonSmall>
                </div>
            </div>
        </div>
        
    )
}

export default PhotoHandler