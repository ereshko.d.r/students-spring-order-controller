import React, { useState, useRef } from 'react'
import classes from './ImageHandler.module.css'
import { PassportCutter } from './imageCutter'

import InputRange from '../../UI/inputrange/InputRange'
import TemplateForPassport from '../../../static/media/templateforpassport.svg'
import ButtonSmall from '../../UI/buttons/buttonSmall/BurronSmall'
import AvatarEditor from 'react-avatar-editor'
import Minus from '../../../static/media/minus.svg'
import Plus from '../../../static/media/plus.svg'
import ArrowLeft from '../../../static/media/arrowleft.svg'
import ArrowRight from '../../../static/media/arrowright.svg'


const PassportHandler = ({ image, setImage, setPassData, setShowModal }) => {
    const [scale, setScale] = useState(1)
    const [rotate, setRotate] = useState(0)
    const [disabled, setDisabled] = useState(false)

    const templateRef = useRef()
    const canvasRef = useRef()

    const checkData = async () => {
        setDisabled(true)
        const canvas = canvasRef.current.getImage()
        const template = templateRef.current
        const img = new Image()
        img.src = canvas.toDataURL('image/png')
        img.onload = async () => {
            const cutter = new PassportCutter(img, template)
            const data = await cutter.getPassData()
            setShowModal(currentState => {return currentState ? false : true})
            setPassData(data)
            setDisabled(false)
        }
    }

    const scaleHandler = (event) => {
        if (event.type === 'dblclick') {
            setScale(1)
        }
        else {
            setScale(Number(event.target.value))
        }
    }

    const rotateHandler = (event) => {
        if (event.type === 'dblclick') {
            setRotate(0)
        }
        else {
            setRotate(Number(event.target.value))
        }
    }

    return (
        <div className={classes.ImageContainer}>
            <img
                ref={templateRef}
                src={TemplateForPassport}
                alt=''
                className={classes.TemplateImage}
            />
            <AvatarEditor
                className={classes.AvatarEditor}
                ref={canvasRef}
                image={image}
                width={351}
                height={253}
                border={0}
                scale={scale}
                rotate={rotate}
            />
            <div className={classes.RangeContainer}>
                <div className={classes.Range}>
                    <img src={Minus} alt='' />
                    <InputRange
                        min={0}
                        max={2}
                        step={0.01}
                        value={scale}
                        onChange={scaleHandler}
                        onDoubleClick={scaleHandler}
                    />
                    <img src={Plus} alt='' />
                </div>
                <div className={classes.Range}>
                    <img src={ArrowLeft} alt='' />
                    <InputRange
                        min={-45}
                        max={45}
                        step={0.1}
                        value={rotate}
                        onChange={rotateHandler}
                        onDoubleClick={rotateHandler}
                    />
                    <img src={ArrowRight} alt='' />
                </div>
                <div className={classes.ButtonContainer}>
                    <ButtonSmall
                        color={'red'}
                        disabled={disabled}
                        onClick={() => {
                            setImage()
                            setPassData(false)
                        }}
                    >
                        Удалить
                    </ButtonSmall>
                    <ButtonSmall
                        disabled={disabled}
                        onClick={() => checkData()}
                    >
                        Применить
                    </ButtonSmall>
                </div>
            </div>
        </div>
        
    )
}

export default PassportHandler