import React, { Children, cloneElement, useEffect, useRef, useState } from 'react'
import classes from './Carousel.module.css'
import { CarouselContext } from './CarouselContext'

const Carousel = ({children, ...props}) => {
    const [offset, setOffset] = useState(0)
    const [pages, setPages] = useState([])

    const containerRef = useRef({offsetWidth: 0})
    const allContentRef = useRef()

    const handleNextPage = () => {
        setOffset((currentOffset) => {
            const newOffset = currentOffset - containerRef.current.offsetWidth
            const maxOffest = - (
                containerRef.current.offsetWidth * (children.length - 1)
            )
            return Math.max(newOffset, maxOffest)
        })
    }

    const handlePrevPage = () => {
        setOffset((currentOffset) => {
            const newOffset = currentOffset + containerRef.current.offsetWidth
            return Math.min(newOffset, 0)
        })
    }

    useEffect(() => {
        setPages(
            Children.map(children, child => {
                return cloneElement(child, {
                    style: {
                        minWidth: containerRef.current.offsetWidth,
                        maxWidth: containerRef.current.offsetWidth
                    },
                    handleNextPage,
                    handlePrevPage,
                })
            })
        )
    }, [])

    return (
        <CarouselContext.Provider value={props}>
            <div className={classes.CarouselContainer} ref={containerRef}>
                <div className={classes.CarouselContent}>
                    <div className={classes.AllContent} ref={allContentRef} style={{transform: `translateX(${offset}px)`}}>
                        {pages}
                    </div>
                </div>
            </div>
        </CarouselContext.Provider>
    )
}

export default Carousel