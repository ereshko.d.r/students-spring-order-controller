import { useEffect } from 'react'

const useOutsideAlerter = (ref, handler) => {
    useEffect(() => {
        const handlerClickOutside = (event) => {
            if (ref.current && !ref.current.contains(event.target)) {
                handler()
            }
        }
        document.addEventListener('mousedown', handlerClickOutside)
        return () => {
            document.removeEventListener('mousedown', handlerClickOutside)
        }
    }, [ref])
}

export default useOutsideAlerter