import React from 'react'
import { makeAutoObservable } from 'mobx'

class OrdersInvites {
    orders = []
    order = {}
    invites = []

    constructor() {
        makeAutoObservable(this)
    }

    setOrders(data) {
        this.orders = data
    }

    setOrder(data) {
        this.order = data
    }

    setInvites(data) {
        this.invites = data
    }
}

export const orderInvites = new OrdersInvites()

const OrderInvitesContext = React.createContext(orderInvites)

export default OrderInvitesContext
