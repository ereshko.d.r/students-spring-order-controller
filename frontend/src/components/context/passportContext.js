import React from 'react'
import { makeAutoObservable } from 'mobx'

class Passport {
    firstName = ''
    lastName = ''
    secondName = ''
    telegram = ''
    gender = ''
    image = ''
    serialPass = ''
    numberPass = ''

    firstNameError = false
    lastNameError = false
    secondNameError = false
    telegramError = false
    serialError = false
    numberError = false

    constructor() {
        makeAutoObservable(this)
    }

    setFirstName(firstName) {
        this.firstName = firstName
    }

    setLastName(lastName) {
        this.lastName = lastName
    }

    setSecondName(secondName) {
        this.secondName = secondName
    }

    setTelegram(telegram) {
        this.telegram = telegram
    }

    setGender(gender) {
        this.gender = gender
    }

    setImage(image) {
        this.image = image
    }

    setSerialPass(serialPass) {
        this.serialPass = serialPass
    }

    setNumberPass(numberPass) {
        this.numberPass = numberPass
    }
}

export const passport = new Passport()

const PassportContext = React.createContext(passport)

export default PassportContext