import React from 'react'
import { makeAutoObservable } from 'mobx'

class Admin {
    admin = null

    constructor() {
        makeAutoObservable(this)
    }

    setAdminState(data) {
        this.admin = data
    }
}

export const admin = new Admin()

const AdminContext = React.createContext(admin)

export default AdminContext