import React from 'react'
import { makeAutoObservable } from 'mobx'

class User {
    user = undefined

    constructor() {
        makeAutoObservable(this)
    }

    setUser(data) {
        this.user = data
    }

    get userData() {
        return this.user
    }
}

export const user = new User()

const UserContext = React.createContext(user)

export default UserContext