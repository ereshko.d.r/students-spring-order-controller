import React, { useContext } from 'react'
import classes from './Header.module.css'
import { Outlet, useLocation, useNavigate } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import { URL } from '../../../../App'

import AvatarMini from '../../../UI/avatar/avatarmini/AvatarMini'
import ButtonSmall from '../../../UI/buttons/buttonSmall/BurronSmall'
import UserContext from '../../../context/userContext'
import AuthService from '../../../../api/auth'

const authService = new AuthService()

const Header = observer(() => {
    const {user} = useContext(UserContext)
    const userContext = useContext(UserContext)

    const navigate = useNavigate()
    const location = useLocation()
    const path = location.pathname.split('/')

    const userRole = user.role === 'volunteer' ? 'Волонтёр' : 'Организатор'

    const logoutHandler = () => {
        authService.logout()
        userContext.setUser(null)
        navigate('/')
    }

    const downloadHandler = () => {
        fetch(`${URL}api/download`, {
            method: 'GET',
            responseType: 'blob',
        })
            .then((response) => response.blob())
            .then((blob) => {
              const url = window.URL.createObjectURL(new Blob([blob]))
              const link = document.createElement('a')
              link.href = url
              link.setAttribute('download', 'bages.zip')
              document.body.appendChild(link)
              link.click()
              window.URL.revokeObjectURL(url)
            })
            .catch((error) => {
              console.log(error)
            })
    }

    const NavbarComponent = observer(() => {
        return (
            <nav className={classes.Navbar}>
                <ButtonSmall
                    onClick={() => navigate('/admin/orders')}
                    color={path.includes('orders') ? 'yellow' : null}
                >
                    Заявки
                </ButtonSmall>
                <ButtonSmall
                    onClick={() => navigate('/admin/participants')}
                    color={path.includes('participants') ? 'yellow' : null}
                >
                    Участники
                </ButtonSmall>
            </nav>
        )
    })
    return (
        <>
            <div className={classes.Container}>
                <header className={classes.Header}>
                    <div className={classes.User}>
                        <AvatarMini avatar={user.avatar}/>
                        <h3>{userRole}</h3>
                    </div>
                    <NavbarComponent />
                    <div className={classes.LogButton}>
                        <ButtonSmall onClick={downloadHandler}>
                            Бейджи
                        </ButtonSmall>
                        <ButtonSmall
                            onClick={logoutHandler}
                            color={'red'}
                        >
                            Выйти
                        </ButtonSmall>
                    </div>
                </header>
                <Outlet />
            </div>
        </>
    )
})

export default Header