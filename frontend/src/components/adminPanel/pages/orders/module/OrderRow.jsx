import React from 'react'
import classes from './OrderRow.module.css'
import { useNavigate } from 'react-router-dom'

import StatusbarSmall from '../../../../UI/statusbars/statusbarSmall/StatusbarSmall'

const OrderRow = ({order}) => {
    const navigate = useNavigate()

    const status = () => {
        if (order.orderIsSent || order.orderStatus === false) {
            if (order.orderStatus === true) {
                return (
                    <StatusbarSmall color={'blue'}>
                        Допущена
                    </StatusbarSmall>
                )
            }
            else if (order.orderStatus === false) {
                return (
                    <StatusbarSmall color={'red'}>
                        Не допущена
                    </StatusbarSmall>
                )
            }
            else {
                return (
                    <StatusbarSmall>
                        Проверяется
                    </StatusbarSmall>
                )
            }
        }
        else {
            return (
                <StatusbarSmall>
                    Формируется
                </StatusbarSmall>
            )
        }
    }

    const linkHandler = () => {
        navigate(`/admin/orders/${order.id}`)
    }
    return (
        <div className={classes.OrderRow} onClick={linkHandler}>
            <div>
                {order.groupName}
            </div>
            <div>
                {order.perfomanceName}
            </div>
            <div>
                {order.nomination}
            </div>
            <div>
                {status()}
            </div>
        </div>
    )
}

export default OrderRow