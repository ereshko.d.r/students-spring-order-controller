import React, { useState, useEffect } from 'react'
import classes from './RejectModal.module.css'

import Input from '../../../../UI/inputs/Input'
import ButtonMedium from '../../../../UI/buttons/buttonMedium/ButtonMedium'

const RejectModal = ({ rejectHandler }) => {
    const [groupNameValue, setGroupNameValue] = useState('')
    const [perfomanceNameValue, setPerfomanceNameValue] = useState('')
    const [disabled, setDisabled] = useState(false)

    const groupNameHandler = (event) => {
        setGroupNameValue(event.target.value)
    }

    const performanceNameHandler = (event) => {
        setPerfomanceNameValue(event.target.value)
    }

    useEffect(() => {
        if (perfomanceNameValue || groupNameValue) {
            setDisabled(false)
        }
        else {
            setDisabled(true)
        }
    }, [groupNameValue, perfomanceNameValue])

    return (
        <div className={classes.RejectModal}>
            <h3>Причины недопуска</h3>
            <form id='rejectForm'>
                <Input 
                    inputName={'Название коллектива'}
                    value={groupNameValue}
                    onChange={groupNameHandler}
                    form='rejectForm'
                />
                <Input 
                    inputName={'Название номера'}
                    value={perfomanceNameValue}
                    onChange={performanceNameHandler}
                    form='rejectForm'
                />
                <ButtonMedium
                    type='submit'
                    color={'red'} onClick={(event) => {
                        rejectHandler(event, groupNameValue, perfomanceNameValue)
                    }}
                    form='rejectForm'
                    disabled={disabled}
                >
                    Отклонить
                </ButtonMedium>
            </form>
        </div>
    )
}

export default RejectModal