import React, { useEffect, useRef, useState } from 'react'
import classes from './Order.module.css'

import apiService from '../../../../../api/api'
import Dropdown from '../../../../UI/dropdown/Dropdown'
import Input from '../../../../UI/inputs/Input'
import StatusbarSmall from '../../../../UI/statusbars/statusbarSmall/StatusbarSmall'
import ButtonImage from '../../../../UI/buttons/buttonImage/ButtonImage'

import Next from '../../../../../static/media/next.svg'
import Prev from '../../../../../static/media/prev.svg'
import OrderRow from '../module/OrderRow'

const AdminOrdersList = () => {
    const limit = 20
    
    const [page, setPage] = useState(1)
    const [orders, setOrders] = useState([])
    const [nameValue, setNameValue] = useState('')
    const [statusValue, setStatusValue] = useState()
    const [totalCount, setTotalCount] = useState()
    const [admitted, setAdmitted] = useState()
    const [notAdmitted, setNotAdmitted] = useState()

    const ordersListRef = useRef(null)

    const searchHandler = async () => {
        const status = () => {
            if (statusValue?.name && statusValue.name === 'Допущена') {
                return true
            }
            else if (statusValue?.name && statusValue.name === 'Не допущена') {
                return false
            }
            else if (statusValue?.name && statusValue.name === 'Проверяется') {
                return 'null'
            }
            else if (statusValue?.name && statusValue.name === 'Формируется') {
                return 'forming'
            }
        }
        const papams = {
            search: {
                page: page,
                limit: limit,
                name: nameValue,
                status: status()
            }
        }
        const response = await apiService.getOrdersList(papams)
        if (response.status === 200) {
            setOrders(response.data.result)
            setTotalCount(response.data.totalCount)
            setAdmitted(response.data.admitted)
            setNotAdmitted(response.data.notAdmitted)
        }
    }

    const nameHandler = (event) => {
        setNameValue(event.target.value)
    }

    const nextPageHandler = () => {
        setPage(prev => prev + 1)
    }

    const prevPageHandler = () => {
        setPage(prev => prev - 1)
    }

    useEffect(() => {
        setPage(1)
        searchHandler()
    }, [nameValue, statusValue])

    useEffect(() => {
        searchHandler()
    }, [page])

    useEffect(() => {
        ordersListRef.current.scrollTo({
            top: 0,
            behavior: 'smooth',
        })
    }, [orders])

    return (
        <div className={classes.Orders}>
            <div className={classes.OrdersContainer}>
                <div className={classes.OrdersListContent}>
                    <header className={classes.Header}>
                        <div className={classes.Search}>
                            <Input 
                                style={{width: 300}}
                                inputName={'Поиск по имени'}
                                value={nameValue}
                                onChange={nameHandler}
                            />
                        </div>
                        <div className={classes.SelectBox}>
                            <Dropdown
                                style={{width: 250}}
                                inputName={'Статус'}
                                value={statusValue}
                                setValue={setStatusValue}
                                list={[
                                    {id: 0, name: 'Допущена'},
                                    {id: 1, name: 'Не допущена'},
                                    {id: 2, name: 'Проверяется'},
                                    {id: 3, name: 'Формируется'},
                                    {id: 4, name: '----------'}
                                ]}
                            />
                        </div>
                    </header>
                    <div className={classes.OrdersList} ref={ordersListRef}>
                        {
                            orders.map((order, index) => {
                                return (
                                    <OrderRow order={order} key={index} />
                                )
                            })
                        }
                    </div>
                    <div className={classes.OrdersStatus}>
                        <StatusbarSmall color={'red'}>
                            {notAdmitted}
                        </StatusbarSmall>
                        <StatusbarSmall>
                            {limit * (page - 1) + orders.length}/{totalCount}
                        </StatusbarSmall>
                        <StatusbarSmall color={'blue'}>
                            {admitted}
                        </StatusbarSmall>
                    </div>
                    <div className={classes.Paginator}>
                        <div>
                            {
                                page === 1 ?
                                null :
                                <ButtonImage onClick={prevPageHandler}>
                                    {Prev}
                                </ButtonImage>
                            }
                        </div>
                        <div>
                            {page}
                        </div>
                        <div>
                            {
                                (page * limit) >= totalCount ? 
                                null :
                                <ButtonImage onClick={nextPageHandler}>
                                    {Next}
                                </ButtonImage>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AdminOrdersList