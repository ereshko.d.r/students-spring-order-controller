import React, { useEffect, useState } from 'react'
import classes from './Order.module.css'
import { useLocation, useNavigate } from 'react-router-dom'
import { observer } from 'mobx-react-lite'

import ParticipantRow from '../../partipians/module/ParticipantRow'
import StatusbarMedium from '../../../../UI/statusbars/statusbarMedium/StatusbarMedium'
import ButtonMedium from '../../../../UI/buttons/buttonMedium/ButtonMedium'
import ButtonImage from '../../../../UI/buttons/buttonImage/ButtonImage'

import ArrowBack from '../../../../../static/media/arrowback.svg'
import apiService from '../../../../../api/api'
import ModalWindow from '../../../../UI/modalwindow/ModalWindow'
import RejectModal from '../module/RejectModal'


const AdminOrdersDetails = observer(() => {
    const location = useLocation()
    const navigate = useNavigate()
    const orderId = location.pathname.split('/')[3]

    const [order, setOrder] = useState(false)
    const [nominationName, setNominationName] = useState('')
    const [groupName, setGroupName] = useState('')
    const [perfomanceName, setPerfomanceName] = useState('')
    const [status, setStatus] = useState(null)
    const [orderIsSent, setOrderIsSent] = useState(false)
    const [hasRejectedParticipants, setHasRejectedParticipants] = useState(false)

    const [showModal, setShowModal] = useState(false) 

    const [participants, setParticipants] = useState([])

    const orderHandler = async () => {
        const response = await apiService.getOrderDetails(orderId)
        if (response.status === 200) {
            setOrder(true)
            setNominationName(response.data.nominationName)
            setGroupName(response.data.groupName)
            setPerfomanceName(response.data.perfomanceName)
            setParticipants(response.data.participants)
            setStatus(response.data.orderStatus)
            setOrderIsSent(response.data.orderIsSent)
        }
    }

    const acceptHandler = async () => {
        const body = {
            status: true,
            groupName: null,
            perfomanceName: null
        }
        const response = await apiService.verdictOrder(orderId, body)
        if (response.status === 200) {
            setStatus(response.data.orderStatus)
        }
    }

    const rejectHandler = async (event, groupName=null, perfomanceName=null) => {
        event.preventDefault()
        const body = {
            status: false,
            groupName: {reason: groupName},
            perfomanceName: {reason: perfomanceName}
        }
        const response = await apiService.verdictOrder(orderId, body)
        if (response.status === 200) {
            setStatus(response.data.orderStatus)
            setShowModal(false)
        }
    }

    const StatusComponent = observer(() => {
        if (orderIsSent || status === false) {
            if (status === true) {
                return (
                    <StatusbarMedium
                        color={'blue'}
                    >
                        Допущена
                    </StatusbarMedium>
                )
            }
            else if (status === false) {
                return (
                    <StatusbarMedium
                        color={'red'}
                    >
                        Не допущена
                    </StatusbarMedium>
                )
            }
            else {
                return (
                    <StatusbarMedium>
                        Проверяется
                    </StatusbarMedium>
                )
            }
        }
        else {
            return (
                <StatusbarMedium>
                    Не отправлена
                </StatusbarMedium>
            )
        }
    })    

    const ParticipantsStatusOrderComponent = observer(() => {
        const acceptedParticipants = participants.reduce((sum, participant) => {
            if (participant.status === true) {
                sum++
            }
            else if (participant.status === false){
                setHasRejectedParticipants(true)
            }
            return sum
        }, 0)
        if (acceptedParticipants === participants.length) {
            return (
                <StatusbarMedium
                    color={'blue'}
                >
                    {acceptedParticipants}/{participants.length}
                </StatusbarMedium>
            )
        }
        else if (hasRejectedParticipants) {
            return (
                <StatusbarMedium
                    color={'red'}
                >
                    {acceptedParticipants}/{participants.length}
                </StatusbarMedium>
            )
        }
        else {
            return (
                <StatusbarMedium>
                    {acceptedParticipants}/{participants.length}
                </StatusbarMedium>
            )
        }
    })

    useEffect(() => {
        if (!order) {
            orderHandler()
        }
    }, [orderIsSent])

    return (
        <>
            <ModalWindow showModal={showModal} setShowModal={setShowModal}>
                <RejectModal rejectHandler={rejectHandler}/>
            </ModalWindow>
            <div className={classes.Orders}>
                <div className={classes.OrderDetailsContainer}>
                    <div className={classes.OrderDetailsContent}>
                        <div className={classes.OrderDetailsHeader}>
                            <ButtonImage
                                onClick={() => navigate('/admin/orders')}
                            >
                                {ArrowBack}
                            </ButtonImage>
                            <div>
                                <h4>Номинация</h4>
                                <h3>{nominationName}</h3>
                            </div>
                        </div>
                        <div className={classes.OrderDetaisInfo}>
                            <div className={classes.OrderDetailsInfoContent}>
                                <h4>Название коллектива</h4>
                                <h3>{groupName}</h3>
                            </div>
                            {
                                perfomanceName ?
                                <div className={classes.OrderDetailsInfoContent}>
                                    <h4>Название номера</h4>
                                    <h3>{perfomanceName}</h3>
                                </div> :
                                null
                            }
                        </div>
                        <h3>Участники</h3>
                        <div className={classes.OrderDetailsParticipants}>
                            {
                                participants.length > 0 ?
                                participants.map((participant, index) => (
                                    <ParticipantRow participant={participant} key={index}/>
                                )) :
                                <h3>Нет участников</h3>
                            }
                        </div>
                    </div>
                    <div className={classes.OrderDetailsFooter}>
                        <div className={classes.OrderDetailsFooterStatus}>
                            <ParticipantsStatusOrderComponent />
                        </div>
                        <div className={classes.OrderDetailsFooterButtons}>
                            {
                                orderIsSent ? 
                                    (hasRejectedParticipants ?
                                    <ButtonMedium
                                        color={'red'}
                                        onClick={rejectHandler}
                                    >
                                        Отклонить
                                    </ButtonMedium> :
                                    <>
                                        <ButtonMedium
                                            onClick={acceptHandler}
                                        >
                                            Допустить
                                        </ButtonMedium>
                                        <ButtonMedium
                                            onClick={() => {
                                                setShowModal(true)
                                            }}
                                            color={'red'}
                                        >
                                            Отклонить
                                        </ButtonMedium>
                                    </> ) : null
                                    
                            }
                        </div>
                        <div>
                            <StatusComponent />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
})

export default AdminOrdersDetails