import React, { useContext, useEffect } from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import apiService from '../../../../api/api'
import UserContext from '../../../context/userContext'

import Header from '../../module/header/Header'

const AdminPanelPrivateRoute = observer(() => {
    const userContext = useContext(UserContext)
    const {user} = userContext

    const navigate = useNavigate()
    const getAdminData = async () => {
        const response = await apiService.getUserData()
        if (response.status === 200) {
            userContext.setUser(response.data)
        }
        else if (response.status === 401) {
            navigate('/admin/auth')
        }
        else if (response.status === 403) {
            navigate('/')
        }
    }

    useEffect(() => {
        getAdminData()
    }, [])
    return (
        user ? 
        <Header>
            <Outlet />
        </Header> :
        null
    )
})

export default AdminPanelPrivateRoute