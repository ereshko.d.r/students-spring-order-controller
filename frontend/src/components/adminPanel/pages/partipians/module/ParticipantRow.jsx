import React from 'react'
import classes from './ParticipantRow.module.css'
import AvatarMini from '../../../../UI/avatar/avatarmini/AvatarMini'
import StatusbarSmall from '../../../../UI/statusbars/statusbarSmall/StatusbarSmall'
import { useNavigate } from 'react-router-dom'

const ParticipantRow = ({participant}) => {
    const navigate = useNavigate()

    const fullName = (
        `${participant.lastName} ${participant.firstName} ${participant.secondName}`
    )

    const status = () => {
        if (participant.status === true) {
            return (
                <StatusbarSmall color={'blue'}>
                    Допущен
                </StatusbarSmall>
            )
        }
        else if (participant.status === false) {
            return (
                <StatusbarSmall color={'red'}>
                    Не допущен
                </StatusbarSmall>
            )
        }
        else {
            return (
                <StatusbarSmall>
                    Проверяется
                </StatusbarSmall>
            )
        }
    }

    const linkHandler = () => {
        navigate(`/admin/participants/${participant.id}`)
    }
    return (
        <div className={classes.ParticipantRow} onClick={linkHandler}>
            <AvatarMini avatar={participant.avatar}/>
            <div>{fullName}</div>
            <div>
                {status()}
            </div>
        </div>
    )
}

export default ParticipantRow