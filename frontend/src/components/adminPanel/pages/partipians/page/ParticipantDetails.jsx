import React, { useEffect, useState } from 'react'
import classes from './Participant.module.css'
import { observer } from 'mobx-react-lite'
import { useLocation, useNavigate } from 'react-router-dom'
import apiService from '../../../../../api/api'

import ButtonMedium from '../../../../UI/buttons/buttonMedium/ButtonMedium'
import StatusbarSmall from '../../../../UI/statusbars/statusbarSmall/StatusbarSmall'
import Image from '../../../../UI/image/Image'
import Checkbox from '../../../../UI/checkbox/Checkbox'
import OrderRow from '../../orders/module/OrderRow'

import Arrowback from '../../../../../static/media/arrowback.svg'
import ButtonImage from '../../../../UI/buttons/buttonImage/ButtonImage'

const PartipiansDetails = () => {
    const location = useLocation()
    const navigate = useNavigate()
    const userId = location.pathname.split('/')[3]

    const [user, setUser] = useState(null)
    const [fullName, setFullName] = useState('')
    const [status, setStatus] = useState()
    const [bage, setBage] = useState('')
    const [photoPass, setPhotoPass] = useState('')
    const [photoFirstName, setPhotoFirstName] = useState('')
    const [photoLastName, setPhotoLastName] = useState('')
    const [photoSecondName, setPhotoSecondName] = useState('')
    const [participantOrders, setParticipantOrders] = useState([])

    const [isPersonMatched, setIsPersonMatched] = useState(false)
    const [isNameMatched, setIsNameMatched] = useState(false)
    const [isPhotoMatched, setIsPhotoMatched] = useState(false)
    const [isNormalPhoto, setIsNormalPhoto] = useState(false)

    const getUserHandler = async () => {
        const response = await apiService.getParticipantDetails(userId)
        if (response.status === 200) {
            const name = `${response.data.lastName} ${response.data.firstName} ${response.data.secondName} `
            setUser(true)
            setFullName(name)
            setBage(response.data.bagePhoto)
            setPhotoPass(response.data.passPhoto)
            setPhotoFirstName(response.data.photoFirstName)
            setPhotoLastName(response.data.photoLastName)
            setPhotoSecondName(response.data.photoSecondName)
            setParticipantOrders(response.data.participantOrders)
            setStatus(response.data.status)
        }
        else if (response.status === 404) {
            navigate('/admin/participants')
        }
    }

    const getStatus = () => {
        if (status === true) {
            return <StatusbarSmall color={'blue'}>Допущен</StatusbarSmall>
        }
        else if (status === false) {
            return <StatusbarSmall color={'red'}>Не допущен</StatusbarSmall>
        }
        else {
            return <StatusbarSmall>Проверяется</StatusbarSmall>
        }
    }

    const verdictHandler = async () => {
        const status = (
            isPersonMatched && isNameMatched &&
            isPhotoMatched && isNormalPhoto
        )
        const personError = 'Личность на фото неразличима'
        const nameError = 'ФИО не совпадает с профилем'
        const photoError = 'Личность не совпадает с паспортом'
        const normalError = 'Данное фото недопустимо'
        const body = {
            status: status,
            passport: {
                isPersonMatched: !isPersonMatched ? personError : null,
                isNameMatched: !isNameMatched ? nameError : null
            },
            bage: {
                isPhotoMatched: !isPhotoMatched ? photoError : null,
                isNormalPhoto: !isNormalPhoto ? normalError : null
            }
        }
        const response = await apiService.verdictParticipant(userId, body)
        if (response.status === 200) {
            setStatus(response.data.status)
        }
    }

    const VerdictButton = observer(() => {
        if (
            isPersonMatched && isNameMatched &&
            isPhotoMatched && isNormalPhoto
        ) {
            return (
                <ButtonMedium
                    onClick={verdictHandler}
                    color={'blue'}
                >Допустить</ButtonMedium>   
            )
        }
        else {
            return (
                <ButtonMedium
                    onClick={verdictHandler}
                    color={'red'}
                >Отклонить</ButtonMedium>   
            )
        }
        
    })

    useEffect(() => {
        if (!user) {
            getUserHandler()
        }
    }, [])

    return (
      <div className={classes.Participants}>
            <div className={classes.ParticipantsContainer}>
                <div className={classes.ParticipantDetailContent}>
                    <div className={classes.ParticipantDetailContentHeader}>
                        <ButtonImage
                            onClick={() => {
                                navigate('/admin/participants')
                            }}
                        >
                            {Arrowback}
                        </ButtonImage>
                    </div>
                    <div className={classes.ParticipantDetail}>
                        <div className={classes.ParticipantDetailHeader}>
                            <h3>{fullName}</h3>
                            {getStatus()}
                        </div>
                        <div className={classes.ParticipantDetailInfo}>
                            <div>
                                Паспорт
                                <Image path={photoPass} height={150}/>
                            </div>
                            <div>
                                Бейдж
                                <Image path={bage} width={150}/>
                            </div>
                            <div>
                                Паспортные данные
                                <div className={classes.PassportImage}>
                                    Фамилия
                                    <Image path={photoLastName} width={180}/>
                                </div>
                                <div className={classes.PassportImage}>
                                    Имя
                                    <Image path={photoFirstName} width={180}/>
                                </div>
                                <div className={classes.PassportImage}>
                                    Отчество
                                    <Image path={photoSecondName} width={180}/>
                                </div>
                            </div>
                            <div className={classes.FormContainer}>
                                <div className={classes.Form}>
                                    <p>Паспорт</p>
                                    <div>
                                        <Checkbox
                                            value={isPersonMatched}
                                            setValue={setIsPersonMatched}
                                        />
                                        Личность на фото различима
                                    </div>
                                    <div>
                                        <Checkbox
                                            value={isNameMatched}
                                            setValue={setIsNameMatched}
                                        />
                                        ФИО совпадает с паспортом
                                    </div>
                                </div>
                                <div className={classes.Form}>
                                    <p>Бейдж</p>
                                    <div>
                                        <Checkbox
                                            value={isPhotoMatched}
                                            setValue={setIsPhotoMatched}
                                        />
                                        Фото совпадает с паспортом
                                    </div>
                                    <div>
                                        <Checkbox
                                            value={isNormalPhoto}
                                            setValue={setIsNormalPhoto}
                                        />
                                        Приличное фото
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={classes.ParticipantDetailFooter}>
                            <VerdictButton onClick={verdictHandler}/>
                        </div>
                    </div>
                    <h3>Заявки пользователя</h3>
                    <div className={classes.ParticipantOrders}>
                        {
                            participantOrders?.length > 0 ? 
                            participantOrders.map((order, index) => {
                                return <OrderRow order={order} key={index} />
                            }) :
                            <h3>Нет заявок</h3>
                        }
                    </div>
                </div>
            </div>
      </div>
    )
}

export default PartipiansDetails