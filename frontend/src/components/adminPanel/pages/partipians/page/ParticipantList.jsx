import React, { useEffect, useRef, useState } from 'react'
import classes from './Participant.module.css'

import apiService from '../../../../../api/api'
import Dropdown from '../../../../UI/dropdown/Dropdown'
import Input from '../../../../UI/inputs/Input'
import ParticipantRow from '../module/ParticipantRow'
import StatusbarSmall from '../../../../UI/statusbars/statusbarSmall/StatusbarSmall'
import ButtonImage from '../../../../UI/buttons/buttonImage/ButtonImage'


import Next from '../../../../../static/media/next.svg'
import Prev from '../../../../../static/media/prev.svg'

const ParticipantList = () => {
    const limit = 20
    
    const [page, setPage] = useState(1)
    const [participants, setParticipants] = useState([])
    const [nameValue, setNameValue] = useState('')
    const [statusValue, setStatusValue] = useState()
    const [totalCount, setTotalCount] = useState()
    const [admitted, setAdmitted] = useState()
    const [notAdmitted, setNotAdmitted] = useState()

    const participantListRef = useRef(null)

    const searchHandler = async () => {
        const status = () => {
            if (statusValue?.name && statusValue.name === 'Допущен') {
                return true
            }
            else if (statusValue?.name && statusValue.name === 'Не допущен') {
                return false
            }
            else if (statusValue?.name && statusValue.name === 'Проверяется') {
                return 'null'
            }
        }
        console.log(page)
        const papams = {
            search: {
                page: page,
                limit: limit,
                name: nameValue,
                status: status()
            }
        }
        const response = await apiService.getParticipantList(papams)
        if (response.status === 200) {
            setParticipants(response.data.result)
            setTotalCount(response.data.totalCount)
            setAdmitted(response.data.admitted)
            setNotAdmitted(response.data.notAdmitted)
            // setPage(response.data.page)
        }
    }

    const nameHandler = (event) => {
        setNameValue(event.target.value)
    }

    const nextPageHandler = (event) => {
        setPage(prev => prev + 1)
        // searchHandler()
    }

    const prevPageHandler = () => {
        setPage(prev => prev - 1)
        // searchHandler()
    }

    useEffect(() => {
        setPage(1)
        searchHandler()
    }, [nameValue, statusValue])

    useEffect(() => {
        searchHandler()
    }, [page])

    useEffect(() => {
        participantListRef.current.scrollTo({
            top: 0,
            behavior: 'smooth',
        })
    }, [participants])

    return (
        <div className={classes.Participants}>
            <div className={classes.ParticipantsContainer}>
                <div className={classes.ParticipantListContent}>
                    <header className={classes.Header}>
                        <div className={classes.Search}>
                            <Input 
                                style={{width: 300}} inputName={'Поиск по имени'}
                                value={nameValue} onChange={nameHandler}
                            />
                        </div>
                        <div className={classes.SelectBox}>
                            <Dropdown
                                style={{width: 250}}
                                inputName={'Статус'}
                                value={statusValue}
                                setValue={setStatusValue}
                                list={[
                                    {id: 0, name: 'Допущен'},
                                    {id: 1, name: 'Не допущен'},
                                    {id: 2, name: 'Проверяется'},
                                    {id: 3, name: '----------'}
                                ]}
                            />
                        </div>
                    </header>
                    <div className={classes.ParticipantList} ref={participantListRef}>
                        {
                            participants.map((participant, index) => {
                                return (
                                    <ParticipantRow participant={participant} key={index} />
                                )
                            })
                        }
                    </div>
                    <div className={classes.ParticipantsStatus}>
                        <StatusbarSmall color={'red'}>{notAdmitted}</StatusbarSmall>
                        <StatusbarSmall>{limit * (page - 1) + participants.length}/{totalCount}</StatusbarSmall>
                        <StatusbarSmall color={'blue'}>{admitted}</StatusbarSmall>
                    </div>
                    <div className={classes.Paginator}>
                        <div>
                            {
                                page === 1 ?
                                null :
                                <ButtonImage onClick={prevPageHandler}>{Prev}</ButtonImage>
                            }
                        </div>
                        <div>
                            {page}
                        </div>
                        <div>
                            {
                                (page * limit) >= totalCount ? 
                                null :
                                <ButtonImage onClick={nextPageHandler}>{Next}</ButtonImage>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ParticipantList