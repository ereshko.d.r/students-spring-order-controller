import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import classes from './AuthAdminPage.module.css'

import AuthService from '../../../../../api/auth'

import Input from '../../../../UI/inputs/Input'
import ButtonLarge from '../../../../UI/buttons/buttonLarge/ButtonLarge'

const authService = new AuthService()

const AuthAdminPage = () => {
    const [showModal, setShowModal] = useState(false)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [disabled, setDisabled] = useState(false)
    const [errorEmail, setErrorEmail] = useState(false)
    const [errorPass, setErrorPass] = useState(false)
    const navigate = useNavigate()

    const emailHandler = (event) => {
        setEmail(event.target.value)
        setErrorEmail(false)
    }

    const passwordHandler = (event) => {
        setPassword(event.target.value)
        setErrorPass(false)
    }
    
    const submitHandler = async (event) => {
        event.preventDefault()
        const acceptedRoles = ['volunteer', 'moderator', 'admin']
        if (!disabled) {
            const response = await authService.authorization(email, password)
            if (response.status === 200) {
                const {role} = response.data
                if (acceptedRoles.includes(role)) {
                    navigate('/admin/orders')
                }
                else {
                    setErrorEmail('У вас нет прав доступа')
                }
            }
            if (response.status === 404) {
                setErrorEmail('Неверный email')
                setErrorPass('Неверный пароль')
            }
        }
    }

    useEffect(() => {
        if (email && password) {
            setDisabled(false)
        }
        else {
            setDisabled(true)
        }
    }, [email, password])
    return (
        <div className={classes.AuthPage}>
            <div className={classes.AuthPageContainer}>
                <div className={classes.AuthPageContent}>
                    <h3>Авторизация</h3>
                    <form onSubmit={submitHandler} id='AuthForm'>
                        <Input
                            inputName={'Email'} error={errorEmail}
                            value={email} onChange={emailHandler}
                            form='AuthForm'
                        />
                        <Input
                            inputName={'Пароль'} isPass error={errorPass}
                            value={password} onChange={passwordHandler}
                            form='AuthForm'
                        />
                    </form>
                    <div 
                        className={classes.ForgotPassword}
                        onClick={() => setShowModal(true)}
                    >
                        Забыл пароль
                    </div>
                    <ButtonLarge
                        color={'yellow'}
                        disabled={disabled} type="submit" form='AuthForm'
                    >
                        Войти
                    </ButtonLarge>
                </div>
            </div>
        </div>
    )
}

export default AuthAdminPage