import React, { useState } from 'react'
import classes from './Checkbox.module.css'

const Checkbox = ({value, setValue}) => {

    return (
        <label className={classes.CheckboxLabel}>
            <input
            className={classes.Input}
            type="checkbox"
            checked={value}
            onChange={() => setValue(prev => !prev)}
            />
            <span className={`${classes.Checkbox} ${value ? classes.Checked : ''}`}></span>
        </label>
    )
}

export default Checkbox