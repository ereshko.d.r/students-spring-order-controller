import React from 'react'
import classes from './AvatarLarge.module.css'
import { URL } from '../../../../App'
import NoAvatar from '../../../../static/media/noavatar.png'

const AvatarLarge = ({avatar, firstName, lastName}) => {
    const image = `${URL}${avatar}`
    const getImage = () => {
        return avatar ? image : NoAvatar
    }
    return (
        <div className={classes.AvatarContainer}>
            <div className={classes.Avatar}>
                <img src={getImage()} alt='' width={150}/>
            </div>
            <h3>{`${firstName} ${lastName}`}</h3>
        </div>
    )
}

export default AvatarLarge