import React from 'react'
import classes from './AvatarSmall.module.css'
import { URL } from '../../../../App'
import NoAvatar from '../../../../static/media/noavatar.png'

const AvatarSmall = ({avatar, firstName}) => {
    const image = `${URL}${avatar}`
        const getImage = () => {
            return avatar ? image : NoAvatar
        }
    return (
        <div className={classes.AvatarContainer}>
            <div className={classes.Avatar}>
                <img src={getImage()} alt='' />
            </div>
            <h3>{firstName}</h3>
        </div>
    )
}

export default AvatarSmall