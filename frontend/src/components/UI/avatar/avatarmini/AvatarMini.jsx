import React from 'react'
import classes from './AvatarMini.module.css'
import NoAvatar from '../../../../static/media/noavatar.png'
import { URL } from '../../../../App'

const AvatarMini = ({avatar}) => {
    const image = `${URL}${avatar}`
    const getImage = () => {
        return avatar ? image : NoAvatar
    }
return (
    <div className={classes.AvatarContainer}>
        <img src={getImage()} alt='' />
    </div>
)
}

export default AvatarMini