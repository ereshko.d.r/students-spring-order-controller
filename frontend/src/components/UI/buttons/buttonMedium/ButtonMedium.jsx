import React from 'react'
import classes from './ButtonMedium.module.css'

const ButtonMedium = ({color, children, ...props}) => {
    const getColor = () => {
        switch (color) {
            case 'blue':
                return classes.Blue
            case 'yellow':
                return classes.Yellow
            case 'red':
                return classes.Red
            case 'black':
                return classes.Black
            default:
                return classes.Blue
        }
    }
    return (
        <button
            className={[classes.ButtonMedium, getColor()].join(' ')}
            {...props}
        >
            {children}
        </button>
    )
}

export default ButtonMedium