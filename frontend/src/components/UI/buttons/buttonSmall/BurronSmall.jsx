import React from 'react'
import classes from './ButtonSmall.module.css'

const ButtonSmall = ({color, children, ...props}) => {
    const getColor = () => {
        switch (color) {
            case 'blue':
                return classes.Blue
            case 'yellow':
                return classes.Yellow
            case 'red':
                return classes.Red
            case 'black':
                return classes.Black
            default:
                return classes.Blue
        }
    }
    return (
        <button
            className={[classes.ButtonSmall, getColor()].join(' ')}
            {...props}
        >
            {children}
        </button>
    )
}

export default ButtonSmall