import React from 'react'
import classes from './ButtonExtraLarge.module.css'

const ButtonExtraLarge = ({children, ...props}) => {
    return (
        <button
            className={classes.ButtonExtraLarge}
            {...props}
        >
            {children}
        </button>
    )
}

export default ButtonExtraLarge