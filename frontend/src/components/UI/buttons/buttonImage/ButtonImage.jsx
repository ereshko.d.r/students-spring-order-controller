import React from 'react'
import classes from './ButtonImage.module.css'

const ButtonImage = ({children, ...props}) => {
    return (
        <button className={classes.ButtonImage} {...props}>
            <img src={children} alt='' />
        </button>
    )
}

export default ButtonImage