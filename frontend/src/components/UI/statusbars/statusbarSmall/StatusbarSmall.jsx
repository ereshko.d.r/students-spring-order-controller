import React from 'react'
import classes from './StatusbarSmall.module.css'
import { Tooltip } from 'react-tooltip'

const StatusbarSmall = ({color, children, status=[], ...props}) => {
    const getColor = () => {
        switch (color) {
            case 'blue':
                return classes.Blue
            case 'red':
                return classes.Red
            case 'grey':
                return classes.Grey
            default:
                return classes.Grey
        }
    }
    return (
        <>
            <div 
                className={[classes.StatusbarSmall, getColor()].join(' ')}
                data-tooltip-id='test'
                data-tooltip-content={status.map((object, index) => (
                    object
                ))}
                data-tooltip-place='top'
                data-tooltip-delay-show={300}
            >
                {children}
            </div>
            <Tooltip id='test' />
        </>
    )
}

export default StatusbarSmall