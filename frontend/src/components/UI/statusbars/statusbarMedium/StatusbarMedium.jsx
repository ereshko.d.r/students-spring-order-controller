import React from 'react'
import classes from './StatusbarMedium.module.css'
import { Tooltip } from 'react-tooltip'

const StatusbarMedium = ({color, children, status=[], ...props}) => {
    const getColor = () => {
        switch (color) {
            case 'blue':
                return classes.Blue
            case 'red':
                return classes.Red
            case 'grey':
                return classes.Grey
            default:
                return classes.Grey
        }
    }

    return (
        <>
            <div 
                className={[classes.StatusbarMedium, getColor()].join(' ')}
                data-tooltip-id='test'
                data-tooltip-content={status.map((value, index) => (
                    value
                ))}
                data-tooltip-place='top'
                data-tooltip-delay-show={300}
            >
                {children}
            </div>
            <Tooltip id='test' />
        </>
    )
}

export default StatusbarMedium