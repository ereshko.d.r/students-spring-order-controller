import React from 'react'
import classes from './GenderRadiobutton.module.css'
import MaleCat from '../../../static/media/MaleCat.svg'
import FemaleCat from '../../../static/media/FemaleCat.svg'

const GenderRadiobutton = ({gender, setGender}) => {
    const inputValue = (event) => {
        setGender(event.target.name)
    }

    return (
        <div>
            <div className={classes.title}>Пол</div>
            <div className={classes.RadiobuttonContainer}>
                <div className={classes.case}>
                    <input
                        type='radio'
                        name='male'
                        id='1'
                        checked={(gender === 'male') ? true : false}
                        onChange={inputValue}
                    />
                    <label htmlFor='1' className={[classes.Radiobutton, classes.Male].join(' ')}><img src={MaleCat} alt='Мужской'/></label>
                    <div className={classes.genderName}>Мужской</div>
                </div>
                <div className={classes.case}>
                    <input
                        type='radio'
                        name='female'
                        id='0'
                        checked={(gender === 'female') ? true : false}
                        onChange={inputValue}
                    />
                    <label htmlFor='0' className={[classes.Radiobutton, classes.Female].join(' ')}><img src={FemaleCat} alt='Женский'/></label>
                    <div className={classes.genderName}>Женский</div>
                </div>
            </div>
        </div>
    )
}

export default GenderRadiobutton