import React, { useEffect, useState } from 'react'
import classes from './Input.module.css'
import DropdownRow from '../dropdown/DropdownRow'

const Input = ({error, isPass, inputName, value, hidden, list=null, setValue, ...props}) => {
    const [disabled, setDisabled] = useState(false)
    const setHandler = (event) => {
        setValue(list[event.target.id])
        setDisabled(true)
        
    }

    useEffect(() => {
        setDisabled(false)
    }, [list])
    return (
        <div hidden={hidden}>
            <div className={classes.InputName}>
                {inputName}
            </div>
            <input
                className={
                    [classes.Input, (error) ? classes.Error : ''].join(' ')
                }
                type={(isPass) ? 'password' : 'text'}
                value={value}
                {...props}
                
            >
            </input>
            <div className={classes.InputList} hidden={!list}>
                {
                    list && !disabled ? 
                    list.map((value, index) => (
                        <DropdownRow
                            key={index} id={index}
                            onClick={setHandler}
                        >
                            {value}
                        </DropdownRow>
                    )) : null
                }
            </div>
            <div 
                className={classes.ErrorMessage}
                hidden={!error}
            >
                {error}
            </div>
        </div>
    )
}

export default Input