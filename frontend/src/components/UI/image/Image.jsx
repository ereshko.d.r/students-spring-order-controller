import React from 'react'
import { URL } from '../../../App'

const Image = ({path, ...props}) => {
    const src = `${URL}${path}`
    return (
        <img src={src} alt='' {...props}/>
    )
}

export default Image