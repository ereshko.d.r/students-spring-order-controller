import React from 'react'
import classes from './Dropdown.module.css'

const DropdownRow = ({children, ...props}) => {
    
    return (
        <div className={classes.DropdownRow} {...props}>
            {children}
        </div>
    )
}

export default DropdownRow