import React, { useRef, useState } from 'react'
import classes from './Dropdown.module.css'
import Icon from '../../../static/media/arrowdown.svg'
import DropdownRow from './DropdownRow'
import useOutsideAlerter from '../../hooks/useOutsideHook'

const Dropdown = ({list, inputName, value, setValue, style}) => {
    const [showList, setShowList] = useState(false)
    

    if (!list || list?.length === 0) {
        list = ['----------']
    }

    const clickHandler = (event) => {
        setShowList(!showList)
    }

    const closeHandler = () => {
        setShowList(false)
    }

    const selectHandler = (event) => {
        setValue(list[event.target.id])
        setShowList(false)
    }

    const ref = useRef(null)
    useOutsideAlerter(ref, closeHandler)

    return (
        <div className={classes.DropdownContainer} style={style}>
            <div className={classes.DropdownContainerName}>{inputName}</div>
            <div className={classes.Dropdown} style={style} onClick={clickHandler} >
                <div className={classes.DropdownHeader}>
                    {
                        value?.name ?
                        value.name :
                        '----------'
                    }
                </div>
                <div className={classes.DropdownArrow}>
                    <img src={Icon} alt=''/>
                </div>
            </div>
            <div className={classes.DropdownList} style={style} hidden={!showList} ref={ref}>
                {list.map((value, index) => (
                    <DropdownRow key={index} id={index} onClick={selectHandler} >{value.name}</DropdownRow>
                ))}
            </div>
        </div>
    )
}

export default Dropdown