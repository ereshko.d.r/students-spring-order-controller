import React from 'react'
import classes from './InputRange.module.css'

const InputRange = ({value, ...props}) => {

    return (
        <input
            className={classes.Range}
            type="range"
            value={value}
            {...props}
        />
    )
}

export default InputRange