import React from 'react'
import classes from './ModalWindow.module.css'

const ModalWindow = ({children, showModal, setShowModal, ...props}) => {

    return (
        <div hidden={showModal ? false : true} >
            <div className={classes.ModalWindowContainer} onClick={() => setShowModal(false)}>
                <div className={classes.ModalWindowContent} onClick={e => e.stopPropagation()}>
                    {children}
                </div>
            </div>
        </div>
    )
}

export default ModalWindow