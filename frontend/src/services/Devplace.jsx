import React from 'react'
import classes from './Devplace.module.css'
import Input from '../components/UI/inputs/Input'
import ButtonExtraLarge from '../components/UI/buttons/buttonXLarge/ButtonExtraLarge'
import ButtonLarge from '../components/UI/buttons/buttonLarge/ButtonLarge'
import ButtonMedium from '../components/UI/buttons/buttonMedium/ButtonMedium'
import ButtonSmall from '../components/UI/buttons/buttonSmall/BurronSmall'
import GenderRadiobutton from '../components/UI/radiobuttons/GenderRadiobutton'
import StatusbarSmall from '../components/UI/statusbars/statusbarSmall/StatusbarSmall'
import StatusbarMedium from '../components/UI/statusbars/statusbarMedium/StatusbarMedium'
import Draganddrop from '../components/modules/draganddrop/Draganddrop'
import Dropdown from '../components/UI/dropdown/Dropdown'
import InputRange from '../components/UI/inputrange/InputRange'


const Devplace = () => {
    return (
        <div className={classes.Devplace}>
            <h1>Input</h1>
            <div className={classes.Podium}>
                <Input isError={false} errorMessage={'Error message'} inputName={'Input Name'}/>
                <Input isPass={true} inputName={'Input Name'}/>
                <Input isError={true} errorMessage={'Error message'} inputName={'Input Name'}/>
            </div>
            <h1>XLarge Button</h1>
            <div className={classes.Podium}>
                <ButtonExtraLarge>Button</ButtonExtraLarge>
                <ButtonExtraLarge disabled>Button</ButtonExtraLarge>
            </div>
            <h1>Large button</h1>
            <div className={classes.Podium}>
                <ButtonLarge color={'blue'}>Button blue</ButtonLarge>
                <ButtonLarge color={'blue'} disabled>Button blue</ButtonLarge>
                <ButtonLarge color={'yellow'}>Button yellow</ButtonLarge>
                <ButtonLarge color={'yellow'}disabled>Button yellow</ButtonLarge>
                <ButtonLarge color={'red'}>Button red</ButtonLarge>
                <ButtonLarge color={'red'}disabled>Button red</ButtonLarge>
                <ButtonLarge color={'black'}>Button black</ButtonLarge>
                <ButtonLarge color={'black'}disabled>Button black</ButtonLarge>
            </div>
            <h1>Meduim button</h1>
            <div className={classes.Podium}>
                <ButtonMedium color={'blue'}>Button blue</ButtonMedium>
                <ButtonMedium color={'blue'} disabled>Button blue</ButtonMedium>
                <ButtonMedium color={'yellow'}>Button yellow</ButtonMedium>
                <ButtonMedium color={'yellow'}disabled>Button yellow</ButtonMedium>
                <ButtonMedium color={'red'}>Button red</ButtonMedium>
                <ButtonMedium color={'red'}disabled>Button red</ButtonMedium>
                <ButtonMedium color={'black'}>Button black</ButtonMedium>
                <ButtonMedium color={'black'}disabled>Button black</ButtonMedium>
            </div>
            <h1>Small button</h1>
            <div className={classes.Podium}>
                <ButtonSmall color={'blue'}>Button blue</ButtonSmall>
                <ButtonSmall color={'blue'} disabled>Button blue</ButtonSmall>
                <ButtonSmall color={'yellow'}>Button yellow</ButtonSmall>
                <ButtonSmall color={'yellow'}disabled>Button yellow</ButtonSmall>
                <ButtonSmall color={'red'}>Button red</ButtonSmall>
                <ButtonSmall color={'red'}disabled>Button red</ButtonSmall>
                <ButtonSmall color={'black'}>Button black</ButtonSmall>
                <ButtonSmall color={'black'}disabled>Button black</ButtonSmall>
            </div>
            <h1>Radio button</h1>
            <div className={classes.Podium}>
                <GenderRadiobutton />
            </div>
            <h1>Medium statusbar</h1>
            <div className={classes.Podium}>
                <StatusbarMedium color={'blue'}>blue statusbar</StatusbarMedium>
                <StatusbarMedium color={'red'}>red statusbar</StatusbarMedium>
                <StatusbarMedium color={'grey'}>grey statusbar</StatusbarMedium>
            </div>
            <h1>Small statusbar</h1>
            <div className={classes.Podium}>
                <StatusbarSmall color={'blue'}>blue statusbar</StatusbarSmall>
                <StatusbarSmall color={'red'}>red statusbar</StatusbarSmall>
                <StatusbarSmall color={'grey'}>grey statusbar</StatusbarSmall>
            </div>
            <h1>Select</h1>
            <div className={classes.Podium}>
                <Dropdown />
            </div>
            <h1>Input range</h1>
            <div className={classes.Podium}>
                <InputRange />
            </div>
            <h1>Drag and drop zone</h1>
            <div className={classes.Podium}>
                <Draganddrop />
            </div>
        </div>
    )
}

export default Devplace