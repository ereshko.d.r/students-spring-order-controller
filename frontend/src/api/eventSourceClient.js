import { EventSourcePolyfill } from 'event-source-polyfill'
import { API_URL } from '../App'

/**
 * EventSourceClient - class to subscribe and unsubscribe
 * to server events
 */

class EventSourceClient {
    constructor() {
        this.eventSource = null
    }

    subscribe(params) {
        this.eventSource = new EventSourcePolyfill(
            `${API_URL}/events`, params
        )
        return this.eventSource
    }

    unsubscribe() {
        if (this.eventSource) {
            this.eventSource.close()
            this.eventSource = null
        }
    }
}

const eventSourceClient = new EventSourceClient()
export default eventSourceClient