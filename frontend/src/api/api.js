import axios from 'axios'
import AuthService from './auth.js'
import { API_URL } from '../App.js'


const authService = new AuthService()

axios.defaults.withCredentials = true

/**
 * ApiService - class for creating http requests to backend
 * and returning http response from backend
 * 
 * use `postRequest`, `patchRequest` and `getRequest` class methods
 * to create request
 */

class ApiService {

    /**
     * Method wich create post request to backend
     * 
     * if backend send `status 401` it will try to refresh token
     * and if it refresh token, method call itself to send request again
     * 
     * @param {string} path 
     * @param {object} body 
     * @param {object} params 
     * @returns 
     */

    postRequest(path, body, params=null) {
        const {headers} = authService.authHeader
        const response = axios.post(`${API_URL}/${path}`, body, {
            headers,
            params
        })
            .catch(async (error) => {
                if (error.response.status === 401) {
                    if (await authService.refreshAccessToken()) {
                        return this.postRequest(path, body, params)
                    }
                    return {
                        status: error.response.status
                    }
                }
                return error.response
            })
        return response
    }

    /**
     * Method wich create get request to backend
     * 
     * if backend send `status 401` it will try to refresh token
     * and if it refresh token, method call itself to send request again
     * 
     * @param {string} path 
     * @param {object} body 
     * @param {object} params 
     * @returns 
     */

    getRequest(path, params=null) {
        const {headers} = authService.authHeader
        const response = axios.get(`${API_URL}/${path}`, {
            headers,
            params
        })
            .catch(async (error) => {
                if (error.response.status === 401) {
                    if (await authService.refreshAccessToken()) {
                        return this.getRequest(path, params)
                    }
                    return {
                        status: error.response.status
                    }
                }
                return error.response
            })
        return response
    }

    /**
     * Method wich create patch request to backend
     * 
     * if backend send `status 401` it will try to refresh token
     * and if it refresh token, method call itself to send request again
     * 
     * @param {string} path 
     * @param {object} body 
     * @param {object} params 
     * @returns 
     */

    patchRequest(path, body) {
        const response = axios
            .patch(`${API_URL}/${path}`, body, authService.authHeader)
            .catch(async (error) => {
                if (error.response.status === 401) {
                    if (await authService.refreshAccessToken()) {
                        return this.getRequest(path, body)
                    }
                    return {
                        status: error.response.status
                    }
                }
                return error.response
            })
        return response
    }

    // registration endpoints

    async RegistrationAccount(email, password) {
        const response = await this
            .postRequest('registration', {email, password})
        authService.saveAccessToken(response.data.accessToken)
        return response
    }

    async RegistrationUser(args) {
        return await this.postRequest('registration/user', args)
    }

    async RegistrationBage(args) {
        return await this.postRequest('registration/bage', args)
    }

    async getUserState() {
        return await this.getRequest('registration/user')
    }

    // users endpoints

    async getUserData() {
        return await this.getRequest('users/me')
    }

    async getUsersByEmail(body) {
        return await this.postRequest('users/emails', body)
    }

    async updatePassData(body) {
        return await this.patchRequest(`users/pass`, body)
    }

    async updateBageData(body) {
        return await this.patchRequest('users/bage', body)
    }

    async getUserByEmail(body) {
        return await this.postRequest(`users/email`, body)
    }

    async checkIsUserInOrder(body) {
        return await this.postRequest('users/orders', body)
    }

    // orders endpoints

    async getDirections() {
        return await this.getRequest('orders/directions')
    }

    async createOrder(body) {
        return await this.postRequest('orders/create', body)
    }

    async userOrders() {
        return await this.getRequest('orders/me')
    }

    async getRoles() {
        return await this.getRequest('orders/roles')
    }

    async detailsOrder(id) {
        return await this.getRequest(`orders/${id}`)
    }

    async updateOrder(id, body) {
        return await this.patchRequest(`orders/${id}`, body)
    }

    async deleteOrder(id, body) {
        return await this.postRequest(`orders/${id}/delete`, body)
    }


    async inviteUserToOrder(id, body) {
        return await this.postRequest(`orders/${id}/invite`, body)
    }

    async acceptInvite(id) {
        return await this.getRequest(`orders/${id}/invite/accept`)
    }

    async rejectInvite(id) {
        return await this.getRequest(`orders/${id}/invite/reject`)
    }

    async leaveOrder(id) {
        return await this.getRequest(`orders/${id}/leave`)
    }

    async removeUser(id, body) {
        return await this.postRequest(`orders/${id}/remove`, body)
    }

    // admin participant endpoints

    async getParticipantList(params) {
        return await this.getRequest(`admin/participants`, params)
    }

    async getParticipantDetails(id) {
        return await this.getRequest(`admin/participants/${id}`)
    }

    async verdictParticipant(id, body) {
        return await this.patchRequest(`admin/participants/${id}`, body)
    }

    // admin order endpoints

    async getOrdersList(params) {
        return await this.getRequest(`admin/orders`, params)
    }

    async getOrderDetails(id) {
        return await this.getRequest(`admin/orders/${id}`)
    }

    async verdictOrder(id, body) {
        return await this.patchRequest(`admin/orders/${id}`, body)
    }

}

const apiService = new ApiService()

export default apiService