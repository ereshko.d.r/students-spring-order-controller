import axios from 'axios'
import { API_URL } from '../App'

axios.defaults.withCredentials = true

/**
 * AuthService - class to management authentication service
 */
class AuthService {
    get authHeader() {
        const accessToken = sessionStorage.getItem('accessToken')
        if (!accessToken) {
            return false
        }
        const bearer = {authorization: 'Bearer ' + accessToken}
        return {headers: bearer}
    }

    saveAccessToken(token) {
        sessionStorage.setItem('accessToken', token)
    }

    removeAccessToken() {
        sessionStorage.removeItem('accessToken')
    }

    async refreshAccessToken() {
        const response = await axios.get(`${API_URL}/auth/refresh`)
            .catch(() => {
                this.removeAccessToken()
                return null
            })
        if (!response) {
            return false
        }
        this.saveAccessToken(response.data.accessToken)
        return true
    }

    async authorization(email, password) {
        const response = await axios.post(`${API_URL}/auth`, {email, password})
            .catch((error) => error.response)
        return response
    }

    async logout() {
        const resopnse = await axios.get(`${API_URL}/auth/logout`)
            .catch((error) => {
                return error.response
            })
        
        if (resopnse?.status === 401) {
            sessionStorage.clear('accessToken')
            return true
        }
        return false
    }
}

export default AuthService