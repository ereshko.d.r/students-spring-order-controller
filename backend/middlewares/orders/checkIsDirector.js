const pkg = require('../../App')
const bcrypt = require('bcrypt')

const checkIsDirector = async (request, response, next) => {
    const userId = request.user
    const {password} = request.body
    const {orderId} = request.params
    try {
        const result = await pkg.knex('orders')
            .join('order_users', 'orders.id', 'order_users.order_id')
            .join('order_roles', 'order_users.role_id', 'order_roles.id')
            .join('users', 'order_users.user_id', 'users.id')
            .select({
                'orderId': 'orders.id',
                'roleName': 'order_roles.name',
                'userId': 'users.id'
            })
            .where({
                'orders.id': orderId,
                'users.id': userId
            })
            .first()
        if (!result) {
            return response.status(403)
                .json({permissionDenied: 'Отказано в доступе'})
        }
        const hashPassword = await pkg.knex('accounts')
            .select('password')
            .where('user_id', userId)
            .first()
        const comapreResult = await bcrypt
            .compare(password, hashPassword.password)
            .catch(error => false)
        if (!comapreResult) {
            return response.status(403).json({password: 'Неверный пароль'})
        }
        next()
    }
    catch (error) {
        console.log(error)
        response.sendStatus(400)
    }
}

module.exports.checkIsDirector = checkIsDirector