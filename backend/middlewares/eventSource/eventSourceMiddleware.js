const { eventSourceServer } = require('../../eventSourceServer')

const eventSourceMiddleware = async (request, response, next) => {
    const userId = request.user
    response.writeHead(200, {
        "Content-Type": "text/event-stream",
        "Cache-Control": "no-store",
        "Access-Control-Allow-Origin": "*"
    })

    let counter = 0
    let interValID = setInterval(() => {
        counter++
        if (counter >= 600) {
            clearInterval(interValID)
            return
        }
        response.write(`data: ${JSON.stringify({num: counter})}\n\n`)
    }, 1000)

    response.on('close', () => {
        const user = userId
        eventSourceServer.removeClient(user)
        clearInterval(interValID)
        response.end()
    })
    eventSourceServer.addClient(userId, response)
}

module.exports.eventSourceMiddleware = eventSourceMiddleware