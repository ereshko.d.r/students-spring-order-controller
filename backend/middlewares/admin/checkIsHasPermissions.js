const { knex } = require('../../knex')

const acceptedRoles = ['volunteer', 'moderator', 'admin']

const checkIsHasPermissions = async (request, response, next) => {
    const userId = request.user
    try {
        const {role} = await knex('users')
            .select('role')
            .where('id', userId)
            .first()
        if (!acceptedRoles.includes(role)) {
            return response.status(403).json('Permission denied')
        }
        request.userRole = role
        next()
    }
    catch (error) {
        console.log(error)
        return response.sendStatus(500)
    }
}

module.exports.checkIsHasPermissions = checkIsHasPermissions