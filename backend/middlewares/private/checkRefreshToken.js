const { knex } = require('../../knex')

const checkRefreshToken = async (request, response, next) => {
    const { refreshToken } = request.cookies
    if (!refreshToken) {
        return response.sendStatus(401)
    }
    try {
        const token = await knex('accounts')
            .join('users', 'accounts.user_id', 'users.id')
            .select('refresh_token')
            .where('refresh_token', refreshToken)
            .where('users.role', 'admin')
        if (token.length === 0) {
            return response.sendStatus(401)
        }
        next()
    }
    catch (error) {
        console.log(error)
        response.sendStatus(500)
    }
}

module.exports.checkRefreshToken = checkRefreshToken