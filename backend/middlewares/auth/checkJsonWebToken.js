const config = require('../../config')
const jwt = require('jsonwebtoken')


const checkJsonWebToken = async (request, response, next) => {
    const accessToken = request.headers.authorization &&
                    request.headers?.authorization.split(' ')
    if (accessToken && accessToken[0] === 'Bearer') {
        try {
            jwt.verify(accessToken[1], config.secretKey, (error, decoded) => {
                if(error) {
                    return response.sendStatus(401)
                }
                else {
                    request.user = decoded.id
                    next()
                }
            })
        }
        catch {
            return response.sendStatus(401)
        }
    }
    else {
        return response.sendStatus(401)
    }
}

module.exports.checkJsonWebToken = checkJsonWebToken