const jwt = require('jsonwebtoken')
const config = require('../../config')

const checkJsonWebToken = (socket, next) => {
    const accessToken = socket.handshake.auth?.authorization &&
                        socket.handshake.auth?.authorization.split(' ')
    if (accessToken && accessToken[0] === 'Bearer') {
        const unauth = new Error('not auth')
        unauth.data = {reason: 401}
        try {
            jwt.verify(accessToken[1], config.secretKey, (error, decoded) => {
                if(error) {
                    next(unauth)
                }
                else {
                    socket.user = decoded.id
                    next()
                }
            })
        }
        catch {
            next(unauth)
        }
    }
    else {
        const unauth = new Error('not auth')
        unauth.data = {reason: 401}
        next(unauth)
    }
}

module.exports.checkJsonWebToken = checkJsonWebToken