const { knex } = require('../../knex')

const checkIsMember = async (socket, next) => {
    const {orderId} = socket.handshake.query
    const userId = socket.user
    const result = await knex('order_users')
        .select({
            'userId': 'user_id',
            'roleId': 'role_id'
        })
        .where({
            'order_id': orderId,
            'user_id': userId
        })
        .first()
    if (!result) {
        const error = new Error()
        error.data = {reason: 404}
        return next(error)
    }
    socket.roleId = result.roleId
    next()
}

module.exports.checkIsMember = checkIsMember