const bodyParser = require('body-parser')
const express = require('express')
const http = require('http')
const cookieParser = require('cookie-parser')
const cors = require('cors')

const { initializeWebsocket } = require('./websocket')
const { checkRefreshToken } = require('./middlewares/private/checkRefreshToken') 
const { router } = require('./controllers/router')
const config = require('./config')


const PORT = config.server.port || 5000

const app = express()
const server = http.createServer(app)
const websocket = initializeWebsocket(server)


app.use(express.json({limit: config.requestSize}))
app.use(express.urlencoded({ extended: false, limit: config.requestSize }))

app.use(bodyParser.urlencoded({extended: true, limit: config.requestSize}))
app.use(bodyParser.json({limit: config.requestSize}))

app.use(cookieParser())

const corsOptions = {
    credentials: true,
    exposedHeaders: ['Content-Disposition'],
    origin: function(origin, callback) {
        callback(null, true)
    },
}
app.use(cors(corsOptions))

app.use((request, response, next) => {
    console.log(`${request.path}  ${request.method}`)
    next()
})

app.use('/public', express.static(__dirname + '/public'))
app.use('/private', checkRefreshToken, express.static(__dirname + '/private'))
app.use('/api', router)

server.listen(PORT, () => {
    console.log(`server has been started on ${PORT} port...`)
})

module.exports.websocket = websocket