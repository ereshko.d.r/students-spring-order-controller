const fs = require('fs')
const config = require('../config')
const path = require('path')

class ConvertToImage {
    /**
     * 
     * @param {string} base64 
     */
    constructor(base64) {
        [this.info, this.base64] = base64.split(',')
        this.extension = this.info
            .match(/^data:image\/(\w+);base64/)[1]
        this.filename = String(performance.now())
            .replace('.', '') + Date.now()
    }

    saveFile(path) {
        fs.writeFile(
            `${path}.${this.extension}`, this.base64, 'base64', (err) => {}
        )
    }

    saveToPublickDir() {
        this.saveFile(path.join(config.publicDir, this.filename))
        return `public/${this.filename}.${this.extension}`
    }

    saveToPrivatDir() {
        this.saveFile(path.join(config.privateDir, this.filename))
        return `private/${this.filename}.${this.extension}`
    }
}

module.exports.ConvertToImage = ConvertToImage