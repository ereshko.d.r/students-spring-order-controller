const { body, validationResult } = require('express-validator')

exports.createOrderValidator = [
    body('directionId')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('nominationId')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('groupName')
        .notEmpty()
        .withMessage('Поле обязательно'),
]

exports.validateFields = (request, response, next) => {
    const errors = validationResult(request)
        .formatWith(errors => {
            return errors.msg
        })
        .mapped()
    if (Object.keys(errors).length !== 0) {
        return response.status(400).json({errors: errors})
    }
    next()
}