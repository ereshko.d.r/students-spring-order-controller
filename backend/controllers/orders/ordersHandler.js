const { BaseHandler } = require('../baseHandler')
const { eventSourceServer } = require('../../eventSourceServer')
const {knex} = require('../../knex')
const { checkJsonWebToken } = require('../../middlewares/auth/checkJsonWebToken')
const { checkIsDirector } = require('../../middlewares/orders/checkIsDirector')
const { validateFields, createOrderValidator} = require('./validator')
const pkg = require('../../App')

class OrdersHandler extends BaseHandler {
    constructor() {
        super(['GET', 'POST', 'PATCH'])
    }

    middleware() {
        this.router.use(checkJsonWebToken)
    }

    routers() {
        this.router.get(
            '/directions',
            (request, response) => this.getDirection(request, response)
        )
        this.router.post(
            '/create',
            createOrderValidator,
            validateFields,
            (request, response) => this.createOrder(request, response)
        )
        this.router.get(
            '/roles',
            (request, response) => this.rolesList(request, response)
        )
        this.router.get(
            '/me',
            (request, response) => this.userOrders(request, response)
        )
        this.router.get(
            '/:orderId/invite/accept',
            (request, response) => this.acceptInvite(request, response)
        )
        this.router.get(
            '/:orderId/invite/reject',
            (request, response) => this.rejectInvite(request, response)
        )
    }

    async getDirection(request, response) {
        try {
            const result = await knex('directions')
                .join(
                    'nominations', 'directions.id', 'nominations.direction_id'
                )
                .select({
                    'id': 'directions.id',
                    'name': 'directions.name',
                    'isPerfomance': 'directions.is_perfomance',
                    'nominations': knex.raw(`
                        JSON_AGG(
                            JSON_BUILD_OBJECT(
                                'id', nominations.id,
                                'name', nominations.name
                            )
                        )
                    `)
                })
                .groupBy(
                    'directions.id', 'directions.name'
                )
                .orderBy('directions.id')
            response.status(this.STATUS_CODE.OK).json(result)
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async rolesList (request, response) {
        knex('order_roles').select('*')
            .then(data => response.status(this.STATUS_CODE.OK).json(data))
    }

    async createOrder(request, response) {
        const userId = request.user
        const {
            directionId, nominationId,
            groupName, perfomanceName
        } = request.body
        const trx = await knex.transaction()
        try {
            const {id} = await trx('orders')
                .insert({
                    'direction_id': directionId,
                    'nomination_id': nominationId,
                    'group_name': groupName,
                    'perfomance_name': perfomanceName || null
                })
                .returning('id')
                .then(data => data[0])
            await trx('orders_status')
                .insert({
                    'order_id': id
                })
            await trx('order_users')
                .insert({
                    'order_id': id,
                    'role_id': 1,
                    'user_id': userId,
                    'is_join': true
                })
            trx.commit()
            response.status(this.STATUS_CODE.CREATED).json({orderId: id})
            eventSourceServer.updateOrdersClients(userId)
        }
        catch (error) {
            trx.rollback()
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }
    async userOrders(request, response) {
        const userId = request.user
        try {
            const result = await knex('orders')
                .join('order_users', 'orders.id', 'order_users.order_id')
                .join('nominations', 'orders.nomination_id', 'nominations.id')
                .join('users', 'order_users.user_id', 'users.id')
                .join('orders_status', 'orders.id', 'orders_status.order_id')
                .select({
                    'id': 'orders.id',
                    'groupName': 'orders.group_name',
                    'perfomanceName': 'orders.perfomance_name',
                    'nominationName': 'nominations.name',
                    'userId': 'users.id',
                    'orderStatus': 'orders_status.status',
                    'orderIsSent': 'orders_status.is_sent'
                })
                .where('users.id', userId)
            response.status(this.STATUS_CODE.OK).json(result)
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async acceptInvite(request, response) {
        const userId = request.user
        const {orderId} = request.params
        const trx = await knex.transaction()
        try {
            await trx('order_users')
                .update({
                    'is_join': true
                })
                .where({
                    'order_id': orderId,
                    'user_id': userId
                })
            await trx('invites')
                .where({
                    'order_id': orderId,
                    'user_id': userId
                })
                .del()
            await trx.commit()
            eventSourceServer.updateInvitesClients(userId)
            eventSourceServer.updateOrdersClients(userId)
            pkg.websocket.to(orderId).emit('orders:selfUpdate')
            response.status(this.STATUS_CODE.OK).json({orderId})
        }
        catch (error) {
            trx.rollback()
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }

    }

    async rejectInvite(request, response) {
        const userId = request.user
        const {orderId} = request.params
        const trx = await knex.transaction()
        try {
            await trx('invites')
                .where({
                    'order_id': orderId,
                    'user_id': userId
                })
                .del()
            await trx('order_users')
                .where({
                    'order_id': orderId,
                    'user_id': userId
                })
                .del()
            await trx.commit()
            eventSourceServer.updateInvitesClients(userId)
            eventSourceServer.updateOrdersClients(userId)
            pkg.websocket.to(orderId).emit('orders:selfUpdate')
            response.sendStatus(this.STATUS_CODE.OK)
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }
}

module.exports.ordersHandler = new OrdersHandler