const validators = require('../../validators/validators')
const { body, validationResult } = require('express-validator')

exports.accountFileds = [
    body('email')
        .notEmpty()
        .withMessage('Поле обязательно')
        .custom(validators.emailValidator)
        .withMessage('Невалидный email')
        .custom(validators.uniqueEmailValidator)
        .withMessage('Данный email уже используется'),
    body('password')
        .notEmpty()
        .withMessage('Поле обязательно')        
]

exports.userFields = [
    body('serialPass')
        .notEmpty()
        .withMessage('Поле обязательно')
        .custom(validators.serialPassValidator)
        .withMessage('Невалидная строка, ожидается длина === 4'),
    body('numberPass')
        .notEmpty()
        .withMessage('Поле обязательно')
        .custom(validators.numberPassValidator)
        .withMessage('Невалидная строка, ожидается длина === 6'),
    body('photoPass')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('photoFirstName')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('photoLastName')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('photoSecondName')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('firstName')
        .notEmpty()
        .withMessage('Поле обязательно')
        .custom(validators.nameValidator)
        .withMessage('Невалидное имя'),
    body('lastName')
        .notEmpty()
        .withMessage('Поле обязательно')
        .custom(validators.nameValidator)
        .withMessage('Невалидное имя'),
    body('secondName')
        .custom(validators.nameValidator)
        .withMessage('Невалидное имя'),
    body('gender')
        .notEmpty()
        .withMessage('Поле обязательно')
        .custom(validators.genderValidator)
        .withMessage('Невалидное значение'),
    body('telegram')
        .custom(validators.telegramValidator)
        .withMessage('Невалидное имя'),
]

exports.validateFields = (request, response, next) => {
    const errors = validationResult(request)
        .formatWith(errors => {
            return errors.msg
        })
        .mapped()
    if (Object.keys(errors).length !== 0) {
        return response.status(400).json({errors: errors})
    }
    next()
}