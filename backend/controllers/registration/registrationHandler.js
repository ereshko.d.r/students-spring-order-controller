const {knex} = require('../../knex')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const config = require('../../config')
const { BaseHandler } = require('../baseHandler')
const { checkJsonWebToken } = require('../../middlewares/auth/checkJsonWebToken')
const { ConvertToImage } = require('../../services/convertFromBase64')

const {accountFileds, userFields, validateFields} = require('./validators')

class RegistrationHandler extends BaseHandler{
    constructor() {
        super(['GET', 'POST'])
    }
    routers() {
        this.router.post(
            '/',
            accountFileds,
            validateFields,
            async (request, response) => this.createAccount(request, response)
        )
        this.router.get(
            '/user',
            async (request, response) => this.userState(request, response)
        )

        this.router.post(
            '/user',
            checkJsonWebToken,
            userFields,
            validateFields,
            (request, response) => this.cerateUserData(request, response)
        )
        this.router.post(
            '/bage',
            checkJsonWebToken,
            (request, response) => this.createUserBage(request, response)
        )
    }

    async createAccount(request, response) {
        const {email, password} = request.body
        const trx = await knex.transaction()
        try {
            const data = await trx('users')
                .insert({role: 'user'})
                .returning('id')
            const userId = data[0].id
            await trx('users_status').insert({'user_id': userId})
            const hashPassword = await bcrypt.hash(password, 10)
            const accessToken = jwt
                .sign({id: userId}, config.secretKey, {expiresIn: '10m'})
            const refreshToken = jwt
                .sign({id: userId}, config.secretKey)
            await trx('accounts').insert({
                'user_id': userId,
                'email': email,
                'password': hashPassword,
                'refresh_token': refreshToken
            })
            await trx.commit()
            response.cookie('refreshToken', refreshToken, {httpOnly: true})
            response.status(this.STATUS_CODE.CREATED).json({
                accessToken: accessToken
            })

        }
        catch(error) {
            console.log(error)
            await trx.rollback()
            response.sendStatus(this.STATUS_CODE.BAD_REQUEST)
        }   
    }

    async userState(request, response) {
        console.log('here')
        const {refreshToken} = request.cookies
        if (!refreshToken) {
            return response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        const {id} = jwt.decode(refreshToken, config.secretKey, (error, decode) => {
                if (error) {
                    console.log('error')
                    return response.sendStatus(this.STATUS_CODE.NOT_FOUND)
                }
        })
        console.log(id)
        try {
            const users = await knex('users')
                .select({
                    'id': 'id',
                    'firstName': 'first_name',
                    'lastName': 'last_name'
                })
                .where('id', id).first()
            const bio = await knex('users_bio')
                .select('avatar')
                .where('user_id', id).first()
            response.status(this.STATUS_CODE.OK).json({
                'id': users?.id,
                'firstName': users?.firstName,
                'lastName': users?.lastName,
                'avatar': bio?.avatar
            })
        }
        catch(error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async cerateUserData(request, response) {
        const userId = request.user
        const {
            firstName,
            lastName,
            secondName,
            gender,
            telegram,
            photoPass,
            photoFirstName,
            photoLastName,
            photoSecondName,
            serialPass,
            numberPass
        } = request.body
        const trx = await knex.transaction()
        try {
            await trx('users')
                .update({
                    'first_name': firstName,
                    'last_name': lastName,
                    'second_name': secondName,
                    'gender': gender
                })
                .where('id', userId)
            await trx('users_bio')
                .update({
                    'telegram': telegram
                })
                .where({'user_id': userId})
            const photoPath = new ConvertToImage(photoPass)
                .saveToPrivatDir()
            const firstNamePath = new ConvertToImage(photoFirstName)
                .saveToPrivatDir()
            const lastNamePath = new ConvertToImage(photoLastName)
                .saveToPrivatDir()
            const secondNamePath = new ConvertToImage(photoSecondName)
                .saveToPrivatDir()
            await trx('users_docs')
                .insert({
                    'user_id': userId,
                    'pass_photo': photoPath,
                    'pass_photo_first_name': firstNamePath,
                    'pass_photo_last_name': lastNamePath,
                    'pass_photo_second_name': secondNamePath,
                    'pass_serial': serialPass,
                    'pass_number': numberPass
                })
                .where({'user_id': userId})
            await trx.commit()
            response.sendStatus(this.STATUS_CODE.CREATED)
        }
        catch(error) {
            console.log(error)
            await trx.rollback()
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async createUserBage(request, response) {
        const userId = request.user
        const {bage, avatar} = request.body
        const bagePath = new ConvertToImage(bage).saveToPrivatDir()
        const avatarPath = new ConvertToImage(avatar).saveToPublickDir()
        knex('users_bio')
            .insert({
                'user_id': userId,
                'bage': bagePath,
                'avatar': avatarPath
            })
            .then(() => {
                response.sendStatus(this.STATUS_CODE.CREATED)
            })
            .catch(() => {
                response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
            })
    }
}

module.exports.registrationHandler = new RegistrationHandler
