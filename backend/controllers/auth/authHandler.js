const { BaseHandler } = require('../baseHandler')
const { knex } = require('../../knex')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const config = require('../../config')
const { EmailSender } = require('../../emailSender')
const { authFields, validateFields} = require('./validators')


class AuthHandler extends BaseHandler {
    constructor(){
        super(['GET', 'POST'])
    }

    routers() {
        this.router.post(
            '/',
            authFields,
            validateFields,
            async (request, response) => this.authorization(request, response)
        )
        this.router.get(
            '/refresh',
            async (request, response) => this.refreshToken(request, response)
        )
        this.router.get(
            '/logout',
            async (request, response) => this.logout(request, response)
        )
        this.router.post(
            '/reset_password',
            (request, response) => this.resetPassword(request, response)
        )
    }

    async getUser(email) {
        return await knex('accounts')
            .select({
                'userId': 'user_id',
                'email': 'email',
                'password': 'password'
            })
            .where('email', email)
            .first()
    }

    createTokens(userId) {
        const accessToken = jwt
            .sign({id: userId}, config.secretKey, {expiresIn: '10m'})
        const refreshToken = jwt
            .sign({id: userId}, config.secretKey)
        return {accessToken, refreshToken}
    }

    async checkPassword(password, hashPassword) {
        return await bcrypt.compare(password, hashPassword)
    }


    async authorization(request, response) {
        const {email, password} = request.body
        console.log('here')
        const userData = await this.getUser(email)
        if (!userData) {
            return response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        const isPasswordCorrect = await this
            .checkPassword(password, userData.password)
        if (!isPasswordCorrect) {
            return response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        const {accessToken, refreshToken} = this
            .createTokens(userData.userId)
        knex('accounts')
            .update({refresh_token: refreshToken})
            .where('user_id', userData.userId)
            .then(async () => {
                const {role} = await knex('users')
                    .select('role')
                    .where('id', userData.userId)
                    .first()
                response.cookie(
                    'refreshToken',
                    refreshToken,
                    {
                        httpOnly: true
                    }
                )
                response.status(200).json({
                    accessToken: accessToken,
                    role: role
                })
                console.log(response)
            })
            .catch(() => {
                response.sendStatus(500)
            })
    }

    removeCookie(response) {
        response.setHeader('Set-Cookie', 'refreshToken=; expires=-1; path=/;')
        response.status(this.STATUS_CODE.UNAUTHORIZED).send()
    }

    async logout(request, response) {
        this.removeCookie(response)
    }

    async refreshToken(request, response) {
        console.log('refresh')
        const {refreshToken} = request.cookies
        if (!refreshToken) {
            console.log('refresh token dont exsist')
            return this.removeCookie(response)
        }
        const userData = await knex('accounts')
            .select({
                'id': 'user_id',
                'refreshToken': 'refresh_token'
            })
            .where('refresh_token', refreshToken)
            .returning('id')
            .first()
        if (!userData) {
            console.log('user data dont exsist')
            return this.removeCookie(response)
        }
        try {
            jwt.verify(refreshToken, config.secretKey, (error, decoded) => {
                if(error) {
                    return this.removeCookie(response)
                }
                if (userData.id !== decoded.id) {
                    return this.removeCookie(response)
                }
                const accessToken = jwt.sign(
                    {id: decoded.id},
                    config.secretKey,
                    {expiresIn: '10m'}
                )
                console.log('send token')
                response.status(this.STATUS_CODE.CREATED).json({
                    accessToken: accessToken
                })
            })
        }
        catch(error) {
            console.log(error)
            return this.removeCookie(response)
        }
    }

    async resetPassword(request, response) {
        const { email } = request.body
        const trx = await knex.transaction()
        try {
            const result = await trx('accounts')
                .select({
                    'email': 'email',
                    'userId': 'user_id'
                })
                .where('email', email)
                .first()
            if (!result) {
                return null
            }
            const resetToken = jwt.sign(
                {email: result.email},
                config.secretKey,
                {expiresIn: '24h'}
            )
            await trx('reset_tokens')
                .insert({
                    'account_user_id': result.userId,
                    'reset_token': resetToken
                })
            const URL = '178.72.89.213:8000/reset-password'
            const link = `${URL}/${resetToken}`
            const sender = new EmailSender()
            sender.sendMail(
                email,
                'Восстановление пароля',
                `Для восстановления пароля перейдите по ссылке: ${link}`
            )
            trx.commit()
        }
        catch (error) {
            trx.rollback()
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }
}

exports.authHandler = new AuthHandler