const { body, validationResult } = require('express-validator')

exports.authFields = [
    body('email')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('password')
        .notEmpty()
        .withMessage('Поле обязательно')
]

exports.validateFields = (request, response, next) => {
    const errors = validationResult(request)
        .formatWith(errors => {
            return errors.msg
        })
        .mapped()
    if (Object.keys(errors).length !== 0) {
        return response.status(400).json({errors: errors})
    }
    next()
}