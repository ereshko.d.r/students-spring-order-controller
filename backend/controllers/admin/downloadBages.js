const path = require('path')
const archiver = require('archiver')
const { knex } = require('../../knex')
const { baseDir } = require('../../config')

const collectBages = async (request, response) => {
    try {
        const bagesToArchive = await knex('users_bio')
            .leftJoin('users', 'users_bio.user_id', 'users.id')
            .leftJoin('users_status', 'users.id', 'users_status.user_id')
            .where('users.role', 'user')
            .where('users_status.status', true)
            .select('users_bio.bage')
            .then(data => data.reduce(
                (arr, data) => {
                    if (data.bage) {
                        return arr.concat(path.join(baseDir, data.bage))
                    }
                    return arr
                }, [])
            )
        const archive = archiver('zip', {
            zlib: { level: 0 }
        })
        response.attachment('bages.zip')
        archive.pipe(response)
        bagesToArchive.forEach((file, index) => {
            if (file && typeof file === 'string') {
                archive.file(file, { name: `bage_${index}.png`})
            }
        })
        archive.finalize()
    }
    catch (error) {
        console.log(error)
        response.sendStatus(500)
    }
}

module.exports.collectBages = collectBages
