const { BaseHandler } = require('../baseHandler')
const { knex } = require('../../knex')
const { eventSourceServer } = require('../../eventSourceServer')
const pkg = require('../../App')
const { checkIsHasPermissions } = require('../../middlewares/admin/checkIsHasPermissions')
const { checkJsonWebToken } = require('../../middlewares/auth/checkJsonWebToken')
const { validateFields, participantVerdictValidator } = require('./validator')

class ParticipantsHandler extends BaseHandler {
    constructor() {
        super(['GET', 'POST', 'PATCH', 'DELETE'])
    }

    middleware() {
        this.router.use(checkJsonWebToken)
        this.router.use(checkIsHasPermissions)
    }

    routers() {
        this.router.get(
            '/',
            (request, response) => this.participantsList(request, response)
        )
        this.router.get(
            '/:userId',
            (request, response) => this.participantDetails(request, response)
        )
        this.router.patch(
            '/:userId',
            participantVerdictValidator,
            validateFields,
            (request, response) => this.participantVerdict(request, response)
        )
    }

    async participantsList(request, response) {
        try {
            const {page, limit, name, status} = request.query.search || {}

            const admitted = await knex('users')
                .join('users_status', 'users.id', 'users_status.user_id')
                .where('users_status.status', true)
                .where('users.role', 'user')
                .count().first()
            const notAdmitted = await knex('users')
                .join('users_status', 'users.id', 'users_status.user_id')
                .where('users_status.status', false)
                .where('users.role', 'user')
                .count().first()
            let query = knex('users')
                .join('users_bio', 'users.id', 'users_bio.user_id')
                .join('users_status', 'users.id', 'users_status.user_id')
                .select({
                    'id': 'users.id',
                    'firstName': 'first_name',
                    'lastName': 'last_name',
                    'secondName': 'second_name',
                    'avatar': 'users_bio.avatar',
                    'status': 'users_status.status',
                    'gender': 'gender'
                })
                .where('users.role', 'user')
            if (name) {
                query = query.where(function () {
                    const nameParts = name.toLowerCase().split(' ')
                    nameParts.forEach((namePart) => {
                        this.orWhere(function () {
                        this.whereRaw(
                            'LOWER(users.first_name) LIKE ?', `%${namePart}%`
                        )
                        this.orWhereRaw(
                            'LOWER(users.last_name) LIKE ?', `%${namePart}%`
                        )
                        this.orWhereRaw(
                            'LOWER(users.second_name) LIKE ?', `%${namePart}%`
                        )
                        })
                    })
                })
              }

            if (status === 'null') {
                query = query.whereNull('users_status.status')
            }
            if (status && status !== 'null') {
                query = query
                    .where('status', status)
            }
            const countQuery = query
                .clone()
                .clearSelect()
                .count('* as total')
            const countResult = await countQuery.first()
            const totalCount = countResult.total
            const users = await query
                .limit(limit)
                .offset((page - 1) * limit)
                .orderBy('users.id', 'desc')
            response.status(this.STATUS_CODE.OK).json({
                result: users,
                page: page,
                limit: limit,
                totalCount: totalCount,
                admitted: admitted.count,
                notAdmitted: notAdmitted.count
            })
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async participantDetails(request, response) {
        const {userId} = request.params
        try {
            const user = await knex('users')
                .join('users_bio', 'users.id', 'users_bio.user_id')
                .join('users_docs', 'users.id', 'users_docs.user_id')
                .join('users_status', 'users.id', 'users_status.user_id')
                .leftJoin('order_users', 'users.id', 'order_users.user_id')
                .leftJoin('orders', 'order_users.order_id', 'orders.id')
                .leftJoin(
                    'orders_status', 'orders.id', 'orders_status.order_id'
                )
                .leftJoin(
                    'nominations', 'orders.nomination_id', 'nominations.id'
                )
                .select({
                    'id': 'users.id',
                    'firstName': 'users.first_name',
                    'lastName': 'users.last_name',
                    'secondName': 'users.second_name',
                    'status': 'users_status.status',
                    'bagePhoto': 'users_bio.avatar',
                    'passPhoto': 'users_docs.pass_photo',
                    'photoFirstName': 'users_docs.pass_photo_first_name',
                    'photoLastName': 'users_docs.pass_photo_last_name',
                    'photoSecondName': 'users_docs.pass_photo_second_name',
                    'participantOrders': knex.raw(`
                        CASE WHEN COUNT(orders.id) > 0
                            THEN JSON_AGG(json_build_object(
                                'id', orders.id,
                                'groupName', orders.group_name,
                                'perfomanceName', orders.perfomance_name,
                                'nomination', nominations.name,
                                'orderStatus', orders_status.status,
                                'orderIsSent', orders_status.is_sent
                        ))
                            ELSE json_build_array()
                        END
                    `)
                })
                .where('users.id', userId)
                .groupBy(
                    'users.first_name', 'users.last_name',
                    'users.second_name', 'users_status.status',
                    'users_bio.avatar', 'users_docs.pass_photo',
                    'users_docs.pass_photo_first_name',
                    'users_docs.pass_photo_last_name',
                    'users_docs.pass_photo_second_name', 'users.id'
                )
                .first()
            response.status(this.STATUS_CODE.OK).json(user)
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async participantVerdict(request, response) {
        const { userId } = request.params
        const { status, bage, passport } = request.body
        try {
            const { userStatus } = await knex('users_status')
                .update({
                    'status': status,
                    'passport': passport,
                    'bage': bage
                })
                .where('user_id', userId)
                .returning({
                    'userStatus': 'status'
                })
                .then(data => data[0])
            const userOrders = await knex('order_users')
                .select({
                    'orderId': 'order_id'
                })
                .where('user_id', userId)
                .then(data => data.reduce((orders, data) => {
                    return orders.concat(String(data.orderId))
                }, []))
            pkg.websocket.to(userOrders).emit('orders:selfUpdate')
            eventSourceServer.updateStateClient(userId)
            response.status(this.STATUS_CODE.OK).json({status: userStatus})
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
        
    }
}

module.exports.participantsHandler = new ParticipantsHandler()