const { BaseHandler } = require('../baseHandler')
const { knex } = require('../../knex')
const { checkIsHasPermissions } = require('../../middlewares/admin/checkIsHasPermissions')
const { checkJsonWebToken } = require('../../middlewares/auth/checkJsonWebToken')
const { eventSourceServer } = require('../../eventSourceServer')
const { validateFields, orderVerdictValidator } = require('./validator')

class AdminOrdersHandler extends BaseHandler {
    constructor() {
        super(['GET', 'POST', 'PATCH', 'DELETE'])
    }

    middleware() {
        this.router.use(checkJsonWebToken)
        this.router.use(checkIsHasPermissions)
    }

    routers() {
        this.router.get(
            '/',
            (request, response) => this.getOrders(request, response)
        )
        this.router.get(
            '/:orderId',
            (request, response) => this.getOrderDetails(request, response)
        )
        this.router.patch(
            '/:orderId',
            orderVerdictValidator,
            validateFields,
            (request, response) => this.orderVerdict(request, response)
        )
    }

    async getOrders(request, response) {
        try {
            const {page, limit, name, status} = request.query.search || {}

            const admitted = await knex('orders')
                .join('orders_status', 'orders.id', 'orders_status.order_id')
                .where('orders_status.status', true)
                .count().first()
            const notAdmitted = await knex('orders')
                .join('orders_status', 'orders.id', 'orders_status.order_id')
                .where('orders_status.status', false)
                .count().first()
            let query = knex('orders')
                .join('directions', 'orders.direction_id', 'directions.id')
                .join('nominations', 'orders.nomination_id', 'nominations.id')
                .leftJoin(
                    'orders_status', 'orders.id', 'orders_status.order_id'
                )
                .select({
                    'id': 'orders.id',
                    'groupName': 'orders.group_name',
                    'perfomanceName': 'orders.perfomance_name',
                    'nomination': 'nominations.name',
                    'orderStatus': 'orders_status.status',
                    'orderIsSent': 'orders_status.is_sent'
                })
            if (name) {
                query = query.where(function () {
                    const nameParts = name.toLowerCase().split(' ')
                    nameParts.forEach((namePart) => {
                        this.orWhere(function () {
                            this.whereRaw(
                                'LOWER(orders.group_name) LIKE ?',
                                `%${namePart}%`
                            )
                            this.orWhereRaw(
                                'LOWER(orders.perfomance_name) LIKE ?',
                                `%${namePart}%`
                            )
                        })
                    })
                })
            }
            if (status === 'null') {
                query = query
                    .whereNull('orders_status.status')
                    .where('orders_status.is_sent', true)
            }
            else if (status === 'forming') {
                query = query
                    .where('orders_status.is_sent', false)
                    .whereNull('orders_status.status')
            }
            else if (status === 'true' || status === 'false') {
                query = query
                .where('orders_status.status', status)
            }
            else {
                query = query
                    .where('orders_status.is_sent', true)
                    .orWhere('orders_status.status', false)
            }
            const countQuery = query
                .clone()
                .clearSelect()
                .count('* as total')
            const countResult = await countQuery.first()
            const totalCount = countResult.total
            const users = await query
                .limit(limit)
                .offset((page - 1) * limit)
                .orderBy('orders.id', 'desc')
            response.status(this.STATUS_CODE.OK).json({
                result: users,
                page: page,
                limit: limit,
                totalCount: totalCount,
                admitted: admitted.count,
                notAdmitted: notAdmitted.count
            })
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async getOrderDetails(request, response) {
        const {orderId} = request.params
        try {
            const order = await knex('orders')
            .join('directions', 'orders.direction_id', 'directions.id')
            .join('nominations', 'orders.nomination_id', 'nominations.id')
            .join('order_users', 'orders.id', 'order_users.order_id')
            .join('users', 'order_users.user_id', 'users.id')
            .join('order_roles', 'order_users.role_id', 'order_roles.id')
            .leftJoin('orders_status', 'orders.id', 'orders_status.order_id')
            .leftJoin('users_status', 'users.id', 'users_status.user_id')
            .join('users_bio', 'users.id', 'users_bio.user_id')
            .select({
                'groupName': 'orders.group_name',
                'perfomanceName': 'orders.perfomance_name',
                'nominationName': 'nominations.name',
                'orderStatus': 'orders_status.status',
                'orderIsSent': 'orders_status.is_sent',
                'isPerfomance': 'directions.is_perfomance',
                'participants': knex.raw(`
                    JSON_AGG(json_build_object(
                        'orderRole', order_roles.name, 'firstName',
                        users.first_name, 'lastName', users.last_name,
                        'secondName', users.second_name, 'id', users.id,
                        'isJoin', order_users.is_join,
                        'avatar', users_bio.avatar,
                        'status', users_status.status
                    ))
                `)
            })
            .groupBy(
                'orders.group_name', 'orders.perfomance_name',
                'nominations.name', 'orders_status.status',
                'directions.is_perfomance', 'orders_status.id'
            )
            .where(
                'orders.id', orderId
            )
            .first()
            response.status(this.STATUS_CODE.OK).json(order)
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async orderVerdict(request, response) {
        const {orderId} = request.params
        const {
            groupName,
            perfomanceName,
            status
        } = request.body
        try {
            const orderStatus = await knex('orders_status')
                .update({
                    'group_name': groupName,
                    'perfomance_name': perfomanceName,
                    'status': status,
                    'is_sent': status
                })
                .where('order_id', orderId)
                .returning({'orderStatus': 'status'})
                .then(data => data[0])
            await knex('order_users')
                .select({
                    'id': 'user_id'
                })
                .where('order_id', orderId)
                .then(data => data.forEach((user) => {
                    eventSourceServer.updateOrdersClients(user.id)
                }))
            response.status(this.STATUS_CODE.OK).json(orderStatus)
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }
}

module.exports.adminOrdersHandler = new AdminOrdersHandler