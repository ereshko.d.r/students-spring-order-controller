const { body, validationResult } = require('express-validator')

exports.orderVerdictValidator = [
    body('groupName')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('status')
        .notEmpty()
        .withMessage('Поле обязательно')
]

exports.participantVerdictValidator = [
    body('status')
        .notEmpty()
        .withMessage('Поле обязательно')
]


exports.validateFields = (request, response, next) => {
    const errors = validationResult(request)
        .formatWith(errors => {
            return errors.msg
        })
        .mapped()
    if (Object.keys(errors).length !== 0) {
        return response.status(400).json({errors: errors})
    }
    next()
}