const express = require('express')

/**
 * BaseHandler class
 * Using to create new handlres for routing
 */

class BaseHandler {
    STATUS_CODE = {
        OK:                     200,
        CREATED:                201,
        ACCEPTED:               202,
        NO_CONTENT:             204,

        MOVED_PERMAMENTLY:      301,
        FOUND:                  302,

        BAD_REQUEST:            400,
        UNAUTHORIZED:           401,
        FORBIDDEN:              403,
        NOT_FOUND:              404,
        NOT_ALLOWED_METHOD:     405,

        INTERNAL_SERVER_ERROR:  500
    }

    /**
     * it is allowing request methods for handlering
     * 
     * required param. If you don't use it, handler won't accept requests
     * 
     * used methods: [`'GET'`, `'POST'`, `'PUT`, `'PATCH'`, `'DELETE'`]
     * @param {Array} allowedMethods 
     * @returns 
     */

    constructor(allowedMethods) {
        this.allowedMethods = allowedMethods || []
        this.router = express.Router()
        this.router.use((request, response, next) => {
            if (!this.#checkAllowedMethods(request.method)) {
                return response.sendStatus(this.STATUS_CODE.NOT_ALLOWED_METHOD)
            }
            next()
        })
        this.middleware()
        this.routers()
        return this.router
    }

    #checkAllowedMethods(method) {
        return this.allowedMethods.includes(method)
    }

    /**
     * Mehtod wich add middlewares for all handlers
     */

    middleware() {

    }

    /**
     * Method for adding handlers
     */

    routers() {

    }

}

exports.BaseHandler = BaseHandler