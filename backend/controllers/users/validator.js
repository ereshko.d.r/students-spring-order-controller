const validators = require('../../validators/validators')
const { param, body, validationResult } = require('express-validator')

exports.requestParams = [
    param('userId')
        .isInt()
        .withMessage('must be int'),
]

exports.getUserByIdValidator = [
    body('email')
        .notEmpty()
        .withMessage('Поле обязательно')
]

exports.checkIsUserInOrderValidator = [
    body('email')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('orderId')
        .notEmpty()
        .withMessage('Поле обязательно')
]

exports.updateUserPassValidator = [
    body('firstName')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('lastName')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('gender')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('serialPass')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('numberPass')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('photoPass')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('photoFirstName')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('photoLastName')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('photoSecondName')
        .notEmpty()
        .withMessage('Поле обязательно')
]

exports.updateBageValidator = [
    body('bage')
        .notEmpty()
        .withMessage('Поле обязательно'),
    body('avatar')
        .notEmpty()
        .withMessage('Поле обязательно')
]

exports.validateFields = (request, response, next) => {
    const errors = validationResult(request)
        .formatWith(errors => {
            return errors.msg
        })
        .mapped()
    if (Object.keys(errors).length !== 0) {
        return response.status(400).json({errors: errors})
    }
    next()
}