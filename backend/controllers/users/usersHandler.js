const { BaseHandler } = require('../baseHandler')
const {knex} = require('../../knex')
const { eventSourceServer } = require('../../eventSourceServer')
const { checkJsonWebToken } = require('../../middlewares/auth/checkJsonWebToken')
const {
    requestParams, validateFields,
    getUserByIdValidator, checkIsUserInOrderValidator,
    updateUserPassValidator, updateBageValidator
} = require('./validator')
const { ConvertToImage } = require('../../services/convertFromBase64')

class UsersHandler extends BaseHandler {
    constructor() {
        super(['GET', 'POST', 'PATCH'])
    }

    middleware() {
        this.router.use(checkJsonWebToken)
    }

    routers() {
        this.router.get(
            '/',
            (request, response) => this.getUsers(request, response)
        )
        this.router.get(
            '/me',
            (request, response) => this.getMe(request, response)
        )
        this.router.post(
            '/email',
            getUserByIdValidator,
            validateFields,
            (request, response) => this.getUserByEmail(request, response)
        )
        this.router.post(
            '/emails',
            getUserByIdValidator,
            validateFields,
            (request, response) => this.getUsersListByEmail(request, response)
        )
        this.router.post(
            '/orders',
            checkIsUserInOrderValidator,
            validateFields,
            (request, response) => this.checkIsUserInOrder(request, response)
        )
        this.router.get(
            '/:userId',
            requestParams,
            validateFields,
            (request, response) => this.getUser(request, response)
        )
        this.router.patch(
            '/pass',
            updateUserPassValidator,
            validateFields,
            (request, response) => this.updateUserPass(request, response)
        )
        this.router.patch(
            '/bage',
            updateBageValidator,
            validateFields,
            (request, response) => this.updateUserBage(request, response)
        )
    }

    async getUserObject(userId) {
        return await knex('users')
            .join('users_bio', 'users.id', 'users_bio.user_id')
            .join('users_status', 'users.id', 'users_status.user_id')
            .select({
                'id': 'users.id',
                'lastName': 'users.last_name',
                'firstName': 'users.first_name',
                'secondName': 'users.second_name',
                'gender': 'users.gender',
                'role': 'users.role',
                'avatar': 'users_bio.avatar',
                'bage': 'users_bio.bage',
                'status': 'users_status.status',
                'passportStatus': 'users_status.passport',
                'bageStatus': 'users_status.bage'
            })
            .where('users.id', userId)
            .first()
            .then(data => {
                
                if(!data) {
                    return false
                }
                return data
            })
    }

    getUsers(request, response) {
        knex('users')
            .select('*')
            .then(data => {
                response.status(this.STATUS_CODE.OK).json(data)
            })
            .catch(() => {
                response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
            })
    }

    async getUserByEmail(request, response) {
        const {email} = request.body
        try {
            const user = await knex('users')
                .join('accounts', 'users.id', 'accounts.user_id')
                .select({
                    'id': 'users.id',
                    'firstName': 'users.first_name',
                    'lastName': 'users.last_name',
                    'secondName': 'users.second_name',
                    'email': 'accounts.email'
                })
                .where('accounts.email', email)
                .first()
            if (user) {
                return response.status(this.STATUS_CODE.OK).json(user)
            }
            response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async getUsersListByEmail(request, response) {
        const { email } = request.body
        try {
            const usersList = await knex('accounts')
                .leftJoin('users', 'accounts.user_id', 'users.id')
                .select('email')
                .where('email', 'like', `%${email}%`)
                .where('users.role', 'user')
            response.status(this.STATUS_CODE.OK)
                .json(usersList.map(({ email }) => email))
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async checkIsUserInOrder(request, response) {
        const {email, orderId} = request.body
        try {
            const userOrders = await knex('order_users')
                .join('accounts', 'order_users.user_id', 'accounts.user_id')
                .select({
                    'id': 'order_users.order_id'
                })
                .where({
                    'order_users.order_id': orderId,
                    'accounts.email': email
                })
            if (userOrders.length > 0) {
                return response.sendStatus(this.STATUS_CODE.OK)
            }
            response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async getMe(request, response) {
        const userId = request.user
        const userObject = await this.getUserObject(userId)
        if (!userObject) {
            return response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        response.status(this.STATUS_CODE.OK).json(userObject)
        
    }

    async getUser(request, response) {
        const {userId} = request.params
        const userObject = await this.getUserObject(userId)
        if (!userObject) {
            return response.sendStatus(this.STATUS_CODE.NOT_FOUND)
        }
        response.status(this.STATUS_CODE.OK).json(userObject)
    }

    async updateUserPass(request, response) {
        const userId = request.user
        const {
            firstName, lastName, secondName,
            gender, serialPass, numberPass,
            photoPass, photoFirstName,
            photoLastName, photoSecondName
        } = request.body
        const trx = await knex.transaction()
        try {
            await trx('users')
                .update({
                    'first_name': firstName,
                    'last_name': lastName,
                    'second_name': secondName,
                    'gender': gender
                })
                .where('id', userId)
            const photoPath = new ConvertToImage(photoPass)
                .saveToPrivatDir()
            const firstNamePath = new ConvertToImage(photoFirstName)
                .saveToPrivatDir()
            const lastNamePath = new ConvertToImage(photoLastName)
                .saveToPrivatDir()
            const secondNamePath = new ConvertToImage(photoSecondName)
                .saveToPrivatDir()
            await trx('users_docs')
                .where('user_id', userId)
                .update({
                    'pass_serial': serialPass,
                    'pass_number': numberPass,
                    'pass_photo': photoPath,
                    'pass_photo_first_name': firstNamePath,
                    'pass_photo_last_name': lastNamePath,
                    'pass_photo_second_name': secondNamePath
                })
                await trx('users_status')
                .where('user_id', userId)
                .update({
                    'status': null,
                    'passport': null
                })
            trx.commit()
            response.sendStatus(this.STATUS_CODE.OK)
            eventSourceServer.updateStateClient(userId)
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }

    async updateUserBage(request, response) {
        const userId = request.user
        const { bage, avatar } = request.body
        const trx = await knex.transaction()
        try {
            const bagePath = new ConvertToImage(bage).saveToPrivatDir()
            const avatarPath = new ConvertToImage(avatar).saveToPublickDir()
            await trx('users_bio')
                .update({
                    'bage': bagePath,
                    'avatar': avatarPath
                })
                .where('user_id', userId)
            await trx('users_status')
                .update({
                    'status': null,
                    'bage': null
                })
                .where('user_id', userId)
            await trx.commit()
            response.sendStatus(this.STATUS_CODE.OK)
            eventSourceServer.updateStateClient(userId)
        }
        catch (error) {
            console.log(error)
            response.sendStatus(this.STATUS_CODE.INTERNAL_SERVER_ERROR)
        }
    }
}

module.exports.usersHandler = new UsersHandler