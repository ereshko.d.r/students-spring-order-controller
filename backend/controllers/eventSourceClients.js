const {knex} = require('../knex')

/**
 * EventSourceServer - class for storing and manipulating connected client. 
 */

class EventSourceServer {
    clientsList = []

    /**
     * Method to add connected user to `clientList`
     * @param {Number} userId 
     * @param {Object} response 
     */

    addClient(userId, response) {
        console.log(`EVENT SOURCE: userId: ${userId} has been connected`)
        if (!this.getClient(userId)) {
            this.clientsList.push({userId, response})
        }
        else {
            const client = this.getClient(userId)
            client.response = response
        }
        this.updateOrdersClients(userId)
        this.updateInvitesClients(userId)
        console.log(`EVENT SOURCE: active users: ${this.clientsList.length}`)
    }

    /**
     * Method to get client from `clientList`
     * @param {Number} userId 
     * @returns {Object}
     */

    getClient(userId) {
        const index = this.clientsList
            .findIndex((obj) => Number(userId) === obj.userId)
        return index === -1 ? null : this.clientsList[index]
    }

    /**
     * Method to remove client from `clientList`
     * @param {Number} userId 
     */

    removeClient(userId) {
        const index = this.clientsList
            .findIndex((obj) => Number(userId) === obj.userId)
        if (index !== -1) {
            this.clientsList.splice(index, 1)
        }
        console.log(`EVENT SOURCE: userId: ${userId} has been disconnected`)
        console.log(`EVENT SOURCE: active users: ${this.clientsList.length}`)
    }

    async getOrdersClient(userId) {
        try {
            const orders = await knex('orders')
                .join('order_users', 'orders.id', 'order_users.order_id')
                .join('nominations', 'orders.nomination_id', 'nominations.id')
                .leftJoin(
                    'orders_status', 'orders.id', 'orders_status.order_id'
                )
                .select({
                    'id': 'orders.id',
                    'groupName': 'orders.group_name',
                    'perfomanceName': 'orders.perfomance_name',
                    'orderStatus': 'orders_status.status',
                    'orderIsSent': 'orders_status.is_sent',
                    'nominationName': 'nominations.name',
                    'userId': 'order_users.user_id'
                })
                .where({
                    'order_users.user_id': userId,
                    'order_users.is_join': true
                })
                .orderBy(
                    'orders_status.is_sent', 'desc'
                )
            return orders
        }
        catch (error) {
            console.log(error)
        }
        return null
    }

    async getInvitesClient(userId) {
        
        try {
            const invites = await knex('invites')
                .join('orders', 'invites.order_id', 'orders.id')
                .join('nominations', 'orders.nomination_id', 'nominations.id')
                .select({
                    'orderId': 'orders.id',
                    'groupName': 'orders.group_name',
                    'perfomanceName': 'orders.perfomance_name',
                    'nominationName': 'nominations.name',
                    'userId': 'invites.user_id'
                })
                .where({
                    'invites.user_id': userId,
                })
            return invites
        }
        catch (error) {
            console.log(error)
        }
        return null
    }

    async getStateClient(userId) {
        try {
            const user = await knex('users')
            .join('users_bio', 'users.id', 'users_bio.user_id')
            .join('users_status', 'users.id', 'users_status.user_id')
            .select({
                'id': 'users.id',
                'lastName': 'users.last_name',
                'firstName': 'users.first_name',
                'secondName': 'users.second_name',
                'gender': 'users.gender',
                'role': 'users.role',
                'avatar': 'users_bio.avatar',
                'bage': 'users_bio.bage',
                'status': 'users_status.status',
                'passportStatus': 'users_status.passport',
                'bageStatus': 'users_status.bage'
            })
            .where('users.id', userId)
            .first()
            return user
        }
        catch (error) {
            console.log(error)
        }
        return null
    }

    /**
     * 
     * @param {[Array, Number]} userId 
     */

    async updateOrdersClients(userId) {
        const clients = []
        if (userId instanceof Array) {
            for (const id of userId) {
                const client = this.getClient(id)
                client ? clients.push(client) : false
            }
        }
        else {
            const client = this.getClient(userId)
            client ? clients.push(client) : false
        }
        const orders = await this.getOrdersClient(userId)
        if (clients.length > 0) {
            const data = `data: ${JSON.stringify({orders})}\n\n`
            clients.forEach(client => {
                client.response.write(data)
            })
        }
    }


    /**
     * 
     * @param {[Array, Number]} userId 
     */

    async updateInvitesClients(userId) {
        const clients = []
        if (userId instanceof Array) {
            for (const id of userId) {
                const client = this.getClient(id)
                client ? clients.push(client) : false
            }
        }
        else {
            const client = this.getClient(userId)
            client ? clients.push(client) : false
        }
        const invites = await this.getInvitesClient(userId)
        if (clients.length > 0) {
            clients.forEach(client => {
                client.response
                    .write(`data: ${JSON.stringify({invites})}\n\n`)
            })
        }
    }

    /**
     * 
     * @param {Number} userId 
     */

    async updateStateClient(userId) {
        const state = await this.getStateClient(userId)
        const client = this.getClient(userId)
        if (client) {
            client.response.write(`data: ${JSON.stringify({state})}\n\n`)
        }
    }
}

module.exports.EventSourceServer = EventSourceServer