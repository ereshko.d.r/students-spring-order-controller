const express = require('express')
const router = express.Router()


const { registrationHandler } = require('./registration/registrationHandler')
const { authHandler } = require('./auth/authHandler')
const { usersHandler } = require('./users/usersHandler')
const { ordersHandler } = require('./orders/ordersHandler')
const { participantsHandler } = require('./admin/participantsHandler')
const { adminOrdersHandler } = require('./admin/ordersHandler')

const { collectBages } = require('../controllers/admin/downloadBages')
const { checkJsonWebToken } = require('../middlewares/auth/checkJsonWebToken')
const { eventSourceMiddleware } = require('../middlewares/eventSource/eventSourceMiddleware')
const { checkRefreshToken } = require('../middlewares/private/checkRefreshToken')

router.use('/registration', registrationHandler)
router.use('/auth', authHandler)
router.use('/users', usersHandler)
router.use('/orders', ordersHandler)
router.use('/admin/participants', participantsHandler)
router.use('/admin/orders', adminOrdersHandler)
router.use('/events', checkJsonWebToken, eventSourceMiddleware)

router.get('/download', checkRefreshToken ,collectBages)

module.exports.router = router