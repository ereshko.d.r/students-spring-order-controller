const confing = require('./config')

const knexfile = require('./db/knexfile')
const knex = require('knex')(knexfile[confing.env || 'development'])

module.exports.knex = knex