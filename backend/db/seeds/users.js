const bcrypt = require('bcrypt')
const { fakerRU, Faker } = require('@faker-js/faker')

const USERS_COUNT = 2000


const createRandomUser = () => {
    const gender = fakerRU.person.sex()
    return {
        first_name: fakerRU.person.firstName(gender),
        last_name: fakerRU.person.lastName(gender),
        second_name: fakerRU.person.middleName(gender),
        role: 'user',
        gender: gender
    }
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async (knex) => {
    await knex('users').del()
    await knex('users_bio').del()
    const trx = await knex.transaction()
    try {
        const usersId = await trx('users')
            .insert(fakerRU.helpers.multiple(createRandomUser, {count: USERS_COUNT}))
            .returning({'user_id': 'id'})
        await trx('users_bio')
            .insert(usersId)
        let users = []
        for (const user of usersId) {
            user.status = fakerRU.datatype.boolean(0.85)
            users.push(user)
        }
        await trx('users_status')
            .insert(users)
        
        await trx.commit()
    }
    catch (error) {
        await trx.rollback()
        console.log(error)
    }
}