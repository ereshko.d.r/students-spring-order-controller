const directions = [
    {name: 'Танцевальное'}, {name: 'Мода'},
    {name: 'Оригинальный жанр'},{name: 'Театральное'},
    {name: 'Музыкальное'}, {name: 'Концертные программы'},
    {name: 'Интеллектуальные игры', is_perfomance: false},
    {name: 'Машины Голдберга', is_perfomance: false},
    {name: 'Хакатон', is_perfomance: false}, {name: 'Медиа'},
    {name: 'Видео'}, {name: 'Арт'}
]

const nominations = [
    [
        {name: 'Народный танец'},
        {name: 'Эстрадный танец'},
        {name: 'Современный танец'},
        {name: 'Бально-спортивный танец'},
        {name: 'Уличные танцы'},
        {name: 'Классический танец'},
    ],
    [
        {name: 'Готовое к носке'},
        {name: 'Концептуальная мода'},
        {name: 'Мода мегаполисов'},
        {name: 'Спортивная мода'},
        {name: 'Современная мода'},
        {name: 'Вечерняя мода'},
        {name: 'Косплей'},
    ],
    [
        {name: 'Цирковое исскуство'},
        {name: 'Пантомима'},
        {name: 'Синтез-номер'},
        {name: 'Иллюзия'},
        {name: 'Оригинальный номер'},
        {name: 'Чир данс шоу'},
    ],
    [
        {name: 'Художественное слово'},
        {name: 'Эстрадный монолог'},
        {name: 'Эстрадная миниатюра'},
        {name: 'Театр малых форм'},
        {name: 'Эксперимент'},
    ],
    [
        {name: 'Академическое пение'},
        {name: 'Народное пение'},
        {name: 'Эстрадное пение'},
        {name: 'Джазовое пение'},
        {name: 'Авторская песня'},
        {name: 'Битбокс'},
        {name: 'Реп'},
        {name: 'Инструментальное исполнение'},
        {name: 'Электронная музыка и диджеинг'},
        {name: 'ВИА'},
    ],
    [
        {name: 'Концертная программа'}
    ],
    [
        {name: 'Что? Где? Когда?'},
        {name: 'Брейн-ринг'},
        {name: 'Мультиигры'}
    ],
    [
        {name: 'Машины Голдберга'},
    ],
    [
        {name: 'Хакатон'},
    ],
    [
        {name: 'Аудиоподкаст'},
        {name: 'Фотопроект'},
        {name: 'Публикация'},
        {name: 'Видеорепортаж'},
        {name: 'SMM-проект'},
    ],
    [
        {name: 'Музыкальный клип'},
        {name: 'Юмористический ролик'},
        {name: 'Короткометражный фильм'},
        {name: 'Документальный ролик'},
        {name: 'Рекламный ролик'},
    ],
    [
        {name: 'Кастомизация'},
        {name: 'Стрит-арт'},
        {name: 'Графический дизайн'},
        {name: 'Моушн-дизайн'},
    ]
]

/**
* 
* @param {import('knex').Knex} knex 
* @returns {Promise<void>}
*/

exports.seed = async (knex) => {
    await knex.raw(
        `DELETE FROM directions CASCADE`
    )
    const trx = await knex.transaction()
    try {
        const idList = await trx('directions').insert(directions).returning('id')
        const nominationsList = []
        idList.forEach((data, index) => {
            for (const nomination of nominations[index]) {
                nominationsList.push({direction_id: data.id, name: nomination.name})
            }
        })
        await trx('nominations').insert(nominationsList)
        await trx.commit()
    }
    catch (error) {
        await trx.rollback()
        console.log(error)
    }
}