

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
    // Deletes ALL existing entries
    await knex('order_roles').del()
    await knex('order_roles')
        .insert([
            {id: 1 ,name: 'Руководитель'},
            {id: 2 ,name: 'Участник'},
            {id: 3 ,name: 'Тех. персонал'}
        ])
};
