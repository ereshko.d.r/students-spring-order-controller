const path = require('path')
const config = require('../config')

module.exports = {
    development: {
        client: 'sqlite3',
        connection: {
            filename: path.join(__dirname, 'db.sqlite3')
        },
        migrations: {
            tableName: 'knex_migrations'
        },
        useNullAsDefault: true
    },
    production: {
        client: 'pg',
        connection: {
            host:       config.database.host,
            port:       config.database.port,
            user:       config.database.user,
            password:   config.database.password,
            database:   config.database.database,
        },
        pool: {
            min: 0,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    }
}