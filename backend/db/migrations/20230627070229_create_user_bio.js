/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('users_bio', (table) => {
            table.integer('user_id').unique().unsigned().notNullable()
            table.string('avatar')
            table.string('bage')
            table.string('phone')
            table.string('telegram')

            table.primary('user_id')
            table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE')
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTable('users_bio')
};
