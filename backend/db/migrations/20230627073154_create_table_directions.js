/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('directions', (table) => {
            table.increments('id').primary()
            table.string('name').unique().notNullable()
            table.boolean('is_perfomance').defaultTo(true).notNullable()
            table.text('description')
        })
        .createTable('nominations', (table) => {
            table.increments('id').primary()
            table.integer('direction_id').unsigned().notNullable()
            table.string('name').unique().notNullable()
            table.text('description')

            table.foreign('direction_id').references('id').inTable('directions').onDelete('CASCADE').onUpdate('CASCADE')
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema
        .dropTable('directions')
        .dropTable('nominations')
};
