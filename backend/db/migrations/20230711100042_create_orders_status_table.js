/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('orders_status', (table) => {
            table.increments('id').primary()
            table.integer('order_id').unsigned().notNullable()
            table.boolean('status')
            table.json('group_name')
            table.json('perfomance_name')
            table.boolean('is_sent').defaultTo(false).notNullable()

            table.foreign('order_id').references('id').inTable('orders')
                .onDelete('CASCADE')
                .onUpdate('CASCADE')
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};
