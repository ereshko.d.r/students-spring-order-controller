/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('accounts', (table) => {
            table.integer('user_id').unique().unsigned().notNullable()
            table.string('email').unique().notNullable()
            table.string('password').notNullable()
            table.string('refresh_token')
            table.boolean('block_status').defaultTo(false).notNullable()

            table.primary(['user_id', 'email'])
            table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE')
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};
