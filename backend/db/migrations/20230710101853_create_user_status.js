/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('users_status', (table) => {
            table.increments('id').primary()
            table.integer('user_id').unsigned().notNullable()
            table.json('passport')
            table.json('bage')
            table.boolean('status')

            table.foreign('user_id').references('id').inTable('users')
                .onDelete('CASCADE')
                .onUpdate('CASCADE')
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema
        .dropTable('users_status')
};
