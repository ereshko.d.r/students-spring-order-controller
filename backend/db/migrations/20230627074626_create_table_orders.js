/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('orders', (table) => {
            table.increments('id').primary()
            table.integer('direction_id').unsigned().notNullable()
            table.integer('nomination_id').unsigned().notNullable()
            table.string('group_name').notNullable()
            table.string('perfomance_name')

            table.foreign('direction_id').references('id').inTable('directions').onDelete('CASCADE').onUpdate('CASCADE')
            table.foreign('nomination_id').references('id').inTable('nominations').onDelete('CASCADE').onUpdate('CASCADE')
        })
        .createTable('order_users', (table) => {
            table.integer('order_id').unsigned().notNullable()
            table.integer('user_id').unsigned().notNullable()
            table.integer('role_id').unsigned().notNullable()
            table.boolean('is_join').defaultTo(false).notNullable()

            table.primary(['order_id', 'user_id'])
            table.foreign('order_id').references('id').inTable('orders').onDelete('CASCADE').onUpdate('CASCADE')
            table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE')
            table.foreign('role_id').references('id').inTable('order_roles').onDelete('CASCADE').onUpdate('CASCADE')
            table.unique(['order_id', 'user_id'])
        })
        .createTable('invites', (table) => {
            table.increments('id').primary()
            table.integer('order_id').unsigned().notNullable()
            table.integer('user_id').unsigned().notNullable()

            table.foreign('order_id').references('id').inTable('orders').onDelete('CASCADE').onUpdate('CASCADE')
            table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE')
            table.unique(['order_id', 'user_id'])
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema
        .dropTable('orders')
        .dropTable('order_users')
        .dropTable('invites')
};
