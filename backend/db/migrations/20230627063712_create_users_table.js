const ROLES = ['user', 'volunteer', 'moderator', 'admin']
const GENDERS = ['male', 'female']

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('users', (table) => {
            table.increments('id').primary()
            table.string('last_name')
            table.string('first_name')
            table.string('second_name')
            table.string('gender').checkIn(GENDERS)
            table.string('role').checkIn(ROLES).defaultTo('user').notNullable()
            table.timestamp('birthdate', { useTz: false })
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTable('users')
};
