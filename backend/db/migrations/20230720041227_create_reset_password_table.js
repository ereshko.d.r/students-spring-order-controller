/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('reset_tokens', (table) => {
            table.increments('id').primary()
            table.integer('account_user_id').unsigned().notNullable()
            table.string('reset_token')

            table.foreign('account_user_id').references('user_id')
                .inTable('accounts')
                .onDelete('CASCADE')
                .onUpdate('CASCADE')
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};
