/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
        .createTable('users_docs', (table) => {
            table.integer('user_id').unique().unsigned().notNullable()
            table.string('pass_photo').notNullable()
            table.string('pass_photo_first_name').notNullable()
            table.string('pass_photo_last_name').notNullable()
            table.string('pass_photo_second_name').notNullable()
            table.string('pass_serial').checkLength('=', 4).notNullable()
            table.string('pass_number').checkLength('=', 6).notNullable()

            table.primary('user_id')
            table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE')
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTable('users_docs')
};
