const { EventSourceServer } = require('./controllers/eventSourceClients')

const eventSourceServer = new EventSourceServer()

module.exports.eventSourceServer = eventSourceServer