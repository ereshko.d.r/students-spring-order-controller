const nodemailer = require('nodemailer')
const { emailAuth } = require('./config')


class EmailSender {
    #accessData = {
        host: emailAuth.host,
        port: emailAuth.port,
        secure: true,
        auth: {
            user: emailAuth.user,
            pass: emailAuth.pass
        }
    }
    constructor() {
        this.transporter = nodemailer.createTransport(this.#accessData)
    }

    async sendMail(to, subject, text, html=null) {
        try {
            this.transporter.sendMail({
                from: 'studvesna.tyumen@yandex.ru',
                to: to,
                subject: subject,
                text: text,
                html: html
            })
        }
        catch (error) {
            console.log(error)
        }
    }
}

module.exports.EmailSender = EmailSender