const { Server } = require('socket.io')
const { knex } = require('./knex')
const { checkJsonWebToken } = require('./middlewares/websocket/checkJsonWebToken')
const { checkIsMember } = require('./middlewares/websocket/checkIsMember')
const { eventSourceServer } = require('./eventSourceServer')

const clientList = []

module.exports.clientList = clientList

module.exports.initializeWebsocket = (server) => {
    const clientList = []
    const websocket = new Server(server, {
        cors: {
            methods: ["GET", "POST"],
            allowedHeaders: ["*"],
            credentials: true
        }
    })

    websocket.use(checkJsonWebToken)
    websocket.use(checkIsMember)

    websocket.on('connection', (socket) => {
        console.log(`connection user => ${socket.user}`)
        const {orderId} = socket.handshake.query
        socket.join(orderId)
        addClient(socket.user, socket)
        socket.on('orders', async (data, args) => {
            console.log(data, args)
        const {orderId} = socket.handshake.query
        const userId = socket.user
        switch (data) {
            case 'getOrder': {
                const result = await orderState(orderId)
                if (result) {
                    return socket.emit('orders:orderState', result)
                }
                return socket.emit('error', 404)
            }
            case 'editOrder': {
                const result = await updateOrder(orderId, args)
                if (result) {
                    return websocket
                        .to(orderId)
                        .emit('orders:updateName', result)
                }
                return socket.emit('error')
            }
            case 'sendInvite': {
                const result = await inviteUserToOrder(orderId, args)
                if (result) {
                    const freshState = await orderState(orderId)
                    return websocket
                        .to(orderId)
                        .emit('orders:orderState', freshState)
                }
            }
            case 'deleteOrder': {
                const result = await deleteOrder(orderId)
                if (result) {
                    return websocket.to(orderId).emit('error', 404)
                }
            }
            case 'leaveOrder': {
                const result = await leaveOrder(orderId, userId)
                const client = getClient(userId)
                if (client?.socket) {
                    client.socket.disconnect()
                }
                if (result) {
                    const freshState = await orderState(orderId)
                    return websocket
                        .to(orderId)
                        .emit('orders:orderState', freshState)
                }
            }
            case 'removeUser': {
                const result = await removeUser(orderId, args)
                const {userId} = args
                const client = getClient(userId)
                if (client?.socket) {
                    client.socket.disconnect()
                }
                if (result) {
                    const freshState = await orderState(orderId)
                    return websocket
                        .to(orderId)
                        .emit('orders:orderState', freshState)
                }
            }
            case 'changeRole': {
                const result = await changeRole(orderId, args)
                if (result) {
                    const freshState = await orderState(orderId)
                    return websocket
                        .to(orderId)
                        .emit('orders:orderState', freshState)
                }
            }
            case 'changeDirector': {
                const result = await giveDirector(orderId, userId, args)
                if (result) {
                    const freshState = await orderState(orderId)
                    return websocket
                        .to(orderId)
                        .emit('orders:orderState', freshState)
                }
            }
            case 'sendOrder': {
                const result = await sendOrder(orderId)
                if (result) {
                    const freshState = await orderState(orderId)
                    freshState.participants.forEach(user => {
                        eventSourceServer.updateOrdersClients(user.id)
                    })
                    return websocket
                        .to(orderId)
                        .emit('orders:orderState', freshState)
                }
            }
        }
        })
        socket.on('disconnecting', () => {
            removeClient(socket.user)
            console.log(`disconnecting user => ${socket.user}`)
        })

    })
      

    const getClient = (userId) => {
        const index = clientList.findIndex((obj) => userId === obj.userId)
        return index === -1 ? null : clientList[index]
    }

    const addClient = (userId, socket) => {
        const client = getClient(userId)
        if (client) {
            client.socket = socket
        }
        clientList.push({userId, socket})
        const check = setInterval(() => {
            if(socket.disconnected) {
                removeClient(userId)
                clearInterval(check)
            }
        }, 5000);
        console.log(`WS: users: ${clientList.length}`)
    }

    const removeClient = (userId) => {
        const index = clientList.findIndex((obj) => userId === obj.userId)
        if (index !== -1) {
            clientList.splice(index, 1)
        }
        console.log(`WS: users: ${clientList.length}`)

    }
    return websocket
}

const orderState = async (orderId) => {
    try {
        const order = await knex('orders')
            .join('directions', 'orders.direction_id', 'directions.id')
            .join('nominations', 'orders.nomination_id', 'nominations.id')
            .join('order_users', 'orders.id', 'order_users.order_id')
            .join('users', 'order_users.user_id', 'users.id')
            .join('order_roles', 'order_users.role_id', 'order_roles.id')
            .leftJoin('orders_status', 'orders.id', 'orders_status.order_id')
            .leftJoin('users_status', 'users.id', 'users_status.user_id')
            .join('users_bio', 'users.id', 'users_bio.user_id')
            .select({
                'groupName': 'orders.group_name',
                'perfomanceName': 'orders.perfomance_name',
                'nominationName': 'nominations.name',
                'orderStatus': 'orders_status.status',
                'statusName': 'orders_status.group_name',
                'statusPerfomance': 'orders_status.perfomance_name',
                'orderIsSent': 'orders_status.is_sent',
                'isPerfomance': 'directions.is_perfomance',
                'participants': knex.raw(`
                    JSON_AGG(json_build_object(
                        'id', users.id,
                        'firstName', users.first_name,
                        'lastName', users.last_name,
                        'secondName', users.second_name,
                        'telegram', users_bio.telegram,
                        'orderRole', order_roles.name, 
                        'isJoin', order_users.is_join,
                        'avatar', users_bio.avatar,
                        'status', users_status.status,
                        'bage', users_status.bage,
                        'passport', users_status.passport
                    ) ORDER BY order_roles.id)
                `)
            })
            .groupBy(
                'orders.group_name', 'orders.perfomance_name',
                'nominations.name', 'orders_status.status',
                'directions.is_perfomance', 'orders_status.id'
            )
            .where(
                'orders.id', orderId
            )
            .first()

        if (order) {
            return order
        }
        return null
    }
    catch (error) {
        console.log(error)
        return null
    }
}

const updateOrder = async (orderId, args) => {
    const { groupName, perfomanceName } = args
    try {
        const users = await knex('order_users')
                .select({userId: 'user_id'})
                .where('order_id', orderId)
        const name = await knex('orders')
            .update({
                'group_name': groupName,
                'perfomance_name': perfomanceName || null
            })
            .where('id', orderId)
            .returning({
                groupName: 'group_name',
                perfomanceName: 'perfomance_name'
            })
            .then(data => data[0])
        users.map(obj => {
            eventSourceServer.updateOrdersClients(obj.userId)
        })
        return name
    }
    catch (error) {
        console.log(error)
        return null
    }
}

const inviteUserToOrder = async (orderId, args) => {
    const { userId, roleId } = args
    const trx = await knex.transaction()
        try {
            await trx('invites')
                .insert({
                    'order_id': orderId,
                    'user_id': userId
                })
            await trx('order_users')
                .insert({
                    'order_id': orderId,
                    'user_id': userId,
                    'role_id': roleId
                })
            await trx.commit()
            eventSourceServer.updateInvitesClients(userId)
            return true
        }
        catch (error) {
            console.log(error)
            trx.rollback()
            return null
        }
}

const deleteOrder = async (orderId) => {
        try {
            const users = await knex('order_users')
                .select({userId: 'user_id'})
                .where('order_id', orderId)
            await knex('orders')
                .del()
                .where('id', orderId)
            users.map(obj => {
                eventSourceServer.updateOrdersClients(obj.userId)
            })
            return users
        }
        catch (error) {
            console.log(error)
            return null
        }
}

const leaveOrder = async (orderId, userId) => {
    try {
        await knex('order_users')
            .where({
                'order_id': orderId,
                'user_id': userId
            })
            .del()
        eventSourceServer.updateOrdersClients(userId)
        return true
    }
    catch (error) {
        console.log(error)
        return null
    }
}

const removeUser = async (orderId, args) => {
    const {userId} = args
    const trx = await knex.transaction()
    try {
        await trx('order_users')
            .where({
                'order_id': orderId,
                'user_id': userId
            })
            .del()
        await trx('invites')
            .where({
                'order_id': orderId,
                'user_id': userId
            })
            .del()
        trx.commit()
        eventSourceServer.updateOrdersClients(userId)
        return true
    }
    catch (error) {
        trx.rollback()
        return null
    }
}

const changeRole = async (orderId, args) => {
    const {userId, roleId} = args
    try {
        await knex('order_users')
            .update({
                'role_id': roleId,
            })
            .where({
                'order_id': orderId,
                'user_id': userId
            })
        return true
    }
    catch (error) {
        console.log(error)
        return null
    }
}

const giveDirector = async (orderId, userId, args) => {
    const newDirector = args.userId
    const trx = await knex.transaction()
    try {
        await trx('order_users')
            .update({
                'role_id': 1
            })
            .where({
                'order_id': orderId,
                'user_id': newDirector
            })
        await trx('order_users')
            .update({
                'role_id': 2
            })
            .where({
                'order_id': orderId,
                'user_id': userId
            })
        trx.commit()
        return true
    }
    catch (error) {
        console.log(error)
        trx.rollback()
    }
}

const sendOrder = async (orderId) => {
    try {
        await knex('orders_status')
            .update({
                'is_sent': true,
                'status': null
            })
            .where('order_id', orderId)
        return true
    }
    catch (error) {
        console.log(error)
    }
    return false
}
