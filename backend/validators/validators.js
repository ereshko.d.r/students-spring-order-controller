const { knex } = require('../knex')

module.exports = {
    emailValidator: (email) => {
        const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}(?:\.[a-zA-Z]{2,})?$/
        if (emailRegex.test(email)) {
            return true
        }
        throw new Error('Невалидное поле "email"')
    },
    uniqueEmailValidator: async (email) => {
        console.log(email)
        const result = await knex('accounts')
            .select('email')
            .where('email', '=', email)
            .then(data => {
                return data.length === 0
            })
            .catch(error => console.log(error))
        console.log(result)
        if (result) {
            return true
        }
        throw new Error('Данный email уже используется')
    },
    genderValidator: (gender) => {
        if (gender === 'male' || gender === 'female') {
            return true
        }
        throw new Error('Невалидное поле "gender"')
    },
    roleValidator: (role) => {
        const roles = ['user', 'volunteer', 'moderator', 'admin']
        if (roles.includes(role)) {
            return true
        }
        throw new Error('Невалидное поле "role"')
    },
    nameValidator: (name) => {
        const nameRegex = /^[А-ЯЁ][а-яё]*$/
        if (nameRegex.test(name)) {
            return true
        }
        throw new Error('Невалидное имя')
    },
    phoneValidator: (number) => {
        const phoneRegex = /^\d{10}$/
        if (phoneRegex.test(number)) {
            return true
        }
        throw new Error('Невалидный номер телефона')
    },
    telegramValidator: (username) => {
        const telegramRegex = /^[a-zA-Z0-9_]{5,}$/
        if (telegramRegex.test(username)) {
            return true
        }
        throw new Error('Невалидный username')
    },
    passwordValidator: (password) => {
        const lenght = (password >= 8)
        const hasUpperAndLowerCase = /^(?=.*[a-z])(?=.*[A-Z]).+$/
            .test(password)
        const hasSpecialSymbols = /[^A-Za-z]/.test(password)
        return (lenght && hasUpperAndLowerCase && hasSpecialSymbols)
    },
    serialPassValidator: (serial) => {        
        return /^\d{4}$/.test(serial)
    },
    numberPassValidator: (number) => {
        return /^\d{6}$/.test(number)
    },
    base64Validator: (base64) => {
        return /^data:image\/(png|jpeg|jpg|gif);base64,[A-Za-z0-9+/=\s]+$/
            .test(base64)
    }
}