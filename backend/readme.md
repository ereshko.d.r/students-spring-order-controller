# backend приложение

## Структура

### root

##### App.js

Точка входа в приложение. Инициализирует приложение и его зависимости

##### config.js

Файл конфигурации

##### eventSourceServer.js

Файл инициализации `eventSourceServer`

##### knex.js

Файл инициализации `knex` для создания запросов к `бд`

##### websocket.js

Файл инициализации `websocket` и подключения обработчиков для событий `websocket`

### controllers

Каталог для обработчиков `http` запросов к приложению

##### baseHandler.js

Базовый класс для обработки запросов. Позволяет создать собственный класс для обработки запросов.

В конструкторе класса создаётся экземпляр класса `Router` библиотеки `express`. В методе `routers()` экземпляру `Router` присваиваются обработчики эндпоинтов. В методе `middleware()` для всего `Router` присваиваются `middlewares` для предварительной обработки входящего запроса.

```js
class BaseHandler {
    constructor(allowedMethods) {
        this.allowedMethods = allowedMethods || []
        this.router = express.Router()
        this.router.use((request, response, next) => {
            if (!this.#checkAllowedMethods(request.method)) {
                return response.sendStatus(this.STATUS_CODE.NOT_ALLOWED_METHOD)
            }
            next()
        })
        this.middleware()
        this.routers()
        return this.router
    }

    #checkAllowedMethods(method) {
        return this.allowedMethods.includes(method)
    }
    
    middleware(){
        
    }
    
    routers() {
        
    }
}
```

Для собственного класса обработки входящих запросов нужно унаследоваться от базового класса `BaseHandler` и в `constructor()` функцией `super()` передать массив доступных http-методов запроса к эндпоинтам

```js
class YourHandler extends BaseHandler {
    constructor() {
        super(['GET', 'POST', 'DELETE', 'PATCH'])
    }
}
```

Далее создать обработчики, которые будут принимать в себя два аргумента `request` и `response` и переопределить метод `routers()` передав в свойство класса `this.router` эндпоинты и обработчики. При необходимости можно подключить дополнительные промежуточные обработчики `middleware` или пробежуточный обработчик для всего класса при помощи переопределения метода `middleware()`

```js
middleware() {
    this.router.use(checkJsonWebToken)
}

routers(){
    this.router.get(
        '/endpoint',
        someMiddleware,
        (request, response) => this.myHandler(request, response)
    )
    this.router.post(
        '/endpoint/:id',
        (request, response) => this.alsoMyHandler(request, response)
    )
}

async myHandler(request, response) {
    response.status(200).json({message: 'hi!'})
}

async alsoMyHandler(request, response) {
    const {id} = request.params
    response.status(201).json({message: `some with id ${id} has been created`})
}
```

##### router.js

Файл для настройки маршрутизации `http` запросов к приложению. Использует промежуточный обработчик `Router` библиотеки `express` для создания маршрутов к эндпонитам.

```js
router.use('/your_path', youHandler)
```

##### eventSourceClients.js

Класс для управления клиентами подписанными на `event source server`. Имеет список подписанных клиентов `clientsList` и интерфейс для взаимодействия с ним, а так же методы для отпарвки различных состояний пользователя, его заявок или приглашений

###### addClient(userId, response)

Метод класса для добавления подписанного клиена в список клиентов. При подписке клиент получает свежие данные о состояниях заявок и приглашений в заявки

###### getClient(userId)

Метод класса возвращающий объект `{userId: Number, response: Object}` или `null`

###### removeClient(userId)

Метод класса удаляющий клиента из списка клиентов

### db

Каталог для настройки интерфейса общения с базой данных. Содержит файлы миграций, сиды и файл конфигурации `knexfile.js`

### middlewares

Каталог для хранения промежуточных обработчиков

### services

Каталог для хранения прочих сервисов не связанных непосредственно с обработкой входящих запросов

### validators

Каталог для хранения общих валидаторов
