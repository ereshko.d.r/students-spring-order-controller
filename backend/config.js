const dotenv = require('dotenv')
const path = require('path')
dotenv.config()

const requestSize = '10mb'

const env = process.env.NODE_ENV
const secretKey = process.env.SECRET_KEY

const baseDir = path.resolve(__dirname)
const publicDir = path.join(baseDir, 'public')
const privateDir = path.join(baseDir, 'private')

const server = {
    port:       process.env.PORT
}

const database = {
    host:       process.env.DB_HOST,
    port:       process.env.DB_PORT,
    user:       process.env.DB_USER,
    password:   process.env.DB_PASS,
    database:   process.env.DB_NAME
}

const emailAuth = {
    host:       process.env.MAIL_HOST,
    port:       process.env.MAIL_PORT,
    user:       process.env.MAIL_USER,
    pass:       process.env.MAIL_PASS
}

module.exports = {
    server,
    database,
    env,
    secretKey,
    publicDir,
    baseDir,
    privateDir,
    requestSize,
    emailAuth
}